var minSize = 25;
$.fn.extend({
    animateCssShow: function (animationName) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        this.addClass('animated open ' + animationName).one(animationEnd, function() {
            $(this).removeClass('animated ' + animationName);
        });
    },
    animateCssHide: function (animationName) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
         $(this).addClass('animated ' + animationName).one(animationEnd, function() {
            $(this).removeClass('animated open ' + animationName);
        });
    },
    animateCss: function (animationName,afterFunc) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
         $(this).addClass('animatedS ' + animationName).one(animationEnd, function() {
            if(afterFunc != ''){
                afterFunc();
            }
            $(this).removeClass('animatedS ' + animationName);
        });
    }
});
var oldBlock = [];
var active = 0;
var xhr = '';
var activeP = 0;
var xhrP = '';
var selectedSize = '';
var oneSide = [52,69,53,54,55];
var notBan = [53,54,55];
var notKonvLam = [21,22,83,84,31,32,33,34,57,58,71,94,79,81,77,78];
var notLackLam = [53,54,55,52,6,38,39,40,41,42,44,46,47,48,49,50,51,67,68,71,72,73,74,75,76,77,78,79,80,81,93,94,95,97,98,99,100,
57,58,59,60,61,62,63,64,65,66,31,32,33,34,35,36,37,
21,22,23,24,25,87,69,83,84,85,86,
83,31,57,44,38,52,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116];
var lam_2 = [8,10,12,14,29,31,33,34,35,36,37,38,39,40,41,42,46,114];
var lackLam = [113,114];
var notClLam = [7,8,11,12,33,34,35,36,37,38,39,40,41,42,46,113,114];
var rulLam = [7,9,11,13,28,30,8,10,12,14,29,31];
var noRulLam = [21,22,23,24,83,84,85,86,31,32,33,34,35,57,58,71,94,79,81,77,78];
var konv_lam = [33,34,35,36,37,38,39,40,41,42,46];
var canWhite = [55,101,102,103,104,107,105,108,109,110,111,112,106,113,114,115,116,44,68,50,51,47,93,100,39,40,41,42,72,97,98,73,74,71,94,79,77,78,80];
var whiteP = [8,9,10,11];
var digiLack = [12,13];
var mondi = [57,58,59,60,61,62,63];
var canBanner = [21,22,23,24,25,26,27,52,69];
var insert = '<p class="text-center" style="font-size: 50px;padding: 20px 0;"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span></p>';
var firstLoad = true;
var printClick = '';
var width = 90;
var height = 50;
var flagReset = true;
// Функция смены текста области запечатки в "печать в листах"
function change_text_info_razmer_print_list(){
    $('.infoPrintHeight').html($('#sel_razmer option:selected').attr('r_h')-10);
    $('.infoPrintWidth').html($('#sel_razmer option:selected').attr('r_w')-10);
}
// функция смены текста тиража в модальном наценки
function change_text_modal_tirazh(name,val_inp){
    $('.m_d_tir_'+name).html(val_inp);
}
$('document').ready(function(){
     $('select.sel_post_rab,#lam,#sel_razmer,.sel_metr,.dop_select,.new_color_option').chosen({width: "100%",disable_search_threshold: 10,disable_search: true});
     $('#price').popover({
        selector: '[data-toggle=tooltip]',
        placement: 'top',
        html: true,
        trigger: 'hover'
    });
    function removeAnim(){
        $('.animated.zoomIn').removeClass('animated zoomIn');
    }
    setTimeout(removeAnim,1000);
    $('.t_greyblock').popover({
        selector: '[data-toggle=tooltip]',
        placement: 'top',
        html: true,
        trigger: 'hover',
        container: 'body'
    });
//    $("#sel_razmer").chosenImage({
//      disable_search_threshold: 10
//    });
    var contentPop = new Array();
    $('.t_greyblock .t_plot[data-toggle]').each(function(){
        var id = $(this).attr('for');
        var text = $(this).attr('data-content');
        var ta = {
             id: text
        }
        contentPop[id] = text;
    });
    /*GET DATA AJAX*/
        $("#pricefilters").submit(function() {
        if(active != 0){
           xhr.abort();
           active=0;
        }
        xhr = $.ajax({
                  type: "POST",
                  url: "cside/ajax.php?action=price_digital",
                  data: $("#pricefilters").serialize(),
                  dataType: 'json',
                  beforeSend: function(){
                    active++;
                    $('#insert-in').html('');
                    $('#loading-table').html(insert);
                    $('.t_greyblock input').attr('readonly',true);
                    $('.t_greyblock input:not(:checked)').closest('.t_switchable').addClass('imitate-disabled').find('.t_plot').addClass('blocked-check');
                    $('.t_greyblock .t_plot[data-toggle]').attr('data-content','Дождитесь загрузки таблицы');
                  },
                complete: function(data){
                        if(data.responseText == 'error'){
                            location.reload();
                        } else if(data.statusText == 'OK' && IsJsonString(data.responseText) == false){
                            $('#loading-table').html('');
                            var text = '<tr><td colspan="9" class="text-center" style="vertical-align:middle;padding:20px;font-weight:500;font-size:22px;">При подсчете произошла ошибка! Пожалуйста сообщите нам, об этом нажав на кнопку "Нашли ошибку".</td></tr>';
                            $('#insert-in').html(text);
                        }
                    },
                  success: function(data){
                       $('#loading-table').html('');
                       $('#insert-in').html(com(data.text));
                        if(data.blockedMat.length > 0){
                            blockMaterial(data.blockedMat);
                         } else {
                             $('.select-mat.postDisabled:disabled').each(function(){
                                 $(this).removeClass('postDisabled');
                                 if($(this).hasClass('lamDisabled') == false && $(this).hasClass('countDisabled') == false && !$(this).hasClass('blockLamRul') && !$(this).hasClass('blockLamKonv') && !$(this).hasClass('blockLamLack')&& !$(this).hasClass('disabledWhite')  && !$(this).hasClass('disabledSize')&& !$(this).hasClass('disabledFigcutting')&& !$(this).hasClass('disabledSize1')&& !$(this).hasClass('disabledCl')){
                                    $(this).prop('disabled',false);
                                 }
                             });
                            blockColor();
                            blockLam();
                         }
                       blockNullGroup();
                        active=0;
                        xhr = '';
                        $(this).queue(function(){
                            for(key in contentPop){
                                $('[for="' + key + '"]').attr('data-content',contentPop[key]);
                            }
                            $(this).dequeue();
                        });
                        $(this).queue(function(){
                            $('.t_greyblock input').attr('readonly',false).closest('.t_switchable').removeClass('imitate-disabled').find('.t_plot').removeClass('blocked-check');
                            $(this).dequeue();
                        });
                        var checkM = $('#selectall:checked').length + $('.select-mat:checked').length;
                        var checkT = $('#alltime:checked').length + $('.terms:checked').length;
                        var checkC = $('#allcolor:checked').length + $('.colors:checked').length;
                        var checkL = $('.laminat:checked').length;
                        if(checkM > 0 && checkT > 0 && checkC > 0 && checkL > 0){
                            $('.time-select.hidden').removeClass('hidden').animateCss('fadeIn','');
                            $('#alltime').closest('.t_switchable').find('.filter-name').html('Cрок печати');
                        } 
                  }
            });
        return false;
    });

    /*END GET DATA AJAX*/

   /*Обработчики фильтров*/
    $('#selectall').change(function(){
        $(this).queue(function(){
            $('.select-mat:checked').prop('checked',false).trigger('delico');
            $(".colors:disabled").prop('disabled',false);
            if($('#selectall').prop('checked') == true){
                $(".laminat.matDisabled:disabled").removeClass('matDisabled');
                $(".laminat.matDisabled:disabled:not(.disabledGlue):not(.sideBlock):not(.blockKonv):not(.disabledPost):not(.countDisabled):not(.blockBySize)").prop('disabled',false);
            }
            blockLam();
            blockerMat();
            blockAllRullLam();
            blockAllLackLam();
            blockAllKonvLam();
            $(this).closest('.t_switchable').find('.filter-name').html('Материал');
            add_opacity_filter_reset();
            $(this).dequeue();
        });
        $(this).queue(function(){
            blockColor();
            setSubmit();
            $(this).dequeue();
        });
    });

    $('#alltime').change(function(){
        $(this).queue(function(){
            $('.terms:checked').prop('checked',false);
            $(this).closest('.t_switchable').find('.filter-name').html('Срок печати');
            add_opacity_filter_reset();
            $(this).dequeue();
        });
        $(this).queue(function(){
            setSubmit();
            $(this).dequeue();
        });
    });

    $('#allcolor').change(function(){
        $(this).queue(function(){
            $('.colors:checked').prop('checked',false);
            $(this).closest('.t_switchable').find('.filter-name').html('Печать');
            blockLam();
            add_opacity_filter_reset();
            $(this).dequeue();
        });
        $(this).queue(function(){
            blockerColor();
            setSubmit();
            $(this).dequeue();
        });
    });
    $('.select-mat').change(function(){
        $(this).queue(function(){
            $('#selectall:checked').prop('checked',false);
            blockLamMaterial('select-mat',oneSide,lam_2,'#l_','matDisabled','lamDisabled');
            
            blockAllRullLam();
            blockAllLackLam();
            blockerMat();
            blockAllKonvLam();
            
            $('#selectall:not(:disabled)').closest('.t_switchable').find('.filter-name').html('Выбрать все<br>материалы');
            add_opacity_filter_reset();
            $(this).dequeue();
        });
        $(this).queue(function(){
            blockColor();
            setSubmit();
            $(this).dequeue();
        });
    });
    $(".laminat").change(function(){
        $(this).queue(function(){
            if($(this).prop('checked') == true){
                if($(".laminat:checked").length <= 3){
                    if($(".laminat:checked").length == 3){
                        $(".laminat:not(:checked):not(.countDisabled)").addClass('countDisabled').prop('disabled',true);
                    } else if($(".laminat:checked").length < 3){
                        $(".laminat.countDisabled").removeClass('countDisabled');
                        $(".laminat:disabled:not(.matDisabled):not(.disabledGlue):not(.sideBlock):not(.blockKonv):not(.disabledPost):not(.blockBySize):not(.blockMatRul):not(.blockMatKonv):not(.blockMatLack):not(.disabledCl1):not(.disabledCl2)").prop('disabled',false);
                    }
                }
            } else {
                if($(".laminat:checked").length < 3){
                    $(".laminat.countDisabled").removeClass('countDisabled');
                    $(".laminat:disabled:not(.matDisabled):not(.disabledGlue):not(.sideBlock):not(.blockKonv):not(.disabledPost):not(.blockBySize):not(.blockMatRul):not(.blockMatKonv):not(.blockMatLack):not(.countDisabled):not(.disabledCl1):not(.disabledCl2)").prop('disabled',false);
                }
            }
            if($('.laminat:checked').length > 0){
                $('#lam-check').closest('.t_switchable').find('.filter-name').html('Покрытие<br><span style="text-transform:none;">До 3-х вариантов</span>');
            } else {
                $('#lam-check').closest('.t_switchable').find('.filter-name').html('Покрытие');
            }
            blockLamMaterial('laminat',lam_2,oneSide,'#m_','lamDisabled','matDisabled');
            blockColor();
            blockAllNotRullMat();
            blockAllNotLackMat();
            blockAllNotKonvMat();
            blockerLam();
            add_opacity_filter_reset();
            $(this).dequeue();
        });
        $(this).queue(function(){
            setSubmit();
            $(this).dequeue();
        });
    });
    $(".terms").change(function(){
        $(this).queue(function(){
            $('#alltime:checked').prop('checked',false);
            $('#alltime:not(:disabled)').closest('.t_switchable').find('.filter-name').html('Выбрать все<br>сроки печати');
            add_opacity_filter_reset();
            $(this).dequeue();
        });
        $(this).queue(function(){
            setSubmit();
            $(this).dequeue();
        });
    });
    
    $(".colors").change(function(){
        $(this).queue(function(){
            $('#allcolor:checked').prop('checked',false);
            $('#allcolor:not(:disabled)').closest('.t_switchable').find('.filter-name').html('Выбрать все<br>печати');
            add_opacity_filter_reset();
            $(this).dequeue();
        });
        $(this).queue(function(){
            blockerColor();
            setSubmit();
            $(this).dequeue();
        });
    });
    function blockerLam(){
        var flagBlockDopCl = true;
        var flagBlockLam0 = false;
        if($('.laminat:checked').length > 0){
            $('.laminat:checked').each(function(){
                if(notClLam.indexOf(+$(this).val()) == -1){
                    flagBlockDopCl = false;
                }
            });
        } else {
            flagBlockDopCl = false;
        }
        if($('.laminat:checked').length == 1 && $('#nolam').prop('checked')){
            flagBlockLam0 = true;
        }
        if(flagBlockDopCl){
            if($('#cl-select').val() > 0){
                $('#cl-select').val(0).change();
            }
            $('#cl-select').prop('disabled',true).addClass('blockUF');
            for(var i = 0; i < digiLack.length; i++){
                $('#color' + digiLack[i] + ':checked').prop('checked',false).change();
                $('#color' + digiLack[i]).prop('disabled',true).addClass('blockUF');
            } 
        } else {
            if($('.colors:checked').length > 0 || $('#allcolor').prop('checked')){
                $('#cl-select:not(.disabledSize):not(.disabledFilm)').prop('disabled',false).trigger("chosen:updated");
            }
            $('#cl-select').removeClass('blockUF');
            for(var i = 0; i < digiLack.length; i++){
                $('#color' + digiLack[i] + ':not(.disabledDop):not(.blockSide):not(.disabledFilm)').prop('disabled',false);
                $('#color' + digiLack[i]).removeClass('blockUF');
            }
        }
        if($('#flagFigcutting').length > 0){
            if(flagBlockLam0){
                $('.select-mat').each(function(){
                    if(+$(this).data('density') >= 250 && +$(this).data('density') != 500){
                        if($(this).prop('checked')){
                            $(this).prop('checked',false).change();
                        }
                        $(this).prop('disabled',true).addClass('disabledFigcutting');
                    }
                });
            } else {
             $('.disabledFigcutting:not(.postDisabled):not(.lamDisabled):not(.countDisabled):not(.blockLamRul):not(.blockLamKonv):not(.blockLamLack):not(.disabledWhite):not(.disabledCl)').prop('disabled',false);
                $('.disabledFigcutting').removeClass('disabledFigcutting');
            }
        }
    }
    function blockerMat(){
        var flagBlockWhite = true;
        var flagBlockLam0 = true;
        var flagBlockDopCl = true;
        if($('.select-mat:checked').length == 0 || $('#selectall').prop('checked')){
            flagBlockWhite = false;
            flagBlockLam0 = false;
            flagBlockDopCl = false;
        } else if($('.select-mat:checked').length == 0 && !$('#selectall').prop('checked')){
            flagBlockWhite = false;
            flagBlockLam0 = false;
            flagBlockDopCl = false;
        } else {
            $('.select-mat:checked').each(function(){
                if(canWhite.indexOf(+$(this).val()) != -1){
                    flagBlockWhite = false;
                }
                if(oneSide.indexOf(+$(this).val()) == -1){
                    flagBlockDopCl = false;
                }
                if($(this).data('density') < 250 || +$(this).data('density') == 500){
                   flagBlockLam0 = false;
                } 
            });
        }
        if(flagBlockDopCl){
            if($('#cl-select').val() > 0){
                $('#cl-select').val(0).change();
            }
            $('#cl-select').prop('disabled',true).addClass('disabledFilm');
            for(var i = 0; i < digiLack.length; i++){
                $('#color' + digiLack[i] + ':checked').prop('checked',false).change();
                $('#color' + digiLack[i]).prop('disabled',true).addClass('disabledFilm');
            } 
        } else {
            if($('.colors:checked').length > 0 || $('#allcolor').prop('checked')){
                $('#cl-select:not(.disabledSize)').prop('disabled',false).trigger("chosen:updated");
            }
            $('#cl-select').removeClass('disabledFilm');
            for(var i = 0; i < digiLack.length; i++){
                $('#color' + digiLack[i] + ':not(.disabledDop):not(.blockSide):not(.blockUF)').prop('disabled',false);
                $('#color' + digiLack[i]).removeClass('disabledFilm');
            }
        }
        if(flagBlockWhite){
            if($('#white-select').val() > 0){
                $('#white-select').val(0).change();
            }
            $('#white-select').prop('disabled',true).addClass('blockUF');
            for(var i = 0; i < whiteP.length; i++){
                $('#color' + whiteP[i] + ':checked').prop('checked',false).change();
                $('#color' + whiteP[i]).prop('disabled',true).addClass('blockUF');
            } 
        } else {
            if($('.colors:checked').length > 0 || $('#allcolor').prop('checked')){
                $('#white-select').prop('disabled',false).trigger("chosen:updated");
            }
            $('#white-select').removeClass('blockUF');
            for(var i = 0; i < whiteP.length; i++){
                $('#color' + whiteP[i] + ':not(.disabledDop):not(.blockSide)').prop('disabled',false);
                $('#color' + whiteP[i]).removeClass('blockUF');
            }
        }
        if($('#flagFigcutting').length > 0){
            if(flagBlockLam0){
                $('#nolam:checked').prop('checked',false).change();
                $('#nolam').prop('disabled',true);
            } else {
                $('#nolam').prop('disabled',false);
            }
        }
    }
    function blockerColor(){
        var flagBlockWhite = true;
        var flagBlockCl = true;
        var unableDop = false;
        var blockTwoSide = true;
        
        if($('.colors:checked').length == 0 && $('#allcolor').prop('checked')){
            flagBlockWhite = false;
            flagBlockCl = false;
            unableDop = true;
        } else if($('.colors:checked').length == 0 && !$('#allcolor').prop('checked')){
            flagBlockWhite = false;
            flagBlockCl = false;
            unableDop = false;
        } else { 
            if($('.colors:checked').length > 0){
                $('.colors:checked').each(function(){
                    if(whiteP.indexOf(+$(this).val()) == -1){
                       flagBlockWhite = false;
                    }
                    if(digiLack.indexOf(+$(this).val()) == -1){
                       flagBlockCl = false;
                    }
                });
                unableDop = true;
            }
        }
        if(unableDop){
            $('.wrapper_select.not_active').removeClass('not_active');
            $('.select_dop_colors:not(#cl-select):disabled:not(.blockUF)').prop('disabled',false).trigger("chosen:updated");
            $('.select_dop_colors#cl-select:disabled:not(.blockUF):not(.disabledSize)').prop('disabled',false).trigger("chosen:updated");
        } else {
            $('.wrapper_select:not(.not_active)').addClass('not_active');
            $('.select_dop_colors:not(:disabled):not(#count_add_white)').prop('disabled',true).val(0).change().trigger("chosen:updated");
        }
        
        if(flagBlockWhite){
            if($('#white-select').val() > 0){
                $('#white-select').val(0).change();
            }
            $('#white-select').prop('disabled',true).trigger("chosen:updated");
        }
        if(flagBlockCl){
            if($('#cl-select').val() > 0){
                $('#cl-select').val(0).change();
            }
            $('#cl-select').prop('disabled',true).trigger("chosen:updated");
        }
        var flagBlockDopWhite = false;
        var flagBlockDopCl = false;
        if($('#white-select').val() > 0 && !$('#white-select').prop('disabled')){
            flagBlockDopWhite = true;
        }
        if($('#cl-select').val() > 0 && !$('#cl-select').prop('disabled')){
            flagBlockDopCl = true;
        }
        if(flagBlockDopWhite){
            $('#count_add_white').prop('disabled',false).trigger("chosen:updated");
            $('.count-white.hidden').removeClass('hidden');
            for(var i = 0; i < whiteP.length; i++){
                $('#color' + whiteP[i] + ':checked').prop('checked',false).change();
                $('#color' + whiteP[i]).prop('disabled',true).addClass('disabledDop');
            }            
        } else {
            $('.count-white:not(.hidden)').addClass('hidden');
            $('#count_add_white').prop('disabled',true).trigger("chosen:updated");
            for(var i = 0; i < whiteP.length; i++){
                $('#color' + whiteP[i] + ':not(.blockUF):not(.blockSide):not(.disabledFilm)').prop('disabled',false);
                $('#color' + whiteP[i]).removeClass('disabledDop');
            }            
        }
        if(flagBlockDopCl){
            for(var i = 0; i < digiLack.length; i++){
                $('#color' + digiLack[i] + ':checked').prop('checked',false).change();
                $('#color' + digiLack[i]).prop('disabled',true).addClass('disabledDop');
            } 
        } else {
            for(var i = 0; i < digiLack.length; i++){
                $('#color' + digiLack[i] + ':not(.blockUF):not(.blockSide):not(.disabledFilm)').prop('disabled',false);
                $('#color' + digiLack[i]).removeClass('disabledDop');
            }
        }
        if(flagBlockWhite || flagBlockDopWhite){
            $('.select-mat').each(function(){
               if(canWhite.indexOf(+$(this).val()) == -1){
                   if($(this).prop('checked')){
                       $(this).prop('checked',false).change();
                   } 
                   if(!$(this).hasClass('disabledWhite')){
                    $(this).prop('disabled',true).addClass('disabledWhite');
                   }
               } 
            });
        } else {
            $('.disabledWhite').each(function(){
                if($(this).hasClass('countDisabled') == false && !$(this).hasClass('lamDisabled') && $(this).hasClass('postDisabled') == false && $(this).hasClass('sideBlock') == false && $(this).hasClass('blockKonv') == false && $(this).hasClass('blockBySize') == false  && !$(this).hasClass('blockMatRul') && !$(this).hasClass('blockMatLack') && !$(this).hasClass('blockMatKonv')&& !$(this).hasClass('blockLamRul') && !$(this).hasClass('blockLamKonv') && !$(this).hasClass('disabledCl1') && !$(this).hasClass('disabledCl2') && !$(this).hasClass('disabledSize')&& !$(this).hasClass('disabledFigcutting')&& !$(this).hasClass('disabledSize1')&& !$(this).hasClass('disabledCl')){
                    $(this).prop('disabled',false);
                } 
                $(this).removeClass('disabledWhite');
            });
        }
        
        if(flagBlockCl || flagBlockDopCl){
            for(var i = 0; i < notClLam.length; i++){
                if($('#l_'+notClLam[i]).prop('checked')){
                    $('#l_'+notClLam[i]).prop('checked',false).change();
                }
                $('#l_'+notClLam[i]).prop('disabled','true').addClass('disabledCl1');
            }
            $('.select-mat').each(function(){
               if(oneSide.indexOf(+$(this).val()) != -1){
                   if($(this).prop('checked')){
                       $(this).prop('checked',false).change();
                   } 
                   if(!$(this).hasClass('disabledCl')){
                    $(this).prop('disabled',true).addClass('disabledCl');
                   }
               } 
            });
        } else {
            for(var i = 0; i < oneSide.length; i++){
                $('#m_'+oneSide[i] + ':not(.postDisabled):not(.lamDisabled):not(.countDisabled):not(.blockLamRul):not(.blockLamKonv):not(.blockLamLack):not(.disabledWhite):not(.disabledFigcutting):not(.disabledSize1)').prop('disabled',false)
                $('#m_'+oneSide[i]).removeClass('disabledCl');
            }
            for(var i = 0; i < notClLam.length; i++){
                $('#l_'+notClLam[i] + ':not(.matDisabled):not(.disabledGlue):not(.sideBlock):not(.blockKonv):not(.disabledPost):not(.blockBySize):not(.blockMatRul):not(.blockMatKonv):not(.blockMatLack):not(.countDisabled):not(.disabledCl2)').prop('disabled',false)
                $('#l_'+notClLam[i]).removeClass('disabledCl1');
            }
        }
        blockNullGroup();
    }
    $('#filter-reset').click(function(){
        $(this).queue(function(){
            $('#alltime:checked,#allcolor:checked,.select-mat:checked,#selectall:checked,.terms:checked,.colors:checked').prop('checked',false).change();
            $('.laminat:checked').prop('checked',false).change();
            if($('#konv_lam').val() > 0){
                $('#l_33').prop('checked',true).change();
            }
            if($('#konv_lam').val() > 0){
                $('#l_33').prop('checked',true).change();
            }
            $(this).dequeue();
        });
        $(this).queue(function(){
            $('.select-all-btn').prop('disabled',true);
            add_opacity_filter_reset();
            flagReset = true;
            $(this).dequeue();
        });
    });
    $('#white-select').change(function(){
       if($(this).val() == 2 || $(this).val() == 3){
           if($('#count_add_white').val() == 2){
               $('#count_add_white').val(1).change();
           }
           $('#count_add_white option[value=2]').prop('disabled',true);
           $('#count_add_white').trigger("chosen:updated");
       } else {
           $('#count_add_white option[value=2]').prop('disabled',false);
           $('#count_add_white').trigger("chosen:updated");
       }
    });
    $('.select_dop_colors, .colors').change(function(){
        var flagShowInfo = false;
        for(var i = 0; i < whiteP.length; i++){
            if($('#color' + whiteP[i]).prop('checked')){
                flagShowInfo = true;
            }
        }
        if(!flagShowInfo){
            for(var i = 0; i < digiLack.length; i++){
                if($('#color' + digiLack[i]).prop('checked')){
                    flagShowInfo = true;
                }
            }
        }
        if(!flagShowInfo){
            $('.select_dop_colors').each(function(){
               if($(this).val() != 0 && !$(this).prop('disabled')){
                   flagShowInfo = true;
               } 
            });
        }
        if(flagShowInfo){
            $('#alert-color:not(.showed)').addClass('showed').show('fast');
        } else {
            $('#alert-color.showed').removeClass('showed').hide('fast');
        }
    });
    function setSubmit(){
        var checkM = $('#selectall:checked').length + $('.select-mat:checked').length;
        var checkT = $('#alltime:checked').length + $('.terms:checked').length;
        var checkC = $('#allcolor:checked').length + $('.colors:checked').length;
        var checkL = $('.laminat:checked').length;
//        if($('#konv_lam').val() == 0 || $('#konv_lam').length == 0){
//            checkL = 1;
//        }

        if(checkM > 0 && checkC > 0 && checkT > 0 && checkL > 0){
            if(flagReset){
                flagReset = false;
                $('.select-all-btn').each(function () {
                   if(this.id == 'selectall'){
                      $(this).closest('.t_switchable').find('.filter-name').html('Выбрать все<br>материалы');
                   } else if (this.id == 'alltime') {
                     $(this).closest('.t_switchable').find('.filter-name').html('Выбрать все<br>сроки печати');
                   } else if (this.id == 'allcolor') {
                     $(this).closest('.t_switchable').find('.filter-name').html('Выбрать все<br>печати');
                   }
                });
            }
            $('.select-all-btn').prop('disabled',false);
            $("#pricefilters").submit();
        } else {
            var insertNotSel = '<tr><td colspan="9" class="text-center" style="vertical-align:middle;padding:20px;font-weight:500;font-size:22px;">Для формирования прайс-листа выберите';
            var text = '';
            if(checkM == 0){
                text += ' материал,';
            }
            if(checkL == 0){
                text += ' ламинацию,';
            }
            if(checkC == 0){
                text += ' печать,';
            }
            if(checkT == 0){
                text += ' срок печати,';
            }
            text = text.substring(0, text.length - 1)
            insertNotSel += text + '</td></tr>';
            $('#insert-in').html(insertNotSel);
        }
    }
    /*END FILTERS*/

   /*Обработчик ввода размера вручную*/
   $('.input_size').on('input',function(){
        if (this.value.match(/[^0-9.]/g)) {
            this.value = this.value.replace(/[^0-9.]/g, '');

        }
        $('.show_price.not-active').removeClass('not-active').addClass('btn-default').css('cursor','pointer');
    });
    $('body').on('click',':checkbox[readonly]',function(){
            return false;
    });
    $('#btn-cont-show-price').on('click','.show_price:not(.not-active)',function(){
            if( ($('#input-width').val()*$('[name="size_in"]').val()) < minWidthProd){
                $('#input-width').val(minWidthProd/$('[name="size_in"]').val());
            }
            if( ($('#input-width').val()*$('[name="size_in"]').val()) > maxWidthProd){
                $('#input-width').val(maxWidthProd/$('[name="size_in"]').val());
            }
            if( ($('#input-height').val()*$('[name="size_in"]').val()) < minHeightProd){
                $('#input-height').val(minHeightProd/$('[name="size_in"]').val());
            }
            if( ($('#input-height').val()*$('[name="size_in"]').val()) > maxHeightProd){
                $('#input-height').val(maxHeightProd/$('[name="size_in"]').val());
            }
        
            $('#s_188').attr("r_w",$('#input-width').val()*$('[name="size_in"]').val());
            $('#s_188').attr("r_h",$('#input-height').val()*$('[name="size_in"]').val());
            var sin = 'мм';
            if($('[name="size_in"]').val() == 10){
                sin = 'см';
            }
            $('#s_188').prop("checked",true);
//            var r_h = $('#s_188').attr("r_h");
//            var r_w = $('#s_188').attr("r_w");
            width = $('#input-width').val()*$('[name="size_in"]').val();
            height = $('#input-height').val()*$('[name="size_in"]').val();
        
            if($('[name="orient"]:checked').val() == 1){
                if(width < height){
                    var tw = width;
                    width = height;
                    height = tw;
                }
            } else {
                if(height < width){
                    var tw = width;
                    width = height;
                    height = tw;
                }
            }
            $('.show_price.btn-default').removeClass('btn-default').addClass('not-active').css('cursor','not-alowed');
        
            if(parametrLoad != 'product' || (parametrLoad == 'product' && $('#rectangle').length > 0)){
                smena_razmera({
                    'width' : width,
                    'height' : height,
                    'name' : 'Ваш размер',
                    'typePrewiev' : typePrewiev,
                    'ind_razmer' : true
                });
            }
            $(this).blur();
            blockLamSize();
            changeNameGluing();
            blockPostBySize();
            calculate();
            if(!firstLoad){             $("#pricefilters").submit();         }
    });
       $('[name="size_type"]').change(function(){
            if($(this).val() == 2){
                $('#sel-size').stop(true,true).hide(10);
                $('.block_ind_razm').find('input[type=text]').prop('disabled',false);
                $('.block_ind_razm').find('select').prop('disabled',false).trigger("chosen:updated");
                $('#s_188').prop('disabled',false);
                if($('#input-width').val() != '' && $('#input-height').val() != ''){
                    if($('#btn-cont-show-price').hasClass('not-aval') == true){
                        $('#btn-cont-show-price').removeClass('not-aval').stop(true,true).show(300);
                    }
                    $('.show_price.not-active').removeClass('not-active').addClass('btn-default').css('cursor','pointer');
                    $('.show_price').click();
                }
                $('#cont-input-size').stop(true,true).show(300);
                $('#sel_razmer').prop('disabled',true).trigger("chosen:updated");

            }else{
                $('#cont-input-size').stop(true,true).hide(10);
                $('.block_ind_razm').find('input[type=text]').prop('disabled',true);
                $('.block_ind_razm').find('select').prop('disabled',true).trigger("chosen:updated");
                $('#s_188').prop('disabled',true);
                if($('#sel_razmer').prop('disabled') == true) {
                    $('#sel_razmer').prop('disabled',false).change().trigger("chosen:updated");
                }
                $('#sel-size').stop(true,true).show(300);
            }
        });

        $('#sel_razmer').change(function(e) {
            var r_h = +$(this).find('option:selected').attr('r_h');
            var r_w = +$(this).find('option:selected').attr('r_w');
            var r_t = $(this).find('option:selected').attr('r_t');
            width = r_w;
            height = r_h;
            if($('[name="orient"]:checked').val() == 1){
                if(width < height){
                    var tw = width;
                    width = height;
                    height = tw;
                }
            } else {
                if(height < width){
                    var tw = width;
                    width = height;
                    height = tw;
                }
            }
            smena_razmera({
                'width' : r_w,
                'height' : r_h,
                'name' : (!newCat ? r_t : ''),
                'typePrewiev' : typePrewiev
            });
            $('#s_188:not(disabled)').prop('disabled',true);
            calculate();
            blockLamSize();
            changeNameGluing();
            blockPostBySize();
            change_text_info_razmer_print_list();
            if(!firstLoad){             $("#pricefilters").submit();         }
        });
        $('[name="orient"]').change(function(){
            if(+$('#falc').val() == 121){
                $('#falc_short').change();
            }
            if($('[name="orient"]:checked').val() == 1){
                if(+width < +height){
                    var tw = +width;
                    width = +height;
                    height = +tw;
                }
            } else {
                if(+height < +width){
                    var tw = +width;
                    width = +height;
                    height = +tw;
                }
            }
        });
    
        $('#falc_short').on('input', function(){
            if (this.value.match(/[^0-9.]/g)) {
                this.value = this.value.replace(/[^0-9.]/g, '');
            }
        });
        $('#falc_short,#falc_short_2').change(function(){
            if($(this).val() < 35){
                $(this).val(35);
            } else {
                if(+width - +$(this).val() < 35){
                    var temp_val = ((+width - 35) > 35 ? (+width - 35) : 35);
                    $(this).val(+temp_val);
                }
            }
            if(this.id == 'falc_short_2'){
                $('#falc_short').val($(this).val());
            } else {
                $('#falc_short_2').val($(this).val());
            }
            if(parametrLoad != 'product' || (parametrLoad == 'product' && $('#rectangle').length > 0)){
                changepic2();
            }
            $("#pricefilters").submit();
        });

        $('#falc').change(function () {
           if($(this).val() == 121){
               $('.asimetric-div').removeClass('hidden');
               $('#falc_short').prop('disabled',false);
           } else {
               $('.asimetric-div:not(.hidden)').addClass('hidden');
               $('#falc_short').prop('disabled',true);
           }
        });
        $('#size-not-st').change(function(){
                if(!firstLoad){             $("#pricefilters").submit();         }
        });
        $(".sel_post_rab").change(function(){
            if(this.id == 'drilling' && $(this).val() > 0){
                $('.drill-dia.hidden').removeClass('hidden');
                $('#drilling-dia:disabled').prop('disabled',false).change().trigger("chosen:updated");
            } else if(this.id == 'drilling' && $(this).val() == 0){
                $('.drill-dia:not(.hidden)').addClass('hidden');
                $('#drilling-dia').prop('disabled',true).trigger("chosen:updated");
            }
            if(this.id == 'person' && $(this).val() > 0 && $('#scratch').length == 0){
                $('.marking-block.hidden').removeClass('hidden');
                $('#marking:disabled').prop('disabled',false).change().trigger("chosen:updated");
            } else if(this.id == 'person' && $(this).val() == 0){
                $('.marking-block:not(.hidden)').addClass('hidden');
                $('#marking').prop('disabled',true).trigger("chosen:updated");
            }
            if(this.id == 'plot' && $(this).val() > 0){
                $('.picking-block.hidden').removeClass('hidden');
                $('#picking:disabled').prop('disabled',false).change().trigger("chosen:updated");
                $('#shipping:disabled').prop('disabled',false).change().trigger("chosen:updated");
                $('.block-plot.hidden').removeClass('hidden');
                $('#count_format:disabled').prop('disabled',false).change().trigger("chosen:updated");
                $('.sel_post_rab:not(#plot):not(#person):not(#contour)').each(function(){
                    if($(this).val() > 0){
                        $(this).val(0).change().trigger("chosen:updated");
                    }
                });
            } else if(this.id == 'plot' && $(this).val() == 0){
                $('.picking-block:not(.hidden)').addClass('hidden');
                $('#picking:disabled').prop('disabled',true).trigger("chosen:updated");
                $('#shipping:disabled').prop('disabled',true).trigger("chosen:updated");
                $('.block-plot:not(.hidden)').addClass('hidden');
                $('#count_format:not(:disabled)').prop('disabled',false).change().trigger("chosen:updated");
            }
            if(this.id == 'luvers' && $(this).val() == 0){
                $('.luvers-block:not(.hidden)').addClass('hidden');
                $('#count_luvers:disabled').prop('disabled',true);
            } else if(this.id == 'luvers') {
                $('.luvers-block.hidden').removeClass('hidden');
                $('#count_luvers:disabled').prop('disabled',false);
            }
            if(this.id == 'gluing' && $(this).val() > 0){
                $(".sel_post_rab:not(#gluing):not(#person)").each(function(){
                    if($(this).val() != 0){
                        $(this).val(0).change().trigger("chosen:updated");
                    }
                });
                $('.laminat:not(#nolam)').each(function () {
                  if(!$(this).hasClass('disabledGlue')){
                     if($(this).prop('checked')){
                       $(this).prop('checked',true).change();
                     }
                     $(this).prop('disabled',true).addClass('disabledGlue');
                  }
                });
                $('.glue-side.hidden').removeClass('hidden');
                $('#gluing_side:disabled').prop('disabled',false).change().trigger("chosen:updated");
            }  else if(this.id == 'gluing' && $(this).val() == 0){
                $('.disabledGlue').each(function () {
                  $(this).removeClass('disabledGlue');
                  if(!$(this).hasClass('blockKonv') && $(this).hasClass('countDisabled') == false && $(this).hasClass('matDisabled') == false && $(this).hasClass('sideBlock') == false && $(this).hasClass('blockBySize') == false){
                    $(this).prop('disabled',false);
                  }
                });
                $('.glue-side:not(.hidden)').addClass('hidden');
                $('#gluing_side').prop('disabled',true).trigger("chosen:updated");
            } else if(this.id != 'gluing' && this.id != 'person' && $(this).val() > 0){
                if($('#gluing').val() != 0){
                    $('#gluing').val(0).change().trigger("chosen:updated");
                }
            }
            if(this.id == 'contour' && $(this).val() > 0){
                $('.sel_post_rab:not(#contour):not(#person):not(#plot)').each(function(){
                    $(this).val(0).change().trigger("chosen:updated");
                });
                $('.block-cont.hidden').removeClass('hidden');
                $('#count_format_contour:disabled').prop('disabled',false).change();
                $('.block-cont .dop_select').prop('disabled',false).change().trigger("chosen:updated");
            } else if(this.id == 'contour' && $(this).val() == 0){
                $('.block-cont:not(.hidden)').addClass('hidden');
                $('#count_format_contour:not(:disabled)').prop('disabled',false).change();
                $('.block-cont .dop_select').prop('disabled',true).change().trigger("chosen:updated");
            }
            
            if(this.id != 'plot' && this.id != 'contour' && this.id != 'person' && $(this).val() > 0){
                if($('#contour').val() > 0){
                    $('#contour').val(0).change().trigger("chosen:updated");
                }
                if($('#plot').val() > 0){
                    $('#plot').val(0).change().trigger("chosen:updated");
                }
                if($('#lazer').val() > 0){
                    $('#lazer').val(0).change().trigger("chosen:updated");
                }
            }
            if(this.id == 'falc'){
                if($(this).val() == 0){
                    $(this).closest('.falc-block-cont.select-with-ico').removeClass('select-with-ico');
                    $(this).closest('.pw-cont').find('.name-cover').css('line-height', '28px');
                } else {
                    $(this).closest('.falc-block-cont:not(.select-with-ico)').addClass('select-with-ico');
                    $(this).closest('.pw-cont').find('.name-cover').css('line-height', '40px');
                }
            }
            calculate();
            add_opacity_option_reset();
            blockKonvert();
            
            if(!firstLoad){             $("#pricefilters").submit();         }
            if(parametrLoad != 'product' || (parametrLoad == 'product' && $('#rectangle').length > 0)){
                changepic2();
            }
        });
        $('.t_switchable input').change(function(){
            blockNullGroup();
        });
        function blockNullGroup(){
            $('.show-element').each(function(){
                var parent = '[data-toggle-child="' + this.id + '"]';
                if($(this).find('.t_switchable input:not(:disabled)').length == 0){
                    $(parent).closest('.t_switchable:not(.imitate-disabled)').addClass('imitate-disabled');
                } else {
                    $(parent).closest('.t_switchable.imitate-disabled').removeClass('imitate-disabled');
                }
            });
        }
         $('.new_color_option').change(function(){
             blockerColor();
             if(!firstLoad){             $("#pricefilters").submit();         }
         });
        $('.dop_select,.size_scratch').change(function(){
            if(parametrLoad != 'product' || (parametrLoad == 'product' && $('#rectangle').length > 0)){
                changepic2();
            }
            calculate();
            if(!firstLoad){             $("#pricefilters").submit();         }
        });
        $('.input-option').on("input change",function(){
            var selector = '[data-controlled-item=' + this.id + '-selected]';
            if (this.value.match(/[^0-9]/g)) {
                this.value = this.value.replace(/[^0-9]/g, '');
            }
            if($(this).val() < 1 && $(this).val() != ''){$(this).val(1).change();}
            add_opacity_option_reset();
        });
        $('.input-option').on('focusout',function(){
            if($(this).val() == ''){$(this).val(1).change();}
        });
        $('.input-option').on("change",function(){
            if(!firstLoad){             $("#pricefilters").submit();         }
            if(this.id == 'sets'){
//                if($(this).val() != 1){$('#text_info_sets').css('display','block');}else{$('#text_info_sets').css('display','none');}
                if($(this).val() > 50){$(this).val(50).change();}
            }
            add_opacity_option_reset();
         });
        $('.plus-input-val').click(function(){
            var selector = $(this).attr("data-change-input");
            $('#' + selector).val(parseInt($('#' + selector).val()) + 1).change();
        });
        $('.minus-input-val').click(function(){
             var selector = $(this).attr("data-change-input");
            $('#' + selector).val(parseInt($('#' + selector).val()) - 1).change();
        });
        $('.orient').change(function(){
            if(!firstLoad){             $("#pricefilters").submit();         }
        });
        $('body').on('submit','[name=form_discount],[name="form_discount_download"]',function(){
            return false;
        });
        $('.print-price').click(function(){
            if($(this).hasClass('icon_print')){
                printClick = 'print';
            } else if($(this).hasClass('icon_exel')) {
                printClick = 'excel';
            }
        });
        $('.input_discount').on('input', function(){
            if (this.value.match(/[^0-9.]/g)) {
                this.value = this.value.replace(/[^0-9.]/g, '');
            }
        });
        $('body').on('click','#m_d_submit',function(){
            $('.d_m_d_variant').eq( $('.m_d_variant:checked').val() ).prop('checked',true);
            $('#d_m_d_all').val( $('#m_d_all').val() );
            $('#d_m_d_t_1').val( $('#m_d_t_1').val() );
            $('#d_m_d_t_2').val( $('#m_d_t_2').val() );
            $('#d_m_d_t_3').val( $('#m_d_t_3').val() );
            $('#d_m_d_t_4').val( $('#m_d_t_4').val() );
            $('#d_m_d_t_5').val( $('#m_d_t_5').val() );
            $('#d_m_d_t_6').val( $('#m_d_t_6').val() );
            $(this).closest('.modal').modal('hide');
            if(!firstLoad){             $("#pricefilters").submit();         }
        });
        $('body').on('click','#d_m_d_submit',function(){
            $(this).closest('.modal').modal('hide');
            pricePints(printClick);
        });
        $('body').on('change keyup','#m_d_all',function(){
            $('#m_d_t_1,#m_d_t_2,#m_d_t_3,#m_d_t_4,#m_d_t_5,#m_d_t_6').val($(this).val());
        });
        $('body').on('change keyup','#m_d_t_1,#m_d_t_2,#m_d_t_3,#m_d_t_4,#m_d_t_5,#m_d_t_6',function(){
            $('#m_d_all').val('');
        });
        $('body').on('change keyup','#d_m_d_all',function(){
            $('#d_m_d_t_1,#d_m_d_t_2,#d_m_d_t_3,#d_m_d_t_4,#d_m_d_t_5,#d_m_d_t_6').val($(this).val());
        });
        $('body').on('change keyup','#d_m_d_t_1,#d_m_d_t_2,#d_m_d_t_3,#d_m_d_t_4,#d_m_d_t_5,#d_m_d_t_6',function(){
            $('#d_m_d_all').val('');
        });
        $("#option-reset").click(function(){
            if($('#drilling-dia').val() != 5){
                $('#drilling-dia').val(5).change().trigger("chosen:updated");
            }
            if($('#drilling-dia').val() != 0){
                $('#drilling-dia').val(0).change().trigger("chosen:updated");
            }
            if($('#sets').val() != 1){
                $('#sets').val(1).change();
            }
            $('.sel_post_rab:not(#dop_new_felling)').each(function(){
                if(($(this).val() != 0)){
                    $(this).val(0).change().trigger("chosen:updated");
                }
            });
            add_opacity_option_reset();
        });
        $('#drilling-dia').change(function(){
            $(this).closest('.drill-dia').find('.selected-show').html('Диаметр сверления ' + $(this).val() + ' мм');
            if(!firstLoad){             $("#pricefilters").submit();         }
        });
        $('#picking,#shipping').change(function(){
            calculate();
            if(!firstLoad){             $("#pricefilters").submit();         }
        });
        $('#gluing_side').change(function(){
            if(!firstLoad){             $("#pricefilters").submit();         }
        });
        $('#count_format,#count_format_contour,#count_luvers').change(function(){
            calculate();
            if(!firstLoad){             $("#pricefilters").submit();         }
        });
    $('#count_format_contour').change(function(){
        if(parametrLoad != 'product' || (parametrLoad == 'product' && $('#rectangle').length > 0)){
            changepic2();
        }
    });
    $('[name="size_in"]').change(function(){
         var wid = $('#input-width').val(),
            hei = $('#input-height').val();
        if( wid != '' && hei != '' ){
            if( $(this).val() == 10 ){
                $('#input-width').val(wid/10).trigger('input');
                $('#input-height').val(hei/10).trigger('input');
            }else{
                $('#input-width').val(wid*10).trigger('input');
                $('#input-height').val(hei*10).trigger('input');
            }
        }
         $('.show_price').click();
//        if(!firstLoad && $('#s_30:not(:disabled)').length > 0){             $("#pricefilters").submit();         }
         
    });
    $('#konv_lam').change(function(){
        if($(this).val() == 1){
            $('.laminat').each(function(){
                if(konv_lam.indexOf(+$(this).val()) == -1){
                    if($(this).prop('checked') == true){
                        $(this).prop('checked',false).change();
                    }
                    if($(this).hasClass('blockKonv') == false){
                        $(this).addClass('blockKonv').prop('disabled',true);
                    }
                }
            });
            if($('.laminat:checked').length == 0){
                $('#l_33').prop('checked',true).change();
            }
            $('.digital-filter:not(.sel-konvert)').addClass('sel-konvert');
            if($('#perforation').val() > 0){
                $('#perforation').val(0).change();
            }
            if($('#falc').val() > 0){
                $('#falc').val(0).change();
            }
            if($('#creasing').val() > 0){
                $('#creasing').val(0).change();
            }
            if($('#plot').val() > 0){
                $('#plot').val(0).change();
            }
            if($('#contour').val() > 0){
                $('#contour').val(0).change();
            }
            if($('#gluing').val() > 0){
                $('#gluing').val(0).change();
            }
            $('#perforation,#falc,#creasing,#plot,#contour,#gluing').prop('disabled',true).trigger("chosen:updated");
        } else {
            $('.laminat.blockKonv').each(function(){
                if(!$(this).hasClass('disabledGlue') && $(this).hasClass('countDisabled') == false && $(this).hasClass('matDisabled') == false && $(this).hasClass('sideBlock') == false && $(this).hasClass('blockBySize') == false){
                    $(this).prop('disabled',false);
                }
                $(this).removeClass('blockKonv');
            });
            $('.digital-filter.sel-konvert').removeClass('sel-konvert');
            $('#perforation,#falc,#creasing,#plot,#contour,#gluing').prop('disabled',false).trigger("chosen:updated");
        }

        blockKonvert();
        calculate();
    });
    
    function blockKonvert(){
        var nK = ['perforation','falc','creasing','plot','contour','gluing'];
        var counter = 0;
        for(var i = 0; i < nK.length; i++){
            if($('#' + nK[i]).val() > 0){
                counter++;
            }
        }
        if(counter > 0){
            for(var i = 0; i < konv_lam.length; i++){
                $('#l_' + konv_lam[i] + ':not(.disabledPost)').addClass('disabledPost').prop('disabled',true).prop('checked',false).change();
            }
        } else {
            $('.laminat.disabledPost:not(.disabledGlue):not(.matDisabled):not(.sideBlock):not(.blockKonv):not(.countDisabled):not(.blockBySize):not(.blockMatKonv)').prop('disabled',false);
            $('.laminat.disabledPost').removeClass('disabledPost');
        }
    }

    function add_opacity_option_reset(){
        var allNull = true;
        $('.sel_post_rab:not(#dop_new_felling)').each(function(){
             if(($(this).val() != 0)){
                allNull = false;
            }
        });
        if($('#sets').val() != 1){
            allNull = false;
        }
        if(allNull){
            $('#option-reset:not(.reset_no_active)').addClass('reset_no_active');
        } else {
            $('#option-reset.reset_no_active').removeClass('reset_no_active');
        }
    }
    if($(window).width() < 991){
        $('.header-opt').click(function(){
            if($(this).hasClass('openChild') == false){
                $(this).addClass('openChild');
                $(this).closest('.mobile-control').find('.mobile-droped').stop(true,true).animateCssShow('slideMenuDown');
                $(this).closest('.mobile-control').find('.icon-drop .fa-plus').fadeOut(250);
                $(this).closest('.mobile-control').find('.icon-drop .fa-minus').fadeIn(250);
            } else {
                $(this).removeClass('openChild');
                $(this).closest('.mobile-control').find('.mobile-droped').stop(true,true).animateCssHide('slideMenuTop');
                $(this).closest('.mobile-control').find('.icon-drop .fa-minus').fadeOut(250);
                $(this).closest('.mobile-control').find('.icon-drop .fa-plus').fadeIn(250);
            }

        });
    } else {
        padT();
        var maxTop = $('#fixed-head').offset().top + $('#fixed-head').height() + 30;
        var maxBottom = $('#fixed-head').offset().top - 30;
        var widthHead = $('#fixed-head').width();
        $('#fixed-head .fixer>td,#fixed-head').css('width',widthHead);
        $(window).scroll(function(){
            if($(window).scrollTop() >= maxTop){
               $('#fixed-head:not(.fixed)').addClass('fixed').animateCss('slideInDown','');
            } else if($(window).scrollTop() < maxBottom) {
                 $('#fixed-head.fixed').removeClass('fixed').animateCss('slideInUp','');
            }
        });
    }
    /*FUNCTIONS*/
    function calculate(){
        var countPost = 0;
        if($('#sel_razmer:disabled').length > 0){
             var $mult1=Math.floor(450/(width)) * Math.floor(320/(height));
             var $mult2=Math.floor(450/(height)) * Math.floor(320/(width));
            if($mult1 > $mult2){var na_liste = $mult1;}else{var na_liste  = $mult2;}
            if(na_liste<1){na_liste=1;}
        } else {
            var na_liste = +$('#sel_razmer option:selected').attr('na_liste');
        }
        $('.sel_post_rab:not(#konv_lam)').each(function(){
                if(this.id == 'perforation' || this.id == 'drilling'){
                    var fix = 3;
                } else {
                    var fix = 2;
                }
                if($(this).val() != 0){
                    if(this.id != 'falc' && this.id != 'plot' && this.id != 'contour'  && this.id != 'lazer' && this.id != 'gluing'){
                        var text_cost = '+ ' + (+(postUpPrice($(this).attr('name'),width,height,$(this).attr('data-price-post'))/1000*(this.id != 'curve' && this.id != 'luvers' ? $(this).val() : (this.id == 'luvers' ? +$('#count_luvers').val() : 1)))).toFixed(fix) + ' грн/шт' + ' (минимум ' + $(this).attr('data-min-price')  + ' грн)';
                    } else if(this.id == 'person'){
                        var text_cost = '+ ' + $(this).attr('data-price-post')+' грн';
                    } else if(this.id == 'falc'){
                        var text_cost = '+ ' + (postUpPrice($(this).attr('name'),width,height,+$(this).find('option:selected').attr('data-post-falc'))/1000).toFixed(2) +' грн/шт до 150 г/м2; ' + ((postUpPrice($(this).attr('name'),+width,+height,+$(this).find('option:selected').attr('data-post-falc'))/1000 + postUpPrice('dop_creasing',+width,+height,+$(this).find('option:selected').attr('data-post-big'))/1000).toFixed(2)) +' грн/шт 170 г/м2';
                    } else if(this.id == 'plot'){
                        var squereList = (width/1000*height/1000);
                        var minP = $(this).find('option:selected').attr('data-min-price');
                        var S_kv = squereList/$('#count_format').val();
                        var l_kv = Math.sqrt(S_kv);
                        var P_kv = l_kv*4;
                        var P_all_kv = P_kv*$('#count_format').val();
                        var price = (P_all_kv*1*($(this).val() == 2 ? 1.5 : 1)).toFixed(2);
                        if($('#shipping').length > 0 && $('#shipping').val() == 1) {
                            minP = (10).toFixed(2);
                        }
                        var text_cost = '+ ' + price + ' грн/шт' + ' (минимум ' + minP + ' грн)';

                    } else if(this.id == 'contour'){
                        var squereList = (width/1000*height/1000);
                        var S_kv = squereList/( $('#count_format_contour').length ? $('#count_format_contour').val() : 1);
                        var l_kv = Math.sqrt(S_kv);
                        var P_kv = l_kv*4;
                        var P_all_kv = P_kv*( $('#count_format_contour').length ? $('#count_format_contour').val() : 1);
                        var price = (P_all_kv*3*($(this).val() == 2 ? 1.5 : 1)).toFixed(2);

                         var text_cost = '+ ' + price + ' грн/шт' + ' (минимум ' + $(this).find('option:selected').attr('data-min-price')  + ' грн)';
                    } else if(this.id == 'gluing'){
                        var text_cost = '+ ' + (((+postUpPrice($(this).attr('name'),width,height,+$(this).attr('data-price-post')/1000))*$(this).val())).toFixed(2) + ' грн за блок (минимум '+ $(this).attr('data-min-price') +' грн). + 3 дня к сроку изготовления';
                    } else {
                        var price = postUpPrice($(this).attr('name'),width,height,$(this).find('option:selected').attr('data-post-cost'));
                        var text_cost = '+ ' + price + ' ' + $(this).find('option:selected').attr('data-post-text') + ' грн/шт' + ' (минимум ' + $(this).find('option:selected').attr('data-min-price')  + ' грн)';
                    }
                    $(this).closest('.pw-cont').find('.selected-show').html(text_cost).show();
                    countPost++;
                } else {
                    $(this).closest('.pw-cont').find('.selected-show').html('').hide();
                }
        });
        if($('#picking').val() > 0){
            $('#picking').closest('.pw-cont').find('.selected-show').html('+ ' + ($('#picking').attr('data-price-post')/(na_liste*$('#count_format').val())).toFixed(2) + ' грн/шт' + ' (минимум ' + $('#picking').attr('data-min-price')  + ' грн)');
        } else {
            $('#picking').closest('.picking-block').find('.selected-show').html('');
        }
        if($('#marking').val() > 0){
            $('#marking').closest('.marking-block').find('.selected-show').html('+ 50 грн 1000 шт').show();
        } else {
            $('#marking').closest('.marking-block').find('.selected-show').html('').hide();
        }
    }

    function tmpHide(){
        $('.material').each(function(){
            var contRowAll = $(this).find('.color-row').length;
            var contRowHide = $(this).find('.color-row.hidden').length;
            if(contRowAll == contRowHide){
                $(this).addClass('hidden-by-print');
            } else {
                $(this).removeClass('hidden-by-print');
            }
        });
    }
    function changeNameGluing(){
        if(width == height){
            var text1 = 'Слева';
            var text2 = 'Сверху';
        } else {
            var text1 = 'По короткой стороне';
            var text2 = 'По длинной стороне';
        }
        $('#gluing_side option[value=1]').html(text1);
        $('#gluing_side option[value=2]').html(text2);
        $('#gluing_side').trigger("chosen:updated")
    }
    function pricePints(type){
        var dateNow = new Date();
        var day = dateNow.getDate();
        var month = dateNow.getMonth() + 1;
        var year = dateNow.getFullYear();
        if(activeP != 0){
            xhrP.abort();
            activeP = 0;
        }
        var dataSend =  $("#pricefilters").serialize() + '&' + $('[name="form_discount_download"]').serialize();
        xhrP = $.ajax({
              type: "POST",
              url: "cside/ajax.php?action=price_digital&type=print",
              data: dataSend,
              dataType: 'json',
              beforeSend: function(){
                activeP++;
              },
              success: function(data){
                if(type == 'print'){
                    $(com(data.text)).print({
                        globalStyles: true,
                        mediaPrint: false,
                        stylesheet: "cside/print-css.css",
                        noPrintSelector: ".no-print",
                        iframe: true,
                        append: null,
                        prepend: null,
                        manuallyCopyFormValues: true,
                        deferred: $.Deferred(),
                        timeout: 750,
                        title: null,
                        doctype: '<!doctype html>'
                    });
                } else if(type == 'excel') {
                    tableToExcel(com(data.text));
                }
                  activeP=0;
                  xhrP = '';
              }
        });
        return false;
    };
    function padT(){
        $(this).stop(true,true).delay(300).queue(function(){
            var pdt =  ($('.container-main-top-block').height() - $('.gallery .white-block-digital').height()-30)/2;
            $('.img-preview').css({'padding-top': pdt,'padding-bottom': pdt});
            var removeOpacity = function(){
                $('.gallery').css('opacity',1);
            };
            $('.gallery').animateCss('fadeIn',removeOpacity);
            $(this).dequeue();

        });
    }
    function com(compressed) {
        "use strict";
        var i,
            dictionary = [],
            w,
            result,
            k,
            entry = "",
            dictSize = 256;
        for (i = 0; i < 256; i += 1) {
            dictionary[i] = String.fromCharCode(i);
        }

        w = String.fromCharCode(compressed[0]);
        result = w;
        for (i = 1; i < compressed.length; i += 1) {
            k = compressed[i];
            if (dictionary[k]) {
                entry = dictionary[k];
            } else {
                if (k === dictSize) {
                    entry = w + w.charAt(0);
                } else {
                    return null;
                }
            }

            result += entry;

            // Add w+entry[0] to the dictionary.
            dictionary[dictSize++] = w + entry.charAt(0);

            w = entry;
        }
        function decode_utf8(s) {
          return decodeURIComponent(escape(s));
        }
        return decode_utf8(result);
    }
    function postUpPrice($namePost,$width,$height,$price){
        switch($namePost){
            case 'dop_curve':
            case 'dop_drilling':
                if(($width >= 50 && $height >= 85) || ($height >= 50 && $width >= 85)){
                    return $price;
                } else{
                    return $price*1.5;
                }
                break;
            case 'dop_falc':
                if(($width >= 100 && $height >= 120) || ($height >= 100 && $width >= 120)){
                    return $price;
                } else {
                    return $price*2;
                }
                break;
            case 'dop_creasing':
            case 'dop_perforation':
                if(($width >= 100 && $height >= 120) || ($height >= 100 && $width >= 120)){
                    return $price;
                } else if(($width >= 50 && $height >= 85) || ($height >= 50 && $width >= 85)) {
                    return $price*1.5;
                } else {
                    return $price*2;
                }
                break;
            default:
                return $price;
                break;
        }
    }
    function export2Word( element ) {
       var html, link, blob, url, css;
       var dateNow = new Date();
       var day = dateNow.getDate();
       var month = dateNow.getMonth() + 1;
       var year = dateNow.getFullYear();
       css = (
         '<style>' +
         '@page WordSection1{size: 841.95pt 595.35pt;mso-page-orientation: landscape;}' +
         'div.WordSection1 {page: WordSection1;}' +
         '</style>'
       );

       html = element;
       blob = new Blob(['\ufeff', css + html], {
         type: 'application/msword'
       });
       url = URL.createObjectURL(blob);
       link = document.createElement('A');
       link.href = url;
       link.download = 'sborkaPrice_' + day + '-' + (month < 10 ? 0 : '') + month + '-' + year + '.doc';  // default name without extension
       document.body.appendChild(link);
       if (navigator.msSaveOrOpenBlob ) navigator.msSaveOrOpenBlob( blob, 'sborkaPrice_' + day + '-' + (month < 10 ? 0 : '') + month + '-' + year + '.doc'); // IE10-11
           else link.click();  // other browsers
       document.body.removeChild(link);
     };
    function tableToExcel( element ) {
       var html, link, blob, url, css;
       var dateNow = new Date();
       var day = dateNow.getDate();
       var month = dateNow.getMonth() + 1;
       var year = dateNow.getFullYear();
       var template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>' + $(element).html() + '</table></body></html>' ;
       blob = new Blob(['\ufeff',css + template], {
         type: 'application/vnd.ms-excel'
       });
       url = URL.createObjectURL(blob);
       link = document.createElement('A');
       link.href = url;
       link.download = 'sborkaPrice_' + day + '-' + (month < 10 ? 0 : '') + month + '-' + year + '.xls';  // default name without extension
       document.body.appendChild(link);
       if (navigator.msSaveOrOpenBlob ) navigator.msSaveOrOpenBlob( blob, 'sborkaPrice_' + day + '-' + (month < 10 ? 0 : '') + month + '-' + year + '.xls'); // IE10-11
           else link.click();  // other browsers
       document.body.removeChild(link);
     };
    function add_opacity_filter_reset(){
        var checkM = $('#selectall:checked').length + $('.select-mat:checked').length;
        var checkC = $('#alltime:checked').length + $('.terms:checked').length;
        var checkT = $('#allcolor:checked').length + $('.colors:checked').length;
        var checkL = $('.laminat:checked').length;
        if($('#konv_lam').val() > 0){
            checkL = checkL - 1;
        };
        var sum =   checkM + checkC + checkT + checkL;
        if( sum == 0 )
        {
            $('#filter-reset').addClass('reset_no_active');
        }else{
            $('#filter-reset').removeClass('reset_no_active');
        }
    }
    function loadPage(){
        $(this).queue(function(){
                var colorArray8 = {
                    'white': {1:8,2:9,5:10,6:11},
                    'lack' : {1:12,5:13}
                };
                var dop_colors = {
                    1: [1,1],
                    2: [2,1],
                    3: [1,2],
                    4: [2,2],
                    5: [3,1],
                    6: [3,2]
                };
                $('.colors:checked').prop('checked',false);
                if(product.color != 8){
                    $('#color' + product.color).prop('checked',true).change();
                }
                    if(product.white > 0){
                        if(typeof colorArray8['white'][product.white] !== "undefined" && product.color == 8){
                            $('#color' + colorArray8['white'][product.white]).prop('checked',true).change();
                        } else {
                            var workArrCol = dop_colors[product.white];
                            $('#white-select').val(workArrCol[0]).change().trigger("chosen:updated");
                            $('#count_add_white').val(workArrCol[1]).change().trigger("chosen:updated");
                        }
                    }
                    if(product.lack > 0){
                        if(typeof colorArray8['lack'][product.lack] !== "undefined" && product.color == 8){
                            $('#color' + colorArray8['lack'][product.lack]).prop('checked',true).change();
                        } else {
                            var workArrCol = dop_colors[product.lack];
                            console.log(workArrCol[0])
                            $('#cl-select').val(workArrCol[0]).change().trigger("chosen:updated");
                            $('#count_add_cl').val(workArrCol[1]).change().trigger("chosen:updated");
                        }
                    }
                $('.terms:checked').prop('checked',false);
                $('#term' + product.time).prop('checked',true).change();
                if(product.mat > 0){
                     $('#m_' + product.mat).prop('checked',true).change();
                }
                if(product.lam > 0){
                    $('#l_' + product.lam).prop('checked',true).change();
                } else {
                    $('#nolam').prop('checked',true).change();
                }
            $('#sets').val(product.sets).change();
            $('[name="orient"]').val(product.orient).change();
            if(product.konv_lam > 0){
                $('#konv_lam').val(product.konv_lam).change();
            }
            $(this).dequeue();
        });
        $(this).queue(function(){
            if(parametrLoad != 'product'){
                if(product.product_p.size == 188){
                    $('[name="size_type"][value="2"]').queue(function(){
                        $('[name="size_type"][value="2"]').prop('checked',true).change();
                        $(this).dequeue();
                    });
                    $('#input-width').val(product.product_p.width).change();
                    $('#input-height').val(product.product_p.height).change();
                    $('.show_price').removeClass('not-active').click();
                    $('[name="size_in"]').chosenReadonly(true);
                 } else if($('#sel_razmer option[value=' + product.product_p.size + ']').length != 0){
                     $('#sel_razmer option[value=' + product.product_p.size + ']').prop('selected',true);
                     $('#sel_razmer').change().trigger("chosen:updated");
                     $('[name="size_type"][value="1"]').prop('checked',true).change();
                 }
            } else {
                 $('#sel_razmer').change().chosenReadonly(true);
             }
             for(post in product.post){
                 if(product.post[post] != 0){
                     if($('#' + post.substring(4)).prop("tagName") == 'SELECT'){
                        $('#' + (post.substring(4) != 'plotter' ? post.substring(4) : 'plot')).val(product.post[post]);
                        $('#' + (post.substring(4) != 'plotter' ? post.substring(4) : 'plot')).change().trigger("chosen:updated");
                        if(post.substring(4) == 'drilling'){
                            $('#drilling-dia').val(product.drilling_dia).change();
                            $('#drilling-dia').trigger("chosen:updated");
                        }
                        if(post.substring(4) == 'plot' || post.substring(4) == 'plotter'){
                            var arrayPlot = ['shipping','picking','count_format']
                            selPost(arrayPlot);
                        }
                        if(post.substring(4) == 'contour'){
                            var arrayCountour = ['contour_big','contour_perf','contour_cut','contour_figcut','count_format_contour'];
                            selPost(arrayCountour);
                        }
                        if(post.substring(4) == 'person'){
                            if(product.marking > 0){
                                $('#marking').val(product.marking).change().trigger("chosen:updated");
                            }
                        }
                        if(post.substring(4) == 'falc'){
                            selPost(['falc_short']);
                        }
                     } else {
                         $('#' + post.substring(4) + '[value="' + product.post[post] + '"]').prop('checked',true).change();
                     }
                }             
             }

            $(this).dequeue();
        });
        $(this).queue(function(){
            $("#pricefilters").submit();
            firstLoad = false;
            $(this).dequeue();
        });
    }
    function selPost(arrpost){
        for(var i=0; i < arrpost.length; i++){
            if (typeof product[arrpost[i]] !== "undefined") {
                if($('#' + arrpost[i]).prop("tagName") == 'SELECT'){
                    $('#' + arrpost[i]).val(product[arrpost[i]]).change().trigger("chosen:updated");
                } else if($('#' + arrpost[i]).prop("tagName") == 'INPUT' && $('#' + arrpost[i]).attr("type") == 'text'){
                    $('#' + arrpost[i]).val(product[arrpost[i]]).change();
                } else {
                    $('#' + arrpost[i] + '[value="' + product[arrpost[i]] + '"]').prop('checked',true).change();
                }
            }
        }
    }
    /*BLOCKER FUNCTION*/
    function blockColor(){
        var flagC = 0;
        var twoSideColors = [2,4,10,11,13];
        if($('.select-mat:checked').length != 0){
            var selector = '.select-mat:checked';
        } else {
            var selector = '.select-mat:not(:disabled)';
        }
        $(selector).each(function(){
            if(oneSide.indexOf(+$(this).val()) == -1){
                flagC++;
            }
        });
        if(flagC == 0){
            for(var i=0; i < twoSideColors.length; i++){
                if($('#color' + twoSideColors[i]).prop('checked')){
                    $('#color' + twoSideColors[i]).prop('checked',false).change();
                }
                $('#color' +twoSideColors[i]).prop('disabled',true).addClass('blockSide');
            }
            $('.select_dop_colors').each(function(){
               if(+$(this).val() == 3 || +$(this).val() == 2){
                   $(this).val(0).change();
               }
            });
            $('.select_dop_colors option[value="3"],.select_dop_colors option[value="2"]').prop('disabled',true);
            $('.select_dop_colors').trigger("chosen:updated");
        } else{
            for(var i=0; i < twoSideColors.length; i++){
                $('#color' + twoSideColors[i] + ':not(.blockUF):not(.disabledDop):not(.disabledFilm)').prop('disabled',false);
                $('.blockSide').removeClass('.blockSide');
            }
            $('.select_dop_colors option:disabled').prop('disabled',false);
            $('.select_dop_colors').trigger("chosen:updated");
        }
    }
    function blockLamSize(){
        if((width <= 297 && height <= 420) || (width <= 420 && height <= 297)){
            $('#konv_lam option[value=1]').prop('disabled',false);
             $('#konv_lam').trigger("chosen:updated")
            $('.laminat').each(function(){
               if(konv_lam.indexOf(+$(this).val()) != -1 && $(this).hasClass('blockBySize')){
                   $(this).removeClass('blockBySize');
                   if(!$(this).hasClass('disabledGlue') && !$(this).hasClass('matDisabled') && !$(this).hasClass('sideBlock') && !$(this).hasClass('blockKonv') && !$(this).hasClass('disabledPost') && !$(this).hasClass('countDisabled') && $(this).hasClass('blockBySize') == false && $(this).hasClass('blockMatRul') == false && $(this).hasClass('blockMatKonv') == false  && $(this).hasClass('blockMatLack') == false && $(this).hasClass('disabledCl1') == false && $(this).hasClass('disabledCl2') == false){
                       $(this).prop('disabled',false);
                   }
               }
            });

        } else {
            if($('#konv_lam').val() > 0){
                $('#konv_lam').val(0).change();
            }
            $('#konv_lam option[value=1]').prop('disabled',true);
             $('#konv_lam').trigger("chosen:updated")
             $('.laminat').each(function(){
                if(konv_lam.indexOf(+$(this).val()) != -1 && !$(this).hasClass('blockBySize')){
                    $(this).prop('checked',false).addClass('blockBySize').change().prop('disabled',true);
                }
             });
        }
    }
    function blockPostBySize() {
        if (((width <= 306 && height <= 436) || (height <= 306 && width <= 436)) || (!$('#sel_razmer').prop('disabled') && $('#sel_razmer').val() == 187) || (!$('#sel_razmer').prop('disabled') && $('#sel_razmer').val() == 186)) {
            $('.select-mat.disabledSize:not(.postDisabled):not(.lamDisabled):not(.countDisabled):not(.blockLamRul):not(.blockLamKonv):not(.blockLamLack):not(.disabledWhite):not(.disabledFigcutting):not(.disabledCl)').prop('disabled', false);
            $('.select-mat.disabledSize').removeClass('disabledSize');
            $('.select-mat.disabledSize1:not(.postDisabled):not(.lamDisabled):not(.countDisabled):not(.blockLamRul):not(.blockLamKonv):not(.blockLamLack):not(.disabledWhite):not(.disabledFigcutting):not(.disabledCl)').prop('disabled', false);
            $('.select-mat.disabledSize1').removeClass('disabledSize1');
            $('#gluing:not(.productDisabled)').prop('disabled', false).trigger("chosen:updated");
            $('#drilling:not(.productDisabled)').prop('disabled', false).trigger("chosen:updated");
            blockNullGroup();
        } else {
            if ($('#drilling').val() > 0) {
                $('#drilling').val(0).change();
            }
            $('#drilling').prop('disabled', true).trigger("chosen:updated");
            if ($('#gluing').val() > 0) {
                $('#gluing').val(0).change();
            }
            $('#gluing').prop('disabled', true).trigger("chosen:updated");
            $('.select-mat').each(function() {
                if (canBanner.indexOf(+$(this).val()) == -1) {
                    if ($(this).prop('checked')) {
                        $(this).prop('checked', false).change();
                    }
                    if (!$(this).hasClass('disabledSize')) {
                        $(this).prop('disabled', true).addClass('disabledSize');
                    }
                }
            });
            if (width > 472 || height > 472) {
                for (var i = 0; i < notBan.length; i++) {
                    $('#m_' + notBan[i] + ':checked').prop('checked', false).change();
                    $('#m_' + notBan[i] + ':not(.disabledSize1)').prop('disabled', true).addClass('disabledSize1');
                }
            } else {
                $('.select-mat.disabledSize1:not(.postDisabled):not(.lamDisabled):not(.countDisabled):not(.blockLamRul):not(.blockLamKonv):not(.blockLamLack):not(.disabledWhite):not(.disabledFigcutting):not(.disabledCl)').prop('disabled', false);
                $('.select-mat.disabledSize1').removeClass('disabledSize1');
            }
            blockNullGroup();
        }
        if ((width > 297 && height > 210) || (width > 210 && height > 297)) {
            if ($('#curve').val() > 0) {
                $('#curve').val(0).change();
            }
            $('#curve').prop('disabled', true).trigger("chosen:updated");
        } else {
            $('#curve:not(.productDisabled)').prop('disabled', false).trigger("chosen:updated");
        }
    }
    function blockLam(){
        var flagC = 0;
        if($('.select-mat:checked').length != 0){
            var selector = '.select-mat:checked';
        } else {
            var selector = '.select-mat:not(:disabled)';
        }
        $(selector).each(function(){
            if(oneSide.indexOf(+$(this).val()) == -1){
                flagC++;
            }
        });

        if(flagC == 0){
            for(var i = 0; i < lam_2.length; i++){
                if($('#l_' + lam_2[i]).prop('checked')){
                    $('#l_' + lam_2[i]).prop('checked',false).change();
                }
                $('#l_' + lam_2[i]).prop('disabled',true).addClass('sideBlock');
            }
        } else{
            $('.sideBlock').each(function(){
                if($(this).hasClass('countDisabled') == false && $(this).hasClass('matDisabled') == false && $(this).hasClass('blockKonv') == false && $(this).hasClass('disabledPost') == false && $(this).hasClass('blockBySize') == false && !$(this).hasClass('blockMatRul') && !$(this).hasClass('blockMatKonv')  && $(this).hasClass('blockMatLack') == false && $(this).hasClass('disabledCl1') == false && $(this).hasClass('disabledCl2') == false){
                    $(this).prop('disabled',false);
                }
                $(this).removeClass('sideBlock');
            });
        }
    }
    
    
    function blockAllRullLam(){
        var flagC = 0;
        if($('.select-mat:checked').length != 0){
            var selector = '.select-mat:checked';
        } else {
            var selector = '.select-mat:not(:disabled)';
        }
        $(selector).each(function(){
            if(noRulLam.indexOf(+$(this).val()) == -1){
                flagC++;
            }
        });
        if(flagC == 0){
            for(var i = 0; i < rulLam.length; i++){
                if($('#l_' + rulLam[i]).prop('checked')){
                    $('#l_' + rulLam[i]).prop('checked',false).change();
                }
                $('#l_' + rulLam[i]).prop('disabled',true).addClass('blockMatRul');
            }
        } else{
            $('.blockMatRul').each(function(){
                if($(this).hasClass('countDisabled') == false && $(this).hasClass('matDisabled') == false && $(this).hasClass('blockKonv') == false && $(this).hasClass('disabledPost') == false && $(this).hasClass('blockBySize') == false && $(this).hasClass('disabledCl1') == false && $(this).hasClass('disabledCl2') == false){
                    $(this).prop('disabled',false);
                }
                $(this).removeClass('blockMatRul');
            });
        }
    }
    function blockAllLackLam(){
        var flagC = 0;
        if($('.select-mat:checked').length != 0){
            var selector = '.select-mat:checked';
        } else {
            var selector = '.select-mat:not(:disabled)';
        }
        $(selector).each(function(){
            if(notLackLam.indexOf(+$(this).val()) == -1){
                flagC++;
            }
        });
        if(flagC == 0){
            for(var i = 0; i < lackLam.length; i++){
                if($('#l_' + lackLam[i]).prop('checked')){
                    $('#l_' + lackLam[i]).prop('checked',false).change();
                }
                $('#l_' + lackLam[i]).prop('disabled',true).addClass('blockMatLack');
            }
        } else{
            $('.blockMatLack').each(function(){
                if($(this).hasClass('countDisabled') == false && $(this).hasClass('matDisabled') == false && $(this).hasClass('blockKonv') == false && $(this).hasClass('disabledPost') == false && $(this).hasClass('blockBySize') == false && $(this).hasClass('blockMatRul') == false && $(this).hasClass('disabledCl1') == false && $(this).hasClass('disabledCl2') == false){
                    $(this).prop('disabled',false);
                }
                $(this).removeClass('blockMatLack');
            });
        }
    }
    function blockAllNotLackMat(){
        var flagC = 0;
        var selector = '';
        if($('.laminat:checked').length != 0){
            selector = '.laminat:checked';
        }
        if(selector != ''){
            $(selector).each(function(){
                if(lackLam.indexOf(+$(this).val()) != -1){
                    flagC++;
                }
            });
        }
        if(flagC > 0){
            for(var i = 0; i < notLackLam.length; i++){
                if($('#m_' + notLackLam[i]).prop('checked')){
                    $('#m_' + notLackLam[i]).prop('checked',false).change();
                }
                $('#m_' + notLackLam[i]).prop('disabled',true).addClass('blockLamLack');
            }
            $('#alert-lack:not(.showed)').addClass('showed').show('fast');
        } else{
            $('#alert-lack.showed').removeClass('showed').hide('fast');
            $('.blockLamLack').each(function(){
                if(!$(this).hasClass('postDisabled') && !$(this).hasClass('lamDisabled') && !$(this).hasClass('blockLamKonv') && !$(this).hasClass('blockLamRul')&& !$(this).hasClass('disabledWhite') && !$(this).hasClass('disabledSize') && !$(this).hasClass('disabledFigcutting') && !$(this).hasClass('disabledSize1')&& !$(this).hasClass('disabledCl')){
                    $(this).prop('disabled',false);
                }
                $(this).removeClass('blockLamRul');
            });
        }
    }
    function blockAllNotRullMat(){
        var flagC = 0;
        var selector = '';
        if($('.laminat:checked').length != 0){
            selector = '.laminat:checked';
        }
        if(selector != ''){
            $(selector).each(function(){
                if(rulLam.indexOf(+$(this).val()) != -1){
                    flagC++;
                }
            });
        }
        if(flagC > 0){
            for(var i = 0; i < noRulLam.length; i++){
                if($('#m_' + noRulLam[i]).prop('checked')){
                    $('#m_' + noRulLam[i]).prop('checked',false).change();
                }
                $('#m_' + noRulLam[i]).prop('disabled',true).addClass('blockLamRul');
            }
        } else{
            $('.blockLamRul').each(function(){
                if(!$(this).hasClass('postDisabled') && !$(this).hasClass('lamDisabled') && !$(this).hasClass('blockLamKonv') && !$(this).hasClass('blockLamLack')&& !$(this).hasClass('disabledWhite') && !$(this).hasClass('disabledSize')&& !$(this).hasClass('disabledFigcutting') && !$(this).hasClass('disabledSize1')&& !$(this).hasClass('disabledCl')){
                    $(this).prop('disabled',false);
                }
                $(this).removeClass('blockLamRul');
            });
        }
    }
    function blockAllNotKonvMat(){
        var flagC = 0;
        var selector = '';
        if($('.laminat:checked').length != 0){
            selector = '.laminat:checked';
        }
        if(selector != ''){
            $(selector).each(function(){
                if(konv_lam.indexOf(+$(this).val()) != -1){
                    flagC++;
                }
            });
        }
        if(flagC > 0){
            for(var i = 0; i < notKonvLam.length; i++){
                if($('#m_' + notKonvLam[i]).prop('checked')){
                    $('#m_' + notKonvLam[i]).prop('checked',false).change();
                }
                $('#m_' + notKonvLam[i]).prop('disabled',true).addClass('blockLamKonv');
            }
        } else{
            $('.blockLamKonv').each(function(){
                if(!$(this).hasClass('postDisabled') && !$(this).hasClass('lamDisabled') && !$(this).hasClass('blockLamRul') && !$(this).hasClass('blockLamLack')&& !$(this).hasClass('disabledWhite')  && !$(this).hasClass('disabledSize')&& !$(this).hasClass('disabledFigcutting') && !$(this).hasClass('disabledSize1')&& !$(this).hasClass('disabledCl')){
                    $(this).prop('disabled',false);
                }
                $(this).removeClass('blockLamKonv');
            });
        }
    }
    function blockAllKonvLam(){
        var flagC = 0;
        if($('.select-mat:checked').length != 0){
            var selector = '.select-mat:checked';
        } else {
            var selector = '.select-mat:not(:disabled)';
        }
        $(selector).each(function(){
            if(notKonvLam.indexOf(+$(this).val()) == -1){
                flagC++;
            }
        });
        if(flagC == 0){
            for(var i = 0; i < konv_lam.length; i++){
                if($('#l_' + konv_lam[i]).prop('checked')){
                    $('#l_' + konv_lam[i]).prop('checked',false).change();
                }
                $('#l_' + konv_lam[i]).prop('disabled',true).addClass('blockMatKonv');
            }
        } else{
            $('.blockMatKonv').each(function(){
                if($(this).hasClass('countDisabled') == false && $(this).hasClass('matDisabled') == false && $(this).hasClass('blockKonv') == false && $(this).hasClass('disabledPost') == false && $(this).hasClass('blockBySize') == false && $(this).hasClass('disabledCl1') == false && $(this).hasClass('disabledCl2') == false){
                    $(this).prop('disabled',false);
                }
                $(this).removeClass('blockMatKonv');
            });
        }
    }
     function blockLamMaterial(nameElem,arrName,arrRequire,nameShow,addC,disableC){
        var flagM = 0;
        $('.' + nameElem + ':checked').each(function(){
            if(arrName.indexOf(+$(this).val()) != -1){
               flagM++;
            }
        });

        if(flagM > 0){
            for(var i = 0; i < arrRequire.length; i++){
                if($(nameShow + arrRequire[i]).hasClass(addC) == false){
                    $(nameShow + arrRequire[i]).addClass(addC);
                    if($(nameShow + arrRequire[i]).prop('disabled') == false){
                        $(nameShow + arrRequire[i]).prop('disabled',true);
                    }
                }

            }
        } else {
           for(var i = 0; i < arrRequire.length; i++){
                if($(nameShow + arrRequire[i]).hasClass(addC) == true){
                    $(nameShow + arrRequire[i]).removeClass(addC);
                    if($(nameShow + arrRequire[i]).hasClass('countDisabled') == false && $(nameShow + arrRequire[i]).hasClass(disableC) == false && $(nameShow + arrRequire[i]).hasClass('postDisabled') == false && $(nameShow + arrRequire[i]).hasClass('sideBlock') == false && $(nameShow + arrRequire[i]).hasClass('blockKonv') == false && $(nameShow + arrRequire[i]).hasClass('blockBySize') == false  && !$(nameShow + arrRequire[i]).hasClass('blockMatRul') && !$(nameShow + arrRequire[i]).hasClass('blockMatLack') && !$(nameShow + arrRequire[i]).hasClass('blockMatKonv')&& !$(nameShow + arrRequire[i]).hasClass('blockLamRul') && !$(nameShow + arrRequire[i]).hasClass('blockLamKonv') && !$(nameShow + arrRequire[i]).hasClass('disabledCl1') && !$(nameShow + arrRequire[i]).hasClass('disabledCl2')  && !$(nameShow + arrRequire[i]).hasClass('disabledSize')&& !$(nameShow + arrRequire[i]).hasClass('disabledFigcutting')&& !$(nameShow + arrRequire[i]).hasClass('disabledSize1')&& !$(nameShow + arrRequire[i]).hasClass('disabledCl')){
                        $(nameShow + arrRequire[i]).prop('disabled',false);
                    }
                }
            }
        }
    }
    function IsJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }
    function blockMaterial(blockArr){
        $('.select-mat.postDisabled:disabled').each(function(){
            $(this).removeClass('postDisabled');
            if($(this).hasClass('lamDisabled') == false && $(this).hasClass('countDisabled') == false && !$(this).hasClass('blockLamRul') && !$(this).hasClass('blockLamKonv')&& !$(this).hasClass('blockLamLack')&& !$(this).hasClass('disabledWhite') && !$(this).hasClass('disabledSize')&& !$(this).hasClass('disabledFigcutting')&& !$(this).hasClass('disabledSize1')&& !$(this).hasClass('disabledCl')){
               $(this).prop('disabled',false);
            }
        });
        if(blockArr.length > 0){
            for(var t = 0; t < blockArr.length; t++){
                if($('#m_' + blockArr[t]).hasClass('postDisabled') == false){
                    $('#m_' + blockArr[t]).prop('checked',false).addClass('postDisabled').prop('disabled',true);
                }
            }
        }
        blockLam();
        blockAllRullLam();
        blockAllLackLam();
        blockAllKonvLam();
        blockColor();
    }
    /*END BLOCKER FUNCTION*
    /*END FUNCTION*/
    $(".tirazh").bind('keyup',function(){
        if (this.value.match(/[^0-9]/g)) {
            this.value = this.value.replace(/[^0-9]/g, '');
        }
	});
    $('.types-prod').click(function () {
        location.href = $(this).attr('data-link');
    });
    /*FIRST LOAD*/
    blockNullGroup();
     if(parametrLoad == 'product' || parametrLoad == 'return'){
        loadPage();
    } else {
        $(this).queue(function(){
            if($('#size_type2:checked').length > 0){
                $('#size_type1').prop('checked',true).change();
            }
            $('#sel_razmer').change().trigger("chosen:updated");
            $('select.sel_post_rab').each(function(){
                if($(this).val() != 0){
                    $(this).change().trigger("chosen:updated");
                }
            });
            $("#pricefilters input:checked").change();

            $(this).dequeue();
        });
        $(this).queue(function(){
            add_opacity_option_reset();
            add_opacity_filter_reset();
            firstLoad = false;
            $("#pricefilters").submit();
            $(this).dequeue();
        });
    }

});
