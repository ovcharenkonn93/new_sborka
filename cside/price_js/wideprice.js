$.fn.extend({
    animateCssShow: function (animationName) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        this.addClass('animated open ' + animationName).one(animationEnd, function() {
            $(this).removeClass('animated ' + animationName);
        });
    },
    animateCssHide: function (animationName) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
         $(this).addClass('animated ' + animationName).one(animationEnd, function() {
            $(this).removeClass('animated open ' + animationName);
        });
    },
    animateCss: function (animationName,afterFunc) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
         $(this).addClass('animated ' + animationName).one(animationEnd, function() {
            if(afterFunc != ''){
                afterFunc();
            }
            $(this).removeClass('animated ' + animationName);
        });
    }
});
function add_opacity_filter_reset(){
    var checkM = $('#selectall:checked').length + $('.select-mat:checked').length;
    var checkT = $('#alltime:checked').length + $('.terms:checked').length;
    var checkP = $('#all-print:checked').length + $('.select-print:checked').length;
    var checkL = $('#all-lam:checked').length + $('.select-lam:checked').length;
    var sum = checkM + checkT + checkP + checkL;
    if( sum == 0)
    {
        $('#filter-reset').addClass('reset_no_active');
    }else{
        $('#filter-reset').removeClass('reset_no_active');
    }
}
var active = 0;
var activeP = 0;
var xhrM = '';
var xhrP = '';
var insert = '<p class="text-center" style="font-size: 50px;padding: 20px 0;"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span></p>';
var width = 0;
var height = 0;
var firstLoad = true;
var flagReset = true;
// функция смены текста тиража в модальном наценки
function change_text_modal_tirazh(name,val_inp){
    $('.m_d_tir_'+name).html(val_inp);
}
    var zavis = {
            m_6 :{'360':55,'540':65,'720':75,'1080':85,'1440':100,'1442':110,'2880':0,'1441':0,'1443':0,'1444':0,'1446':0,'1445':0},
            m_1853:{'360':60,'540':80,'720':80,'1080':90,'1440':110,'1442':115,'2880':185,'1441':195,'1443':170,'1444':0,'1446':0,'1445':0},
            m_40:{'360':65,'540':75,'720':85,'1080':95,'1440':110,'1442':120,'2880':185,'1441':195,'1443':175,'1444':0,'1446':0,'1445':0},

            m_1854:{'360':105,'540':115,'720':125,'1080':135,'1440':150,'1442':160,'2880':225,'1441':235,'1443':215,'1444':0,'1446':0,'1445':0},

            m_1855:{'360':0,'540':0,'720':200,'1080':0,'1440':0,'1442':0,'2880':0,'1441':0,'1443':0,'1444':0,'1446':0,'1445':0},

            m_7 :{'360':75,'540':85,'720':95,'1080':105,'1440':120,'1442':130,'2880':195,'1441':205,'1443':185,'1444':0,'1446':0,'1445':0},
            m_48:{'360':105,'540':115,'720':125,'1080':135,'1440':150,'1442':160,'2880':225,'1441':235,'1443':215,'1444':0,'1446':0,'1445':0},
            m_45:{'360':135,'540':145,'720':155,'1080':165,'1440':0,'1442':0,'2880':0,'1441':265,'1443':0,'1444':0,'1446':0,'1445':0},
            m_27:{'360':85,'540':95,'720':105,'1080':115,'1440':130,'1442':140,'2880':205,'1441':215,'1443':195,'1444':0,'1446':0,'1445':0},
            m_43:{'360':0,'540':60,'720':70,'1080':80,'1440':95,'1442':105,'2880':0,'1441':0,'1443':0,'1444':0,'1446':0,'1445':0},
            m_1 :{'360':0,'540':70,'720':80,'1080':90,'1440':105,'1442':115,'2880':180,'1441':190,'1443':170,'1444':0,'1446':0,'1445':0},
            m_2 :{'360':0,'540':75,'720':85,'1080':95,'1440':110,'1442':120,'2880':185,'1441':195,'1443':175,'1444':0,'1446':0,'1445':0},
            m_3 :{'360':0,'540':75,'720':85,'1080':95,'1440':110,'1442':120,'2880':185,'1441':195,'1443':175,'1444':275,'1446':275,'1445':425},
            m_25:{'360':0,'540':115,'720':125,'1080':135,'1440':150,'1442':160,'2880':225,'1441':235,'1443':215,'1444':0,'1446':0,'1445':0},
            m_19:{'360':0,'540':115,'720':125,'1080':135,'1440':150,'1442':160,'2880':225,'1441':235,'1443':215,'1444':0,'1446':0,'1445':0},
            m_4 :{'360':0,'540':120,'720':130,'1080':140,'1440':155,'1442':165,'2880':230,'1441':0,'1443':0,'1444':0,'1446':0,'1445':0},
            m_46:{'360':0,'540':0,'720':0,'1080':0,'1440':0,'1442':0,'2880':0,'1441':240,'1443':220,'1444':0,'1446':0,'1445':0},
            m_1881:{'360':0,'540':170,'720':180,'1080':190,'1440':205,'1442':215,'2880':280,'1441':290,'1443':270,'1444':0,'1446':0,'1445':0},
            m_1882:{'360':0,'540':170,'720':180,'1080':190,'1440':205,'1442':215,'2880':280,'1441':290,'1443':270,'1444':0,'1446':0,'1445':0},
            m_15:{'360':20,'540':25,'720':30,'1080':35,'1440':0,'1442':0,'2880':0,'1441':0,'1443':0,'1444':0,'1446':0,'1445':0},
            m_1529:{'360':35,'540':40,'720':45,'1080':50,'1440':60,'1442':65,'2880':135,'1441':140,'1443':120,'1444':0,'1446':0,'1445':0},
            m_14:{'360':40,'540':45,'720':50,'1080':55,'1440':65,'1442':70,'2880':135,'1441':145,'1443':125,'1444':0,'1446':0,'1445':0},
            m_21:{'360':50,'540':55,'720':60,'1080':65,'1440':75,'1442':80,'2880':145,'1441':155,'1443':135,'1444':0,'1446':0,'1445':0},
    }
var karm_luv = {
    1:[1,2,3,4,6,7],
    2:[1,2,8,4,6,7],
    3:[1,2,6,7,5],
    4:[1,2,6,7,5],
    5:[1,2,6,7,8,5,4,3],
    6:[1,2,6,7,8,4,3],
    7:[1,2,6,7,5,4,3]
}
var luv_karm = {
    1:[1,2,3,4,5,6,7],
    2:[1,2,3,4,5,6,7],
    3:[1,5,6],
    4:[1,2,5,6],
    5:[3,4,5,7],
    6:[1,2,3,4,5,6,7],
    7:[1,2,3,4,5,6,7],
    8:[2,5,6]
}
$('document').ready(function(){

    $('select.sel_post_rab,#lam,#sel_razmer,.sel_metr,.sel_post_luv,.dop_select').chosen({width: "100%",disable_search_threshold: 10,disable_search: true});
    $('#price').popover({
        selector: '[data-toggle=tooltip]',
        placement: 'top',
        html: true,
        trigger: 'hover'
    });
    function removeAnim(){
        $('.animated.zoomIn').removeClass('animated zoomIn');
    }
    $('.divider-help').height($('.pw-cont:first-child').closest('.mobile-droped').innerHeight() - 0.8);
    setTimeout(removeAnim,1000);
    $('.t_greyblock,.header-opt').popover({
        selector: '[data-toggle=tooltip]',
        placement: 'top',
        html: true,
        trigger: 'hover'
    });
    $('body').on('submit','[name=form_discount],[name="form_discount_download"]',function(){
        return false;
    });

        $('.print-price').click(function(){
            if($(this).hasClass('icon_print')){
                printClick = 'print';
            } else if($(this).hasClass('icon_exel')) {
                printClick = 'excel';
            } else if($(this).hasClass('icon_word')) {
                printClick = 'word';
            }
        });
        $('.input_discount').on('input', function(){
            if (this.value.match(/[^0-9.]/g)) {
                this.value = this.value.replace(/[^0-9.]/g, '');
            }
        });
        $('body').on('click','#m_d_submit',function(){
            $('.d_m_d_variant').eq( $('.m_d_variant:checked').val() ).prop('checked',true);
            $('#d_m_d_all').val( $('#m_d_all').val() );
            $('#d_m_d_t_1').val( $('#m_d_t_1').val() );
            $('#d_m_d_t_2').val( $('#m_d_t_2').val() );
            $('#d_m_d_t_3').val( $('#m_d_t_3').val() );
            $('#d_m_d_t_4').val( $('#m_d_t_4').val() );
            $('#d_m_d_t_5').val( $('#m_d_t_5').val() );
            $('#d_m_d_t_6').val( $('#m_d_t_6').val() );
            $(this).closest('.modal').modal('hide');
            if(!firstLoad){             $("#pricefilters").submit();         }
        });
         $('body').on('click','#d_m_d_submit',function(){
             $(this).closest('.modal').modal('hide');
             pricePints(printClick);
         });

        $('body').on('change keyup','#m_d_all',function(){
            $('#m_d_t_1,#m_d_t_2,#m_d_t_3,#m_d_t_4,#m_d_t_5,#m_d_t_6').val($(this).val());
        });
        $('body').on('change keyup','#m_d_t_1,#m_d_t_2,#m_d_t_3,#m_d_t_4,#m_d_t_5,#m_d_t_6',function(){
            $('#m_d_all').val('');
        });
        $('body').on('change keyup','#d_m_d_all',function(){
            $('#d_m_d_t_1,#d_m_d_t_2,#d_m_d_t_3,#d_m_d_t_4,#d_m_d_t_5,#d_m_d_t_6').val($(this).val());
        });
        $('body').on('change keyup','#d_m_d_t_1,#d_m_d_t_2,#d_m_d_t_3,#d_m_d_t_4,#d_m_d_t_5,#d_m_d_t_6',function(){
            $('#d_m_d_all').val('');
        });

    $("#pricefilters").submit(function() {
        if(active != 0){
            xhrM.abort();
        }

        var dataSend = $("#pricefilters").serialize() + '&class=' + type;
        xhrM = $.ajax({
            type: "POST",
            url: "cside/ajax.php?action=pricedigital_wide",
            data: dataSend,
            dataType: 'json',
            beforeSend: function(){
                active++;
                $('#insert-in').html('');
                $('#loading-table').html(insert);
//                console.log(dataSend)
            },
            complete: function(data){
                if(data.responseText == 'error'){
                    location.reload();
                } else if(data.statusText == 'OK' && IsJsonString(data.responseText) == false){
                    $('#loading-table').html('');
                    var text = '<tr><td colspan="9" class="text-center" style="vertical-align:middle;padding:20px;font-weight:500;font-size:22px;">При подсчете произошла ошибка! Пожалуйста сообщите нам, об этом нажав на кнопку "Нашли ошибку".</td></tr>';
                    $('#insert-in').html(text);
                }
            },
            success: function(data){
                 $('#loading-table').html('');
                 $('#insert-in').html(com(data.text));
                 tmpHide();
                 zavisColors();
                active=0;
                var checkM = $('#selectall:checked').length + $('.select-mat:checked').length;
                var checkT = $('#alltime:checked').length + $('.terms:checked').length;
                var checkP = $('#all-print:checked').length + $('.select-print:checked').length;
                if($('.select-lam').length > 0){
                    var checkL = $('#all-lam:checked').length + $('.select-lam:checked').length;
                } else {
                   checkL = 1;
                }
                if($('#print_color').val() == 1){
                    checkM = 1;
                }
                if(checkM > 0 && checkP > 0 && checkT > 0){
                    $('.time-select.hidden').removeClass('hidden').animateCss('fadeIn','');
                    $('#alltime').closest('.t_switchable').find('.filter-name').html('Cрок печати');
                } 
                xhrM = '';
            }
        });
        return false;
    });

    function pricePints(printClass){
        if(activeP != 0){
            xhrP.abort();
        }
        var dateNow = new Date();
        var day = dateNow.getDate();
        var month = dateNow.getMonth() + 1;
        var year = dateNow.getFullYear();
        var dataSend = $("#pricefilters").serialize() + '&class=' + type + '&' + $('[name="form_discount_download"]').serialize();
        xhrP = $.ajax({
            type: "POST",
            url: "cside/ajax.php?action=pricedigital_wide&type=print",
            data: dataSend,
            dataType: 'json',
            beforeSend: function(){
                activeP++;
            },
            success: function(data){
                if(printClass == 'print'){
                    $(com(data.text)).print({
                        globalStyles: true,
                        mediaPrint: false,
                        stylesheet: "cside/print-css.css",
                        noPrintSelector: ".no-print",
                        iframe: true,
                        append: null,
                        prepend: null,
                        manuallyCopyFormValues: true,
                        deferred: $.Deferred(),
                        timeout: 750,
                        title: null,
                        doctype: '<!doctype html>'
                    });
                } else if(printClass == 'excel') {
                    tableToExcel(com(data.text));
                }
                activeP=0;
                xhrP = '';
            }
        });
        return false;
    };
    /*END PRINT DATA AJAX*/

    /*LUVERS PARAMETR SELECT*/
    $('.sel_post_luv,#count_format,.dop_select').change(function(){
        if($(this).prop('disabled') == false){
            calculate();
        }
        if(!firstLoad){
            $("#pricefilters").submit();
        }
    });
    /*END LUVERS PARAMETR SELECT*/
    $('#luvers_type').change(function(){
        if($(this).val() != 1 && $(this).prop('disabled') == false){
            $('#trim option[value=0]').prop('disabled',true);
            $('#trim').val(1).change().trigger("chosen:updated");
            $('#luvers-dia option[value=10]').prop('disabled',false);
            $('#luvers-dia').trigger("chosen:updated");

        } else {
            if($('#print_color').val() == 0){
                $('#trim option[value=0]').prop('disabled',false);
                $('#trim').trigger("chosen:updated");
                $('#luvers-dia option[value=10]').prop('disabled',false);
                $('#luvers-dia').trigger("chosen:updated");
            } else if($('#print_color').val() == 2){
                if($('#luvers-dia').val() != 12){
                    $('#luvers-dia').val(12).change();
                }
                $('#luvers-dia option[value=10]').prop('disabled',true);
                $('#luvers-dia').trigger("chosen:updated");
            } else {
                $('#luvers-dia option[value=10]').prop('disabled',false);
                $('#luvers-dia').trigger("chosen:updated");
            }

        }

        blockByPodv();
    });
    function blockByPodv(){
        if($('#luvers_type').val() == 1 && !$('#luvers_type').prop('disabled') && type == 'banner' && width > 1520 && height > 1520){
            $('.select-print').each(function(){
                if($(this).val() >= 1440 && $(this).val() != 1443 && !$(this).hasClass('disablePodv') && !$(this).hasClass('disabledSize')){
                    $(this).prop('disabled',true).prop('checked',false).addClass('disablePodv').change();
                }
            });
            if($('.select-print:checked').length == 0){
                $('#all-print').prop('checked',true).change();
            }
        } else if($('#luvers_type').val() != 1 && type == 'banner' && (width < 1520 || height < 1520)) {
            $('.select-print.disablePodv:not(.disabledColor):not(.disableMat):not(.disabledSize)').prop('disabled',false);
            $('.select-print.disablePodv').removeClass('disablePodv');
        }
    }

    /*POSTWORK SELECT*/

    $(".sel_post_rab").change(function(){

        var selector = '[data-controlled-item=' + this.id + '-selected]';

         if($(this).val() != 0){

                if(this.id == 'podvorot'){

                    if($('#luvers_type').prop('disabled') == false){

                        $('#luvers_type').prop('disabled',true).trigger("chosen:updated");
                        $('#luvers_type').change();
                        $('#luvers_type').closest('.parametr-luvers').find('.name-cover').css('opacity',0.5);

                    }
                    if($('#trim').val() > 0){
                        $('#trim').val(0).change().trigger("chosen:updated");
                     }
                    $('#m_6:not(.disabledAdh)').prop('disabled',true).addClass('disabledAdh');
                } else if(this.id == 'luvers'){

//                    $('#trim').val(0).prop('disabled',true).change().trigger("chosen:updated");
//                    $('#trim').val(1).change().find('option[value=0]').prop('disabled',true);
//                    $('#trim').trigger("chosen:updated");


                    $('.parametr-luvers.hidden:not(.only-metki):not(.not-metki)').removeClass('hidden');

                    $('.luvers-option').css('display','block');

                    $('.sel_post_luv:disabled:not(#luvers_type)').prop('disabled',false).trigger("chosen:updated");



                    if($(this).val() != 1 && $(this).val() != 6 && $(this).val() != 7){

                        $('#luvers-shag').prop('disabled',false).trigger("chosen:updated");

                        $('.not-metki.hidden').removeClass('hidden');

                        $('.only-metki:not(.hidden)').addClass('hidden');

                        $('#count-luvers').prop('disabled',true);

                    } else {

                        if($('#luvers-shag').prop('disabled') == false){

                            $('#luvers-shag').prop('disabled',true).trigger("chosen:updated");

                        }

                        $('.not-metki:not(.hidden)').addClass('hidden');



                        if($(this).val() == 7){

                            $('.only-metki').removeClass('hidden');

                            $('#count-luvers').prop('disabled',false);

                        } else {

                            $('.only-metki:not(.hidden)').addClass('hidden');

                            $('#count-luvers').prop('disabled',true);

                        }

                    }

                  if($('#podvorot').val() == 0){
                      $('#luvers_type').closest('.parametr-luvers').find('.name-cover').css('opacity',1);
                      $('#luvers_type').prop('disabled',false).trigger("chosen:updated");
                      $('#luvers_type').change();

                  }
                    luvKonv($(this).val());
              } else if(this.id == 'trim'){
                  if($('#podvorot').val() > 0){
                     $('#podvorot').val(0).change().trigger("chosen:updated");
                  }
              }
             if(this.id == 'karman'){
                 konvLuv($(this).val());
                 $('#m_6:not(.disabledK)').prop('disabled',true).addClass('disabledK');
             }
             if(this.id == 'plot'){

                 $('.block-plot').removeClass('hidden');
                 $('#m_46:not(.disabledPlot)').prop('disabled',true).prop('checked',false).change().addClass('disabledPlot');
                 $('#picking').change();
             }

             if(this.id == 'print_color' && type == 'banner' && $(this).val() == 1){
                 $('.select-mat:not(.disabledColor)').prop('disabled',true).addClass('disabledColor');
//                 $('#trim').val(1).change().find('option[value=0]').prop('disabled',true);
//                    $('#trim').trigger("chosen:updated");
                 $('.select-print:not(#p_720):not(.disabledColor)').prop('disabled',true).addClass('disabledColor');
                 if($('#karman').val() > 0){
                     $('#karman').val(0).change().prop('disabled',true).trigger("chosen:updated");
                 } else {
                     $('#karman').prop('disabled',true).trigger("chosen:updated");
                 }
                 if($('#podvorot').val() > 0){
                     $('#podvorot').val(0).change().prop('disabled',true).trigger("chosen:updated");
                 } else {
                     $('#podvorot').prop('disabled',true).trigger("chosen:updated");
                 }
                 $('#luvers_type').val(0).change();
                 $('#luvers_type option[value=1]').prop('disabled',true);
                 $('#luvers_type option[value=2]').prop('disabled',true);
                 $('#luvers_type').trigger("chosen:updated");

                 $('#luvers-dia option[value=10]').prop('disabled',false);
                 $('#luvers-dia').trigger("chosen:updated");
//                 $('#trim').val(1).change().find('option[value=0]').prop('disabled',true);
//                 $('#trim').trigger("chosen:updated");
                   $('#luvers option[value=0]').prop('disabled',false);
                   $('#luvers').trigger("chosen:updated");
                 if(firstLoad){
                    firstLoad = false;
                    $('#selectall').prop('disabled',false).prop('checked',true).closest('.t_switchable').removeClass('imitate-disabled');
                    $("#pricefilters").submit();
                 }
             } else if(this.id == 'print_color' && type == 'banner' && $(this).val() == 2){
                 if($('#karman').val() > 0){
                     $('#karman').val(0).change().prop('disabled',true).trigger("chosen:updated");
                 } else {
                     $('#karman').prop('disabled',true).trigger("chosen:updated");
                 }
                 if($('#podvorot').val() > 0){
                     $('#podvorot').val(0).change().prop('disabled',true).trigger("chosen:updated");
                 } else {
                     $('#podvorot').prop('disabled',true).trigger("chosen:updated");
                 }
                 $('#luvers_type').val(0).change();
                 $('#luvers_type option[value=2]').prop('disabled',false);
                 $('#luvers_type option[value=1]').prop('disabled',false);
                 $('#luvers_type').val(1).change();
                 $('#luvers_type').trigger("chosen:updated");

                 $('.disabledColor:not(.disabledShow):not(.disableMat):not(.disablePodv):not(.disabledSize)').prop('disabled',false);
                 if($('#luvers_type').val() == 1){
                     if($('#luvers-dia').val() != 12){
                         $('#luvers-dia').val(12).change();
                     }
                     $('#luvers-dia option[value=10]').prop('disabled',true);
                     $('#luvers-dia').trigger("chosen:updated");
                 } else {
                     $('#luvers-dia option[value=10]').prop('disabled',false);
                     $('#luvers-dia').trigger("chosen:updated");
                 }
                 $('.disabledColor').removeClass('disabledColor');
                 $('#luvers option[value=0]').prop('disabled',true);
                 $('#luvers').val(2).change().trigger("chosen:updated");
             }

         } else {

             if(this.id == 'podvorot'){

                if($('#luvers').val() > 0){

                    $('#luvers_type').closest('.parametr-luvers').find('.name-cover').css('opacity',1);

                    $('#luvers_type').prop('disabled',false).trigger("chosen:updated");
                    $('#luvers_type').change();
                }
                 $('#m_6.disabledAdh:not(.disablePrint):not(.disabledShow):not(.disabledK)').prop('disabled',false);
                 $('#m_6.disabledAdh').removeClass('disabledAdh');


            } else if(this.id == 'luvers'){
                 if($('#print_color').val() == 0){
                     $('#trim option[value=0]').prop('disabled',false);
                     $('#trim').trigger("chosen:updated");
                 }



                $('.parametr-luvers:not(.hidden)').addClass('hidden');

                $('.luvers-option').css('display','none');

                $('.sel_post_luv:not(:disabled)').prop('disabled',true).trigger("chosen:updated");
                luvKonv($(this).val());
            }
             if(this.id == 'karman'){
                 konvLuv($(this).val());
                 $('#m_6.disabledK:not(.disablePrint):not(.disabledShow):not(.disabledAdh)').prop('disabled',false);
                 $('#m_6.disabledK').removeClass('disabledAdh');
             }
             if(this.id == 'plot'){

                 $('.block-plot:not(.hidden)').addClass('hidden');
                 $('#m_46.disabledPlot:not(.disablePrint):not(.disablePrint)').prop('disabled',false);
                 $('#m_46.disabledPlot').removeClass('disabledPlot');
             }

             if(this.id == 'print_color' && type == 'banner'){
                 $('#karman').prop('disabled',false).trigger("chosen:updated");
                 $('#podvorot').prop('disabled',false).trigger("chosen:updated");
                 $('#luvers_type option[value=2]').prop('disabled',false);
                 $('#luvers_type option[value=1]').prop('disabled',false);
                 $('#luvers_type').trigger("chosen:updated");
                 $('#luvers-dia option[value=10]').prop('disabled',false);
                 $('#luvers-dia').trigger("chosen:updated");
                 $('.disabledColor:not(.disabledShow):not(.disableMat):not(.disablePodv):not(.disabledSize)').prop('disabled',false);
                 $('#luvers option[value=0]').prop('disabled',false);
                 $('#luvers').trigger("chosen:updated");
                 if($('#luvers').val() == 0){
                    $('#trim option[value=0]').prop('disabled',false);
                     $('#trim').trigger("chosen:updated");
                 }
                 $('.disabledColor').removeClass('disabledColor');
                 $("#pricefilters").submit();

             }

         }

        calculate();
        add_opacity_option_reset();
        blockByPodv();
        if(!firstLoad){

            $("#pricefilters").submit();

        }

    });
    $('#count-luvers').change(function(){
        calculate();
        if(!firstLoad){
            $("#pricefilters").submit();
        }
    });
    $('#picking').change(function(){
        if(+$(this).val() == 0){
            if(+$('#backwash').val() == 2)
                $('#backwash option[value="1"]').prop('selected',true);
                $('#backwash').change().trigger("chosen:updated");
        }
    });
    $('#backwash').change(function(){
        if($(this).val() != 0){
            if($(this).val() == 2){
                $('#picking option[value=1]').prop('selected',true).trigger("chosen:updated");
            }
            $('.mounting_film_require.hidden').removeClass('hidden');
            $('#mounting_film').prop('disabled',false).trigger("chosen:updated");
        } else {
            $('.mounting_film_require:not(.hidden)').addClass('hidden'); 
            $('#mounting_film').prop('disabled',true).trigger("chosen:updated");
        }
    });
    $('.sel-adh').change(function(){
        if(+$('.sel-adh:checked').val() == 0 && !$('.sel-adh:checked').prop('disabled')){
            $('.sel_post_rab:not(#trim)').each(function() {
               if($(this).val() > 0){
                   $(this).val(0).change()
               } 
            });
            $('.sel_post_rab:not(#trim):not(.disabledAdh)').prop('disabled',true).addClass('disabledAdh').trigger("chosen:updated");
        } else {
            $('.sel_post_rab.disabledAdh').prop('disabled',false);
            $('.sel_post_rab.disabledAdh').removeClass('disabledAdh').trigger("chosen:updated");
        }
        if(!firstLoad){
            $("#pricefilters").submit();
        }
    });
     /*END POSTWORK SELECT*/
     /*INPUTS CONTROL*/
    $('.input-option').on("input change",function(){
        var selector = '[data-controlled-item=' + this.id + '-selected]';
        if (this.value.match(/[^0-9]/g)) {
            this.value = this.value.replace(/[^0-9]/g, '');
        }
        if($(this).val() < 1 && $(this).val() != ''){$(this).val(1).change();}
    });
    $('.input-option').on('focusout',function(){
        if($(this).val() == ''){$(this).val(1).change();}
    });
    $('.input-option').on("change",function(){
        if(!firstLoad){             $("#pricefilters").submit();         }
        if(this.id == 'sets'){
//            if($(this).val() != 1){$('#text_info_sets').css('display','block');}else{$('#text_info_sets').css('display','none');}
            if($(this).val() > 50){$(this).val(50).change();}
        }
        add_opacity_option_reset();
     });
    $('.plus-input-val').click(function(){
        var selector = $(this).attr("data-change-input");
        $('#' + selector).val(parseInt($('#' + selector).val()) + 1).change();
    });
    $('.minus-input-val').click(function(){
         var selector = $(this).attr("data-change-input");
        $('#' + selector).val(parseInt($('#' + selector).val()) - 1).change();
    });
    $("#option-reset:not(.reset_no_active)").click(function(){
        if($('#sets').val() != 1){
            $('#sets').val(1).change();
        }
        $('.sel_post_rab').each(function(){
            if($(this).val() != 0){
                $(this).val(0).change().trigger("chosen:updated");
            }
        });
        if($("#lv-count").val() > 0){
            $("#lv-count").val("");
        }
        add_opacity_option_reset();
    });

    $("#lv-count").on("change",function () {
        $('#option-reset.reset_no_active').removeClass('reset_no_active');
    });

    /*Обработчики фильтров*/
    $('#selectall').change(function(){
        $(this).queue(function(){
            $('.select-mat:checked').prop('checked',false);
            zavisColors();
            $(this).closest('.t_switchable').find('.filter-name').html('Материал');
            lamRelations();
            add_opacity_filter_reset();
            $(this).dequeue();
        });
        $(this).queue(function(){
            setSubmit();
            $(this).dequeue();
        });
    });

    $('#alltime').change(function(){
        $(this).queue(function(){
            $('.terms:checked').prop('checked',false);
            zavisColors();
            $(this).closest('.t_switchable').find('.filter-name').html('Срок печати');
            add_opacity_filter_reset();
            $(this).dequeue();
        });
        $(this).queue(function(){
            setSubmit();
            $(this).dequeue();
        });
    });
    $('#all-lam').change(function(){
        $(this).queue(function(){
            $('.select-lam:checked').prop('checked',false);
            zavisColors()
            $(this).closest('.t_switchable').find('.filter-name').html('Ламинация');
            lamRelations();
            add_opacity_filter_reset();
            $(this).dequeue();
        });
        $(this).queue(function(){
            setSubmit();
            $(this).dequeue();
        });
    });
    $('#all-print').change(function(){
        $(this).queue(function(){
            $('.select-print:checked').prop('checked',false);
            zavisMats();
            $(this).closest('.t_switchable').find('.filter-name').html('Качество печати');
            add_opacity_filter_reset();
            $(this).dequeue();
        });
        $(this).queue(function(){
            setSubmit();
            $(this).dequeue();
        });
    });

    $('.select-mat').change(function(){
        $(this).queue(function(){
            $('#selectall:checked').prop('checked',false);
            zavisColors();
            lamRelations();
            $('#selectall:not(:disabled)').closest('.t_switchable').find('.filter-name').html('Выбрать все<br>материалы');
            add_opacity_filter_reset();
            $(this).dequeue();
        });
        $(this).queue(function(){
            setSubmit();
            $(this).dequeue();
        });
    });
    $(".select-print").change(function(){
        $(this).queue(function(){
            $('#all-print:checked').prop('checked',false);
            zavisMats();
            $('#all-print:not(:disabled)').closest('.t_switchable').find('.filter-name').html('Выбрать все<br>сроки печати');
            add_opacity_filter_reset();
            $(this).dequeue();
        });
        $(this).queue(function(){
            setSubmit();
            $(this).dequeue();
        });
    });
     $(".select-lam").change(function(){
         $(this).queue(function(){
            $('#all-lam:checked').prop('checked',false);
            $('#all-lam:not(:disabled)').closest('.t_switchable').find('.filter-name').html('Выбрать все<br>ламинации');
             lamRelations();
            add_opacity_filter_reset();
            $(this).dequeue();
        });
        $(this).queue(function(){
            setSubmit();
            $(this).dequeue();
        });
    });
    $(".terms").change(function(){
        $(this).queue(function(){
            $('#alltime:checked').prop('checked',false);
            $('#alltime:not(:disabled)').closest('.t_switchable').find('.filter-name').html('Выбрать все<br>сроки печати');
            add_opacity_filter_reset();
            $(this).dequeue();
        });
        $(this).queue(function(){
            setSubmit();
            $(this).dequeue();
        });
    });
    function setSubmit(){
        var checkM = $('#selectall:checked').length + $('.select-mat:checked').length;
        var checkT = $('#alltime:checked').length + $('.terms:checked').length;
        var checkP = $('#all-print:checked').length + $('.select-print:checked').length;
        if($('.select-lam').length > 0){
            var checkL = $('#all-lam:checked').length + $('.select-lam:checked').length;
        } else {
           checkL = 1;
        }
        if($('#print_color').val() == 1){
            checkM = 1;
        }
        if(checkM > 0 && checkP > 0 && checkT > 0){
            if(flagReset){
                flagReset = false;
                $('.select-all-btn').each(function () {
                   if(this.id == 'selectall'){
                      $(this).closest('.t_switchable').find('.filter-name').html('Выбрать все<br>материалы');
                   } else if (this.id == 'all-lam') {
                     $(this).closest('.t_switchable').find('.filter-name').html('Выбрать все<br>ламинации');
                   } else if (this.id == 'alltime') {
                     $(this).closest('.t_switchable').find('.filter-name').html('Выбрать все<br>сроки печати');
                   } else if (this.id == 'all-print') {
                     $(this).closest('.t_switchable').find('.filter-name').html('Выбрать все<br>качества печати');
                   }
                });
            }
            $('.select-all-btn').prop('disabled',false);
            $("#pricefilters").submit();
        } else {
            var insertNotSel = '<tr><td colspan="9" class="text-center" style="vertical-align:middle;padding:20px;font-weight:500;font-size:22px;">Для формирования прайс-листа выберите';
            var text = '';
            if(checkM == 0){
                text += ' материал,';
            }
            if(checkL == 0){
                text += ' ламинацию,';
            }
            if(checkP == 0){
                text += ' качество печати,';
            }
            if(checkT == 0){
                text += ' срок печати,';
            }
            text = text.substring(0, text.length - 1)
            insertNotSel += text + '</td></tr>';
            $('#insert-in').html(insertNotSel);
        }
    }
    $('#filter-reset').click(function(){
        $(this).queue(function(){
            $('#alltime:checked,#all-print:checked,.select-mat:checked,#selectall:checked,.terms:checked,.select-print:checked,#all-lam:checked,.select-lam:checked').prop('checked',false).change();
            zavisColors();
            zavisMats();
            lamRelations();
            if($('#print_color').val() != 1){
                $('#selectall').closest('.t_switchable').find('.filter-name').html('Материал');
                $('#alltime').closest('.t_switchable').find('.filter-name').html('Срок печати');
                $('#all-print').closest('.t_switchable').find('.filter-name').html('КАЧЕСТВО ПЕЧАТИ');
                $('#all-lam').closest('.t_switchable').find('.filter-name').html('Покрытие');
            }
            $(this).dequeue();
        });
        $(this).queue(function(){
            if($('#print_color').val() != 1){
                $('.select-all-btn').prop('disabled',true);
            }
            add_opacity_filter_reset();
            flagReset = true;
            $(this).dequeue();
        });
    });

    /*END FILTERS*/
    $('#sel_razmer').change(function(e) {
        width = +$(this).find('option:selected').data('width');
        height = +$(this).find('option:selected').data('height');
        $('.input_size').val('');
        $('[name="size_in"]').val(1).change().trigger("chosen:updated");
        $('#size_input').data("width",width).data("height",height);
        $('#size_input').attr('value',$(this).val()).change();
    });
    $('.input_size').on('input',function(){
        if (this.value.match(/[^0-9.]/g)) {
            this.value = this.value.replace(/[^0-9.]/g, '');
        }
    });
    $('.input_size').on('change',function(){
        $('#sel_razmer').val(0).trigger("chosen:updated");
        if(($('#input-width1').val()*$('[name="size_in"]').val()) < 50){
            $('#input-width1').val(50/$('[name="size_in"]').val());
        }
        if(($('#input-width1').val()*$('[name="size_in"]').val()) > 50000){
             $('#input-width1').val(50000/$('[name="size_in"]').val());
        }
        if( ($('#input-height1').val()*$('[name="size_in"]').val()) < 50){
            $('#input-height1').val(50/$('[name="size_in"]').val());
        }
        if(($('#input-height1').val()*$('[name="size_in"]').val()) > 50000){
             $('#input-height1').val(50000/$('[name="size_in"]').val());
        }
        
        width = +$('#input-width1').val()*$('[name="size_in"]').val();
        height = +$('#input-height1').val()*$('[name="size_in"]').val();
        $('#input-width').val(width);
        $('#input-height').val(height);
        
        $('#size_input').data("width",width).data("height",height);
        $('#size_input').attr('value',30).change();
        if(!firstLoad){             $("#pricefilters").submit();         }
    });
    $('[name="size_in"]').change(function(){
         var wid = $('#input-width1').val(),
            hei = $('#input-height1').val();
        if( wid != '' && hei != '' && +$('#size_input').val() == 30){
            if( $(this).val() == 10 ){
                $('#input-width1').val(wid/10).trigger('change');
                $('#input-height1').val(hei/10).trigger('change');
            }else{
                $('#input-width1').val(wid*10).trigger('change');
                $('#input-height1').val(hei*10).trigger('change');
            }
        }
    });
    $('#size_input').change(function(){
        if(parametrLoad != 'product' || (parametrLoad == 'product' && $('#rectangle').length > 0)){
            if($(this).val() == 30){
               smena_razmera(width,height,'Ваш размер');
            } else {
               smena_razmera(width,height,'');
            }
        }
        var infoShow = true;
        if(width > maxSize && height > maxSize && infoShow){
            infoShow = false;
             $('#modal_error_max_size').modal('show');
        }
        calculate();
        blockByPodv();
        sizeRelations();
        if(!firstLoad){             $("#pricefilters").submit();         }
    });
    if($(window).width() < 991){
         $('.header-opt').click(function(){
            if($(this).hasClass('openChild') == false){
                $(this).addClass('openChild');
                $(this).closest('.mobile-control').find('.mobile-droped').stop(true,true).animateCssShow('slideMenuDown');
                $(this).closest('.mobile-control').find('.icon-drop .fa-plus').fadeOut(250);
                $(this).closest('.mobile-control').find('.icon-drop .fa-minus').fadeIn(250);
            } else {
                $(this).removeClass('openChild');
                $(this).closest('.mobile-control').find('.mobile-droped').stop(true,true).animateCssHide('slideMenuTop');
                $(this).closest('.mobile-control').find('.icon-drop .fa-minus').fadeOut(250);
                $(this).closest('.mobile-control').find('.icon-drop .fa-plus').fadeIn(250);
            }
        });
    } else {
        padT();
    }
   $('[name="orient"]').change(function(){
        if(!firstLoad){             $("#pricefilters").submit();         }
    });
    $('.wrap_radio2').on('click',function(){
        if($(this).children('.krug2').attr('position') == 'left'){
            $('[name=cost_var]').eq(1).prop('checked',true).change();
            $('.cost_tir').css('display','none');
            $('.cost_one').css('display','block');
            $('.t_tir').css('opacity','0.5');
            $(this).children('.krug2').attr('position','right');
            $(this).stop(true,true).children('.krug2').animate({
                left:'100%',
                marginLeft: '-20px'
            },250);
            $(this).delay(250).queue(function(){
                $('.t_one').css('opacity','1');
                $(this).dequeue();
            });
        }else{
            $('[name=cost_var]').eq(0).prop('checked',true).change();
            $('.cost_tir').css('display','block');
            $('.cost_one').css('display','none');
            $('.t_one').css('opacity','0.5');
            $('.krug2').removeClass('radio_bg2');
            $(this).children('.krug2').attr('position','left');
            $(this).stop(true,true).children('.krug2').animate({
                left:'0',
                marginLeft: '0'
            },250);
            $(this).delay(250).queue(function(){
                $('.t_tir').css('opacity','1');
                $(this).dequeue();
            });
        }
    });
    /*FUNCTIONS*/
    function calculate(){
        if($("#luvers").length > 0 && $("#luvers").val() != 0){
            if ($("#luvers-dia").val() == 10){
				var dia = 2.4;
			}else{
				var dia = 3;
			}
            var pi = +( (+width*2 + +height*2 ) ).toFixed();
			if ($("#luvers option:selected").val()==1) {
				var luvers = 4 * dia;
			}else if($("#luvers option:selected").val()==2) {
				var luvers = Math.ceil(pi/+$("#luvers-shag").val()/10) * dia;
			}else if($("#luvers option:selected").val()==3) {
				var luvers = Math.ceil(+width/+$("#luvers-shag").val()/10) * dia;
			}else if($("#luvers option:selected").val()==4) {
				var luvers = Math.ceil(+width/+$("#luvers-shag").val()/10) * dia * 2;
			}else if($("#luvers option:selected").val()==5) {
				var luvers = Math.ceil(+height/+$("#luvers-shag").val()/10) * dia * 2;
			} else if($("#luvers option:selected").val()==6) {
				var luvers = 8*dia;
			}  else if($("#luvers option:selected").val()==7) {
				var luvers = +$('#count-luvers').val()*dia;
			} else if($("#luvers option:selected").val()==8) {
				var luvers = Math.ceil(+width/+$("#luvers-shag").val()/10) * dia;
			}
            $('[data-controlled-item="luvers-dia-selected"]').html('+ ' + (luvers/dia).toFixed(2)+' x '+dia.toFixed(2)+' = '+luvers.toFixed(2)+' грн').show();
//            if($('#luvers-podkl').val() > 0){
//                var podkladka = luvers/dia*1.5;
//                $('[data-controlled-item="luvers-podkl-selected"]').html('+ ' + (luvers/dia).toFixed(2)+' x 1.5 = '+podkladka.toFixed(2)+' грн').show();
//            } else {
//               $('[data-controlled-item="luvers-podkl-selected"]').html('').hide();
//            }
        } else {
            $('[data-controlled-item="luvers-dia-selected"]').html('').hide();
        }
        if($("#karman").val() != 0 && $("#karman").length > 0) {
            if ($("#karman option:selected").val()==1 || $("#karman option:selected").val()==2) {
                var karman = 24 * width/1000;
            }
            if ($("#karman option:selected").val()==3 || $("#karman option:selected").val()==4) {
                var karman = 24 * height/1000;
            }
            if ($("#karman option:selected").val()==5) {
                var karman = (width/1000 + height/1000)*24*2;
            }
            if ($("#karman option:selected").val()==6) {
                var karman = 24 * width/1000 * 2;
            }
            if ($("#karman option:selected").val()==7) {
                var karman = 24 * height/1000 *2;
            }
            $('[data-controlled-item="karman-selected"]').html('+ ' + (karman/24).toFixed(2)+' x 24 = '+karman.toFixed(2)+' грн').show();
        } else {
           $('[data-controlled-item="karman-selected"]').html('').hide();
        }
        
        if($('#mounting_film').length > 0 && $('#backwash').val() != 0){
//            var mountting = 0;
//            if()
            $('[data-controlled-item="mounting_film-selected"]').html('').hide();
        }else{
            $('[data-controlled-item="mounting_film-selected"]').html('').hide();
        }

        if($('#podvorot').length > 0 && $('#podvorot').val() != 0){
            var podvorot = (width/1000 + height/1000)*24*2;
            $('[data-controlled-item="podvorot-selected"]').html('+ ' + (podvorot/24).toFixed(2)+' x 24 = '+podvorot.toFixed(2)+' грн').show();
        } else {
            $('[data-controlled-item="podvorot-selected"]').html('').hide();
        }
//        if(type == 'banner')
//           if($('#trim').val() != 0){
//               var price = (+(width/1000 + height/1000)*1.5*2).toFixed(2);
//               if(price < 1.5){
//                   var text = '+ 1.5 грн (минимальная стоимость)';
//               } else {
//                   var text = '+ ' + (+price/1.5).toFixed(2)+' x 1.5 = ' + price +' грн';
//               }
//               $('[data-controlled-item=trim-selected]').html(text).show();
//           } else {
//               $('[data-controlled-item=trim-selected]').html('').hide();
//           }

            if($('#trim').val() == 0){
                $('[data-controlled-item=trim-selected]').html('').hide();
            } else if($('#trim').val() == 1) {
                if((width >= 100 && height >= 100) || (width >= 100 && height >= 100)){
                    var trimCost = 2;
                } else {
                    var trimCost = 4;
                }
                var price = (+(width/1000 + height/1000)*trimCost*2).toFixed(2);
                var text = '+ ' + price + ' грн/шт' + ' (минимум ' + trimCost  + ' грн)';
                $('[data-controlled-item=trim-selected]').html(text).show();
            } else if($('#trim').val() == 2) {
               var price = (+(width/1000 + height/1000)*6*2).toFixed(2);

                var text = '+ ' + price + ' грн/шт' + ' (минимум ' + 30  + ' грн)';

                $('[data-controlled-item=trim-selected]').html(text).show();
            } else if($('#trim').val() == 3) {
               var price = (+(width/1000 + height/1000)*6*2*1.5).toFixed(2);

                var text = '+ ' + price + ' грн/шт' + ' (минимум ' + 30  + ' грн)';

                $('[data-controlled-item=trim-selected]').html(text).show();
            }
            if($('#plot').val() != 0){
                var squereList = (width/1000*height/1000);
                var S_kv = squereList/$('#count_format').val();
                var l_kv = Math.sqrt(S_kv);
                var P_kv = l_kv*4;
                var P_all_kv = P_kv*$('#count_format').val();
                var price = (P_all_kv*2.5*($('#plot').val() == 2 ? 1.5 : 1)).toFixed(2);
                var text = '+ ' + price + ' грн/шт' + ' (минимум ' + 20  + ' грн)';
                $('[data-controlled-item=plot-selected]').html(text).show();
            } else {
                $('[data-controlled-item=plot-selected]').html('').hide();
            }

//            if($('#figcutting').val() != 0){
//               var price = ((width/1000 + height/1000)*5*2).toFixed(2);
//                if(price < 5){
//                    var text = '+ 5 грн (минимальная стоимость)';
//                } else {
//                    var text = '+ ' + (+price/5).toFixed(2)+' x 1.5 = ' + price +' грн';
//                }
//                $('[data-controlled-item=figcutting-selected]').html(text).show();
//               $('[data-controlled-item=figcutting-selected]').html(text).show();
//            } else {
//                $('[data-controlled-item=figcutting-selected]').html('').hide();
//            }

//        } else {
//            if($('#trim').val() == 1){
//                var price = ((width/1000 + height/1000)*1.5*2).toFixed(2);
//                if(price < 1.5){
//                    var text = '+ 1.5 грн (минимальная стоимость)';
//                } else {
//                    var text = '+ ' + (+price/1.5).toFixed(2)+' x 1.5 = ' + price +' грн';
//                }
//                $('[data-controlled-item=trim-selected]').html(text).show();
//            } else {
//                var price = ((width/1000 + height/1000)*5*2).toFixed(2);
//                if(price < 5){
//                    var text = '+ 5 грн (минимальная стоимость)';
//                } else {
//                    var text = '+ ' + (+price/5).toFixed(2)+' x 1.5 = ' + price +' грн';
//                }
//                $('[data-controlled-item=trim-selected]').html(text).show();
//            }
//        }
    }
    function IsJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }
    function konvLuv(val){
        if(val > 0){
            $('#luvers option').each(function(){
                if(karm_luv[val].indexOf(+$(this).attr('value')) != -1){
                    $(this).prop('disabled',true);
                } else {
                    $(this).prop('disabled',false);
                }
            });
        } else {
            $('#luvers option:disabled').prop('disabled',false);
        }
         $('#luvers').trigger("chosen:updated");
    }
    function luvKonv(val){
        if(val > 0){
            $('#karman option').each(function(){
                if(luv_karm[val].indexOf(+$(this).attr('value')) != -1){
                    $(this).prop('disabled',true);
                } else {
                    $(this).prop('disabled',false);
                }
            });
        } else {
            $('#karman option:disabled').prop('disabled',false);
        }
        $('#karman').trigger("chosen:updated");
    }
    function zavisColors(){
        var notblockP = new Array();
        if($('.select-mat:checked').length > 0){
            $('.select-mat:checked').each(function(){
                var id = this.id;
                for(q in zavis[id]){
                    if(zavis[id][q] > 0){
                        notblockP.push(q);
                    }
                }
            });
            notblockP = unique(notblockP);
            $('.select-print').each(function(){
                if(notblockP.indexOf($(this).val()) == -1){
                    if(!$(this).hasClass('disableMat')){
                        $(this).prop('disabled',true).addClass('disableMat');
                    }
                } else {
                    if($(this).hasClass('disableMat')){
                        $(this).removeClass('disableMat');
                        if(!$(this).hasClass('disabledColor') && !$(this).hasClass('disablePodv') && !$(this).hasClass('disabledSize')){
                            $(this).prop('disabled',false);
                        }
                    }
                }
            });
        } else {
            $('.select-print.disableMat').removeClass('disableMat');
            $('.select-print:not(.disabledColor):not(.disablePodv):not(.disabledSize):disabled').prop('disabled',false);
        }
    }
    function sizeRelations(){
        if(type == 'banner'){
            var printBlock = [1440,1442,2880,1441,1443];
            var dopMinus = 0;
            if($('#luvers').val() > 0 && $('#luvers_type').val() == 1){
                dopMinus = 60;
            }
            if(+width > (1580 - dopMinus) && +height > (1580 - dopMinus)){
                for(var i = 0; i < printBlock.length; i++){
                    if((+width < (2180 - dopMinus) || +height < (2180 - dopMinus)) && printBlock[i] == 1443){
                        $('#p_' + printBlock[i]+ ':not(.disabledColor):not(.disablePodv):disabled').prop('disabled',false).removeClass('disabledSize');
                        continue;
                    }
                    if($('#p_' + printBlock[i]).prop('checked')){
                        $('#p_' + printBlock[i]).prop('checked').change();
                    }
                    $('#p_' + printBlock[i]).prop('disabled',true).addClass('disabledSize');
                }
                if(+width > (3180 - dopMinus) && +height > (3180 - dopMinus)){
                    $('.adh-block').removeClass('hidden');
                    $('.sel-adh').prop('disabled',false).change();
                } else {
                    $('.adh-block:not(.hidden)').addClass('hidden');
                    $('.sel-adh').prop('disabled',true).change();
                }
            } else {
                for(var i = 0; i < printBlock.length; i++){
                    $('#p_' + printBlock[i] + ':not(.disabledColor):not(.disablePodv):disabled').prop('disabled',false);
                    $('#p_' + printBlock[i]).removeClass('disabledSize');
                }
                $('.adh-block:not(.hidden)').addClass('hidden');
                $('.sel-adh').prop('disabled',true).change();
            }
        } else {
            var blockLamSize = {
                'lam99' : 1520,
                'lam101' : 1520,
                'lam103' : 1600,
                'lam105' : 1370,
                'lam108' : 1370,
                'lam109' : 1370,
                'lam110' : 1300,
                'lam107' : 1600                
            }
            for(key in blockLamSize){
                if(+width > blockLamSize[key] && +height > blockLamSize[key]){
                    console.log($('#' + key))
                    $('#' + key + ':checked').prop('checked',false).change();
                    $('#' + key).prop('disabled',true).addClass('disabledSize');
                } else {
                    $('#' + key + '.disabledSize').removeClass('disabledSize').prop('disabled',false); 
                }
            }
        }
        if((+width < 1250 && +height < 2500) || (+width < 2500 && +height < 1250)){
            $('#trim option[value = 2]:disabled, #trim option[value = 3]:disabled').prop('disabled',false);
            $('#trim').trigger("chosen:updated");
        } else {
            if($('#trim').val() == 2 || $('#trim').val() == 3){
                $('#trim').val(0).change();
            }
            $('#trim option[value = 2]:not(:disabled),#trim option[value = 3]:not(:disabled)').prop('disabled',true);
            $('#trim').trigger("chosen:updated");
        }
    }
    function lamRelations(){
        if(type == 'film'){
            if($('.select-lam:checked').length == 1 && $('#lam107').prop('checked')){
                $('.select-mat:not(#m_3):checked').prop('checked',false).change();
                $('.select-mat:not(#m_3):not(.disabledLam)').addClass('disabledLam').prop('disabled','true');
            } else {
                $('.select-mat.disabledLam:not(.disabledShow):not(.disabledAdh):not(.disabledColor)').prop('disabled',false); 
                $('.select-mat.disabledLam').removeClass('disabledLam')
            }
            if(!$('#m_3').prop('checked') && !$('#selectall').prop('checked') && $('.select-mat:checked').length > 0){
                $('#lam107:checked').prop('checked',false).change();
                $('#lam107:not(.disabledFilm)').prop('disabled',true).addClass('disabledFilm');
            } else {
                $('#lam107.disabledFilm:not(.disabledSize)').prop('disabled',false);
                $('#lam107.disabledFilm').removeClass('disabledFilm');
            }
        }
    }
    function zavisMats(){
        var notblockP = new Array();
        if($('.select-print:checked').length > 0){
            $('.select-print:checked').each(function(){
                var id = $(this).val();
                for(key in zavis){
                   if(zavis[key][id] > 0){
                         notblockP.push(key);
                    }
                }
            });
            $('.select-mat').each(function(){
                if(notblockP.indexOf(this.id) == -1){
                    if(!$(this).hasClass('disablePrint')){
                        $(this).prop('disabled',true).addClass('disablePrint');
                    }
                } else {
                    if($(this).hasClass('disablePrint')){
                        $(this).removeClass('disablePrint');
                        if(!$(this).hasClass('disabledShow') && !$(this).hasClass('disabledColor') && !$(this).hasClass('disabledAdh') && !$(this).hasClass('disabledLam')){
                            $(this).prop('disabled',false);
                        }
                    }
                }
            });
        } else {
            $('.select-mat.disablePrint').removeClass('disablePrint');
            $('.select-mat:not(.disabledShow):not(.disabledAdh):not(.disabledColor):not(.disabledLam):disabled').prop('disabled',false);
        }
    }
    function unique(arr) {
      var obj = {};
      for (var i = 0; i < arr.length; i++) {
        var str = arr[i];
        obj[str] = true; // запомнить строку в виде свойства объекта
      }
      return Object.keys(obj); // или собрать ключи перебором для IE8-
    }
    function tmpHide(){
        $('.material').each(function(){
            var contRowAll = $(this).find('.print-row').length;
            var contRowHide = $(this).find('.print-row.hidden-by-print').length;
            if(contRowAll == contRowHide){
                $(this).addClass('hidden-by-print');
                $('#m_' + $(this).attr('data-id')).prop('disabled',true).prop('checked',false).addClass('disabledShow');
                if($('.select-mat:checked').length == 0){
                    $('#selectall:not(:checked)').prop('checked',true).change();
                }
            } else {
                $(this).removeClass('hidden-by-print');
                $('#m_' + $(this).attr('data-id') + ':not(.disabledAdh):not(.disablePrint)').prop('disabled',false);
                $('#m_' + $(this).attr('data-id')).removeClass('disabledShow');
            }
        });
    }
    function add_opacity_option_reset(){
        var allNull = true;
        $('.sel_post_rab').each(function(){
            if($(this).val() != 0){
                allNull = false;
            }
        });
        if($('#sets').val() != 1){
            allNull = false;
        }
        if(allNull){
            $('#option-reset:not(.reset_no_active)').addClass('reset_no_active');
        } else {
            $('#option-reset.reset_no_active').removeClass('reset_no_active');
        }
    }
    function tableToExcel( element ) {
       var html, link, blob, url, css;
       var dateNow = new Date();
       var day = dateNow.getDate();
       var month = dateNow.getMonth() + 1;
       var year = dateNow.getFullYear();
       var template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>' + $(element).html() + '</table></body></html>' ;
       blob = new Blob(['\ufeff',css + template], {
         type: 'application/vnd.ms-excel'
       });
       url = URL.createObjectURL(blob);
       link = document.createElement('A');
       link.href = url;
       link.download = 'sborkaPrice_' + day + '-' + (month < 10 ? 0 : '') + month + '-' + year + '.xls';  // default name without extension
       document.body.appendChild(link);
       if (navigator.msSaveOrOpenBlob ) navigator.msSaveOrOpenBlob( blob, 'sborkaPrice_' + day + '-' + (month < 10 ? 0 : '') + month + '-' + year + '.xls'); // IE10-11
           else link.click();  // other browsers
       document.body.removeChild(link);
     };

    function com(compressed) {
        "use strict";
        var i,
            dictionary = [],
            w,
            result,
            k,
            entry = "",
            dictSize = 256;
        for (i = 0; i < 256; i += 1) {
            dictionary[i] = String.fromCharCode(i);
        }
        w = String.fromCharCode(compressed[0]);
        result = w;
        for (i = 1; i < compressed.length; i += 1) {
            k = compressed[i];
            if (dictionary[k]) {
                entry = dictionary[k];
            } else {
                if (k === dictSize) {
                    entry = w + w.charAt(0);
                } else {
                    return null;
                }
            }
            result += entry;
            dictionary[dictSize++] = w + entry.charAt(0);
            w = entry;
        }
        function decode_utf8(s) {
          return decodeURIComponent(escape(s));
        }
        return decode_utf8(result);
    }
    function padT(){
        $(this).stop(true,true).delay(300).queue(function(){
            var removeOpacity = function(){
                $('.gallery').css('opacity',1);
            };
            $('.gallery').animateCss('fadeIn',removeOpacity);
            $(this).dequeue();

        });
    }

    /*END FUNCTION*/
    // при первой загрузке для смены тиражей в модальном наценки
    $(".tirazh").each(function(){
        change_text_modal_tirazh( $(this).attr('numb_tir'), $(this).val() );
    });

    $(".tirazh").bind('change keyup',function(){
        if($(this).val() != ''){
            change_text_modal_tirazh( $(this).attr('numb_tir'), $(this).val() );
            if(!firstLoad){             $("#pricefilters").submit();         }
        }
	});
    if(parametrLoad == 'return' || parametrLoad == 'product'){
        $.getScript('cside/price_js/repeat_order.js').done(function( script, textStatus ) {
            loadPage();
        });
     } else {
        $(this).queue(function(){
            if($('#size_type2:checked').length > 0){
                $('#size_type1').prop('checked',true).change();
            }
            $('#sel_razmer').change().trigger("chosen:updated");
            $("#pricefilters input:checked").change();
            $('select.sel_post_rab').each(function(){
                if($(this).val() != 0){
                    $(this).change().trigger("chosen:updated");
                }
            });
            $(this).dequeue();
        });
        $(this).queue(function(){
            add_opacity_option_reset();
            add_opacity_filter_reset();
            $("#pricefilters").submit();

            firstLoad = false;
            $(this).dequeue();
        });
     }
    $('.types-prod').click(function () {
        location.href = $(this).attr('data-link');
    });

    /*$("a.a-no-decor.print-price ").on("click",function () {
        console.log("heh");
    })*/

    $(".tabs-style-2 .mini_padding > li").click(function (e) {
        e.preventDefault();
        //console.log("hhh");
        $(".tabs-style-2 .mini_padding > li").removeClass("active");
        $(this).addClass("active");

    });
    $(".tabs-style-3 .nav-tabs > li").click(function(e){
        e.preventDefault();
        //console.log("hhh");
        $(".tabs-style-3 .nav-tabs > li").removeClass("selected-product");
        $(this).addClass("selected-product");
    });







});
