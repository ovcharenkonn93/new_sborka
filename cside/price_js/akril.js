$(document).ready(function(){
    function close_all_tir_drop(){
        $('.wrap_tir').css('background-color','#4a76a8');
        $('.new_i').removeClass('fa-caret-up');
        $('.new_i').addClass('fa-caret-down');
        $('.new_i').css('display','block');
        $('.new_i').css('color','#fff');
        $('.new_ul').css('display','none');
        $('.new_inp').css('width','35px');
    }
    $('.new_inp').focus(function(){
        close_all_tir_drop();
        $(this).val('');
        $(this).css('width','55px');
        $(this).parent().css('background-color','#fff');
//            $(this).parent().find('i').css('color','#333');
        $(this).parent().find('i').removeClass('fa-caret-down');
        $(this).parent().find('i').addClass('fa-caret-up');
        $(this).parent().find('i').css('display','none');
        $(this).parent().find('.new_ul').css('display','block');
    });
    $('.new_i').click(function(){
        if($(this).hasClass('fa-caret-down')){
            var name2 = document.getElementById($(this).attr('input_name'));
            name2.focus();
//                name2.selectionStart = name2.value.length;
        }else{
            close_all_tir_drop();
        }
    });

//        $('li').click(function(){
//            $(this).parents('.wrap_tir').find('input').val($(this).text()).change();
//            close_all_tir_drop()
//        });

    $(document).mouseup(function (e){
        var caret = $('.fa-caret-up'),
            inp = $('.new_inp'),
            ul_li = $('.new_ul li');
        if (!caret.is(e.target) && !ul_li.is(e.target) && !inp.is(e.target)){
            close_all_tir_drop();
        }
    });
    $(".tirazh").bind('blur',function(){
        if($(this).val() == ''){
            $(this).val($(this).attr('placeholder2'));
            change_text_modal_tirazh( $(this).attr('numb_tir'), $(this).val() );
            $("#pricefilters").submit();
        }else{
            change_text_modal_tirazh( $(this).attr('numb_tir'), $(this).val() );
            $(this).attr('placeholder2',$(this).val());
            $("#pricefilters").submit();
        }
    });
    $(".tirazh").keypress(function(e){
        if(e.keyCode==13){
            $(".tirazh").blur();
            close_all_tir_drop();
        }
    });
});

$('document').ready(function () {
    //        Galleria.loadTheme('cside/plugin/gallery/galleria.classic.js');
    Galleria.configure({
        thumbFit: true,
        layerFollow: true,
        showInfo: false,
        showCounter: false,
        imagePosition: 'top',
        imageCrop: false,
        thumbCrop: true,
        wait: true,
        debug: false,
        idleMode: 'hover',
        idleSpeed: 300,
        lightbox: true,
        popupLinks: false
    });
    // -------------------------    МОДАЛЬНЫЕ ОКНА С ИНФО   ---------------------------------
    // Для добавления модального окна на страницу
    // нужно в елемент по которому кликаем
    // добавить атрибут data-sborka-modal-info="id_modal"
    // где "id_modal это id(без #!!!) полностью сверстанного
    // модального окна с id="id_modal" которое находится
    // в условии: if( isset( $_GET['id_modal'] ) )
    // в файле 'modal_info.php'
    $('body').on('click', '[data-sborka-modal-info]', function () {
        var modal_id = this.getAttribute('data-sborka-modal-info');
        // если нужная инфошка уже была показана
        if ($('#' + modal_id).length > 0) {
            $('#' + modal_id).modal("show");
            //            console.log('yes');
            // если нужной инфошки еще нету на странице -> загружаем ее
        } else {
            var url = "cside/modal_info.php?" + modal_id;
            $.ajax({
                type: "POST",
                url: url,
                success: function (data) {
                    // если нету нужной инфошки
                    if (data == 'no_info') {
                        // если инфошка error уже была показана
                        if ($('#info_error').length != 0) {
                            $('#info_error').modal('show');
                            // если инфошки error еще нету на странице
                        } else {
                            $.get('cside/modal_info.php?info_error', function (txt) {
                                $(this).queue(function () {
                                    $('body').append(txt);
                                    $(this).dequeue();
                                });
                                $('#info_error').modal('show');
                            });
                        }
                        // если нашли нужную инфошку ПОКАЗЫВАЕМ ее
                    } else {
                        $(this).queue(function () {
                            $('body').append(data);
                            $(this).dequeue();
                        });
                        $(this).queue(function () {
                            if ($('#' + modal_id + ' .galleria').length > 0) {
                                $('#' + modal_id + ' .galleria').galleria();
                            }
                            $(this).dequeue();
                        });
                        $(this).queue(function () {
                            jQuery(function () {
                                $('a.zoom').easyZoom({
                                    parent: '.zoom-block'
                                });
                                $('a.zoom_uv').easyZoom({
                                    parent: '.zoom-block_uv',
                                    id: 'easy_zoom_uv'
                                });
                                $('a.zoom_holst').easyZoom({
                                    parent: '.zoom-block_holst',
                                    id: 'easy_zoom_holst'
                                });
                            });
                            $(this).dequeue();
                        });
                        $(this).queue(function () {
                            $('#' + modal_id).modal('show');
                            $(this).dequeue();
                        });
                    }
                },
                error: function () {
                    //                    console.log('error ajax modal');
                }
            });
        }
    });
});

$.fn.extend({
    animateCssShow: function (animationName) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        this.addClass('animated open ' + animationName).one(animationEnd, function() {
            $(this).removeClass('animated ' + animationName);
        });
    },
    animateCssHide: function (animationName) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
         $(this).addClass('animated ' + animationName).one(animationEnd, function() {
            $(this).removeClass('animated open ' + animationName);
        });
    },
    animateCss: function (animationName,afterFunc) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
         $(this).addClass('animated ' + animationName).one(animationEnd, function() {
            if(afterFunc != ''){
                afterFunc();
            }
            $(this).removeClass('animated ' + animationName);
        });
    }
});
var oldBlock = [];
var active = 0;
var activeP = 0;
var xhrM = '';
var xhrP = '';
var insert = '<p class="text-center" style="font-size: 50px;padding: 20px 0;"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span></p>';
var width = 0;
var height = 0;
var firstLoad = true;
var insertNotSel = '<tr><td colspan="9" class="text-center" style="vertical-align:middle;padding:20px;font-weight:500;font-size:22px;">Для формирования прайс-листа выберите материал</td></tr>';
var flagReset = true;
// функция смены текста тиража в модальном наценки
function change_text_modal_tirazh(name,val_inp){
    $('.m_d_tir_'+name).html(val_inp);
}
var zavis = {
    m_139 : {0:550,1443:700,1444:0,1446:0,1445:0},
    m_140 : {0:750,1443:900,1444:0,1446:0,1445:0},
    m_134 : {0:550,1443:700,1444:850,1446:850,1445:850},
    m_136 : {0:750,1443:900,1444:1050,1446:1050,1445:1050}
};
$('document').ready(function(){
    function zavisColors(){
        var notblockP = new Array();
        if($('.select-mat:checked').length > 0){
            $('.select-mat:checked').each(function(){
                var id = this.id;
                for(q in zavis[id]){
                    if(zavis[id][q] > 0){
                        notblockP.push(q);
                    }
                }
            });
            notblockP = unique(notblockP);
            $('.select-print').each(function(){
                if(notblockP.indexOf($(this).val()) == -1){
                    if(!$(this).hasClass('disableMat')){
                        $(this).prop('disabled',true).addClass('disableMat');
                    }
                } else {
                    if($(this).hasClass('disableMat')){
                        $(this).removeClass('disableMat');
                        if(!$(this).hasClass('disabledColor') && !$(this).hasClass('disableClass')){
                            $(this).prop('disabled',false);
                        }
                    }
                }
            });
        } else {
            $('.select-print.disableMat').removeClass('disableMat');
            $('.select-print:not(.disabledColor):not(.disableClass):disabled').prop('disabled',false);
        }
    }
    function unique(arr) {
      var obj = {};
      for (var i = 0; i < arr.length; i++) {
        var str = arr[i];
        obj[str] = true; // запомнить строку в виде свойства объекта
      }
      return Object.keys(obj); // или собрать ключи перебором для IE8-
    }
    $('select.sel_post_rab,#lam,#sel_razmer,.sel_metr,#prints').chosen({width: "100%",disable_search_threshold: 10,disable_search: true});
    $('#price').popover({
        selector: '[data-toggle=tooltip]',
        placement: 'top',
        html: true,
        trigger: 'hover'
    });
    function removeAnim(){
        $('.animated.zoomIn').removeClass('animated zoomIn');
    }
    setTimeout(removeAnim,1000);

    $('.t_greyblock,.header-opt').popover({
        selector: '[data-toggle=tooltip]',
        placement: 'top',
        html: true,
        trigger: 'hover'
    });
    $('.t_greyblock,.header-opt').popover({
        selector: '[data-toggle=tooltip]',
        placement: 'top',
        html: true,
        trigger: 'hover'
    });
    $('.input_discount').on('input', function(){
        if (this.value.match(/[^0-9.]/g)) {
            this.value = this.value.replace(/[^0-9.]/g, '');
        }
    });
    $('body').on('click','#m_d_submit',function(){
            $('.d_m_d_variant').eq( $('.m_d_variant:checked').val() ).prop('checked',true);
            $('#d_m_d_all').val( $('#m_d_all').val() );
            $('#d_m_d_t_1').val( $('#m_d_t_1').val() );
            $('#d_m_d_t_2').val( $('#m_d_t_2').val() );
            $('#d_m_d_t_3').val( $('#m_d_t_3').val() );
            $('#d_m_d_t_4').val( $('#m_d_t_4').val() );
            $('#d_m_d_t_5').val( $('#m_d_t_5').val() );
            $('#d_m_d_t_6').val( $('#m_d_t_6').val() );
            $(this).closest('.modal').modal('hide');
            $("#pricefilters").submit();
        });
         $('body').on('click','#d_m_d_submit',function(){
             $(this).closest('.modal').modal('hide');
             pricePints(printClick);
         });

        $('body').on('change keyup','#m_d_all',function(){
            $('#m_d_t_1,#m_d_t_2,#m_d_t_3,#m_d_t_4,#m_d_t_5,#m_d_t_6').val($(this).val());
        });
        $('body').on('change keyup','#m_d_t_1,#m_d_t_2,#m_d_t_3,#m_d_t_4,#m_d_t_5,#m_d_t_6',function(){
            $('#m_d_all').val('');
        });
        $('body').on('change keyup','#d_m_d_all',function(){
            $('#d_m_d_t_1,#d_m_d_t_2,#d_m_d_t_3,#d_m_d_t_4,#d_m_d_t_5,#d_m_d_t_6').val($(this).val());
        });
        $('body').on('change keyup','#d_m_d_t_1,#d_m_d_t_2,#d_m_d_t_3,#d_m_d_t_4,#d_m_d_t_5,#d_m_d_t_6',function(){
            $('#d_m_d_all').val('');
        });

    $("#pricefilters").submit(function() {
        if(active != 0){
            xhrM.abort();
        }

        var dataSend = $("#pricefilters").serialize();
        xhrM = $.ajax({
            type: "POST",
            url: "cside/ajax.php?action=priceakril",
            data: dataSend,
            dataType: 'json',
            beforeSend: function(){
                active++;
                $('#insert-in').html('');
                $('#loading-table').html(insert);
                console.log(dataSend)
            },
            complete: function(data){
                if(data.responseText == 'error'){
                    location.reload();
                } else if(data.statusText == 'OK' && IsJsonString(data.responseText) == false){
                    $('#loading-table').html('');
                    var text = '<tr><td colspan="9" class="text-center" style="vertical-align:middle;padding:20px;font-weight:500;font-size:22px;">При подсчете произошла ошибка! Пожалуйста сообщите нам, об этом нажав на кнопку "Нашли ошибку".</td></tr>';
                    $('#insert-in').html(text);
                }
            },
            success: function(data){
                 $('#loading-table').html('');
                 $('#insert-in').html(com(data.text));
//                tmpHide();
                active=0;
                var checkM = $('#selectall:checked').length + $('.select-mat:checked').length;
                var checkT = $('#alltime:checked').length + $('.terms:checked').length; 
                var checkP = $('#all-print:checked').length + $('.select-print:checked').length;
                if(checkM > 0 && checkP > 0 && checkT > 0){
                    $('.time-select.hidden').removeClass('hidden').animateCss('fadeIn','');
                    $('#alltime').closest('.t_switchable').find('.filter-name').html('Cрок печати');
                } 
                xhrM = '';
            }
        });
        return false;
    });
    $('.print-price').click(function(){
        if($(this).hasClass('icon_print')){
            printClick = 'print';
        } else if($(this).hasClass('icon_exel')) {
            printClick = 'excel';
        } else if($(this).hasClass('icon_exel_all')) {
            printClick = 'excel_all';
        }
    });
    $('.wrap_radio2').on('click',function(){
        if($(this).children('.krug2').attr('position') == 'left'){
            $('[name=cost_var]').eq(1).prop('checked',true).change();
            $('.cost_tir').css('display','none');
            $('.cost_one').css('display','block');
            $('.t_tir').css('opacity','0.5');
            $(this).children('.krug2').attr('position','right');
            $(this).stop(true,true).children('.krug2').animate({
                left:'100%',
                marginLeft: '-20px'
            },250);
            $(this).delay(250).queue(function(){
                $('.t_one').css('opacity','1');
                $(this).dequeue();
            });
        }else{
            $('[name=cost_var]').eq(0).prop('checked',true).change();
            $('.cost_tir').css('display','block');
            $('.cost_one').css('display','none');
            $('.t_one').css('opacity','0.5');
            $('.krug2').removeClass('radio_bg2');
            $(this).children('.krug2').attr('position','left');
            $(this).stop(true,true).children('.krug2').animate({
                left:'0',
                marginLeft: '0'
            },250);
            $(this).delay(250).queue(function(){
                $('.t_tir').css('opacity','1');
                $(this).dequeue();
            });
        }
    });
    function pricePints(type){
        if(activeP != 0){
            xhrP.abort();
        }
        var urlsend = "cside/ajax.php?action=priceakril&type=print";
        var dateNow = new Date();
        var day = dateNow.getDate();
        var month = dateNow.getMonth() + 1;
        var year = dateNow.getFullYear();
        var dataSend = $("#pricefilters").serialize() + '&' + $('[name="form_discount_download"]').serialize();
        xhrP = $.ajax({
            type: "POST",
            url: urlsend,
            data: dataSend,
            dataType: 'json',
            beforeSend: function(){
                activeP++;
            },
            success: function(data){
                if(type == 'print'){
                    $(com(data.text)).print({
                        globalStyles: true,
                        mediaPrint: false,
                        stylesheet: "cside/print-css.css",
                        noPrintSelector: ".no-print",
                        iframe: true,
                        append: null,
                        prepend: null,
                        manuallyCopyFormValues: true,
                        deferred: $.Deferred(),
                        timeout: 750,
                        title: null,
                        doctype: '<!doctype html>'
                    });
                } else if(type == 'excel'){
                    console.log(com(data.text))
                    tableToExcel(com(data.text));
                }
                activeP=0;
                xhrP = '';
            }
        });
        return false;
    };
    $(".sel_post_rab").change(function(){
        if($(this).val() > 0 && this.id == 'print_color'){
            $('#p_0').prop('checked',false).prop('disabled',true).change();
        } else if($(this).val() == 0 && this.id == 'print_color'){
            $('#p_0').prop('disabled',false);
        }

        if(this.id == 'trim' && $(this).val() > 1){
            $('.block-plot.hidden').removeClass('hidden');
            $('#count_format_contour:disabled').prop('disabled',false).change().trigger("chosen:updated");
             if($('#guard_angle').val() > 0){
                $('#guard_angle').val(0).change().trigger("chosen:updated");
            }
        } else if(this.id == 'trim' && $(this).val() <= 1) {
            $('.block-plot:not(.hidden)').addClass('hidden');
            $('#count_format_contour:not(:disabled)').prop('disabled',true).change();
        }
        if(this.id == 'guard_angle' && $('#trim').val() > 1){
            $('#trim').val(1).change().trigger("chosen:updated");
        }
        calculate();
        changepic();
        add_opacity_option_reset();
        if(!firstLoad){
            $("#pricefilters").submit();
        }
    });
    $("#option-reset:not(.reset_no_active)").click(function(){
        if($('#trim').val() != 1){
            $('#trim').val(1).change().trigger("chosen:updated");
        }
        if($('#sets').val() != 1){
            $('#sets').val(1).change();
        }
        $('.sel_post_rab:not(#trim)').each(function(){
            if($(this).val() != 0){
                $(this).val(0).change().trigger("chosen:updated");
            }
        });

        add_opacity_option_reset();
    });
    function calculate(){
        if($("#skotch").val() != 0) {
//            var price = (+(width * height/1000000).toFixed(3) <= 0.1 ? (10).toFixed(2) : ((width * height/1000000).toFixed(3)*10).toFixed(2));
//             var price = 10;
            var squereList = (width*height)/1000000;
            var S_kv = squereList/($('#count_format_contour').length ? $('#count_format_contour').val() : 1);
            var l_kv = Math.sqrt(S_kv);
            var P_kv = l_kv*4;

            var P_all_kv = P_kv*($('#count_format_contour').length ? $('#count_format_contour').val() : 1);
            var price = ((P_all_kv*5.5).toFixed(2) > 11 ? (P_all_kv*5.5).toFixed(2) : 11);

//            var price = ( ((width/1000+height/1000)*2*5).toFixed(2) > 10 ? ((width/1000+height/1000)*2*5).toFixed(2) : 10);
             var text = '+ ' + price +' грн/шт (минимум 11 грн)';
             $("#skotch").closest('.pw-cont').find('.selected-show').html(text).show();
        } else {
           $("#skotch").closest('.pw-cont').find('.selected-show').html('').hide();
        }
        if($('#print_color').val() > 0){
            var price = +((width * height/1000000)*150).toFixed(2);
            var text = '+ ' + (+(width * height/1000000)).toFixed(2)+' x 150 = ' + price +' грн';
            $("#print_color").closest('.pw-cont').find('.selected-show').html(text).show();
        } else {
            $("#print_color").closest('.pw-cont').find('.selected-show').html('').hide();
        }
        if($('#pvh_drilling').val() > 0){
            var count = $('#pvh_drilling').val();
            var price = +(count*2).toFixed(2);

            var text = '+ ' + count +' x 2 = ' + price +' грн';

            $("#pvh_drilling").closest('.pw-cont').find('.selected-show').html(text).show();

        } else {

            $("#pvh_drilling").closest('.pw-cont').find('.selected-show').html('').hide();

        }
        if($('#guard_angle').val() > 0){
            var price = 8;
            var text = '+ ' + price +' грн/изделие';
            $("#guard_angle").closest('.pw-cont').find('.selected-show').html(text).show();
        } else {
            $("#guard_angle").closest('.pw-cont').find('.selected-show').html('').hide();
        }
        if($('#trim').val() == 1){
            var price = ((width/1000 + height/1000)*12*2).toFixed(2);
            var text = '+ ' + price + ' грн/шт' + ' (минимум ' + 12  + ' грн)';
           $('#trim').closest('.pw-cont').find('.selected-show').html(text).show();
        } else {
            var squereList = (width/1000*height/1000);
            var S_kv = squereList/( $('#count_format_contour').length ? $('#count_format_contour').val() : 1);
            var l_kv = Math.sqrt(S_kv);
            var P_kv = l_kv*4;
            var P_all_kv = P_kv*( $('#count_format_contour').length ? $('#count_format_contour').val() : 1);
            var price = (P_all_kv*18*($('#trim').val() == 3 ? 1.5 : 1)).toFixed(2);

            var text = '+ ' + price + ' грн/шт' + ' (минимум ' + 18  + ' грн)';

            $('#trim').closest('.pw-cont').find('.selected-show').html(text).show();
        }
    }
    $('#count_format_contour').change(function(){
        calculate();
        if(!firstLoad){
            $("#pricefilters").submit();
        }
    });
    function add_opacity_option_reset(){
        var allNull = true;
        $('.sel_post_rab:not(#trim)').each(function(){
            if($(this).val() != 0){
                allNull = false;
            }
        });
        if($('#sets').val() != 1){
            allNull = false;
        }
        if($('#trim').val() != 1){
            allNull = false;
        }
        if(allNull){
            $('#option-reset:not(.reset_no_active)').addClass('reset_no_active');
        } else {
            $('#option-reset.reset_no_active').removeClass('reset_no_active');
        }
    }
    function add_opacity_filter_reset(){
        var checkM = $('#selectall:checked').length + $('.select-mat:checked').length;
        var checkT = $('#alltime:checked').length + $('.terms:checked').length;
        var checkP = $('#all-print:checked').length + $('.select-print:checked').length;
        var sum = checkM + checkT + checkP;
        if( sum == 0)
        {
            $('#filter-reset').addClass('reset_no_active');
        }else{
            $('#filter-reset').removeClass('reset_no_active');
        }
    }
    /*Обработчики фильтров*/
    $('#selectall').change(function(){
        $(this).queue(function(){
            $('.select-mat:checked').prop('checked',false);
            zavisColors();
            $(this).closest('.t_switchable').find('.filter-name').html('Материал');
            add_opacity_filter_reset();
            $(this).dequeue();
        });
        $(this).queue(function(){
            setSubmit();
            $(this).dequeue();
        });
    });

    $('#alltime').change(function(){
        $(this).queue(function(){
            $('.terms:checked').prop('checked',false);
            $(this).closest('.t_switchable').find('.filter-name').html('Срок печати');
            add_opacity_filter_reset();
            $(this).dequeue();
        });
        $(this).queue(function(){
            setSubmit();
            $(this).dequeue();
        });
    });
    $('#all-print').change(function(){
        $(this).queue(function(){
            $('.select-print:checked').prop('checked',false);
            $(this).closest('.t_switchable').find('.filter-name').html('Качество печати');
            add_opacity_filter_reset();
            $(this).dequeue();
        });
        $(this).queue(function(){
            setSubmit();
            $(this).dequeue();
        });
    });

    $('.select-mat').change(function(){
        $(this).queue(function(){
            $('#selectall:checked').prop('checked',false);
            $('#selectall:not(:disabled)').closest('.t_switchable').find('.filter-name').html('Выбрать все<br>материалы');
            zavisColors();
            add_opacity_filter_reset();
            $(this).dequeue();
        });
        $(this).queue(function(){
            setSubmit();
            $(this).dequeue();
        });
    });
    $(".select-print").change(function(){
        $(this).queue(function(){
            $('#all-print:checked').prop('checked',false);
            $('#all-print:not(:disabled)').closest('.t_switchable').find('.filter-name').html('Выбрать все<br>печати');
            add_opacity_filter_reset();
            $(this).dequeue();
        });
        $(this).queue(function(){
            setSubmit();
            $(this).dequeue();
        });
    });
    $(".terms").change(function(){
        $(this).queue(function(){
            $('#alltime:checked').prop('checked',false);
            $('#alltime:not(:disabled)').closest('.t_switchable').find('.filter-name').html('Выбрать все<br>сроки печати');
            add_opacity_filter_reset();
            $(this).dequeue();
        });
        $(this).queue(function(){
            setSubmit();
            $(this).dequeue();
        });
    });
    function setSubmit(){
        var checkM = $('#selectall:checked').length + $('.select-mat:checked').length;
        var checkT = $('#alltime:checked').length + $('.terms:checked').length;
        var checkP = $('#all-print:checked').length + $('.select-print:checked').length;
        if($('#print_color').val() == 1){
            checkM = 1;
        }
        if(checkM > 0 && checkP > 0 && checkT > 0){
            if(flagReset){
                flagReset = false;
                $('.select-all-btn').each(function () {
                   if(this.id == 'selectall'){
                      $(this).closest('.t_switchable').find('.filter-name').html('Выбрать все<br>материалы');
                   } else if (this.id == 'alltime') {
                     $(this).closest('.t_switchable').find('.filter-name').html('Выбрать все<br>сроки печати');
                   } else if (this.id == 'all-print') {
                     $(this).closest('.t_switchable').find('.filter-name').html('Выбрать все<br>печати');
                   }
                });
            }
            $('.select-all-btn').prop('disabled',false);
            $("#pricefilters").submit();
        } else {
            var insertNotSel = '<tr><td colspan="9" class="text-center" style="vertical-align:middle;padding:20px;font-weight:500;font-size:22px;">Для формирования прайс-листа выберите';
            var text = '';
            if(checkM == 0){
                text += ' материал,';
            }
            if(checkP == 0){
                text += ' печать,';
            }
            if(checkT == 0){
                text += ' срок печати,';
            }
            text = text.substring(0, text.length - 1)
            insertNotSel += text + '</td></tr>';
            $('#insert-in').html(insertNotSel);
        }
    }
    $('#filter-reset').click(function(){
        $(this).queue(function(){
            $('#alltime:checked,#all-print:checked,.select-mat:checked,#selectall:checked,.terms:checked,.select-print:checked').prop('checked',false).change();
            $('#selectall').closest('.t_switchable').find('.filter-name').html('Материал');
            $('#alltime').closest('.t_switchable').find('.filter-name').html('Срок печати');
            $('#all-print').closest('.t_switchable').find('.filter-name').html('Печать');
            zavisColors();
            $(this).dequeue();
        });
        $(this).queue(function(){
            $('.select-all-btn').prop('disabled',true);
            add_opacity_filter_reset();
            flagReset = true;
            $(this).dequeue();
        });
    });
    /*END FILTERS*/

    $('#sel_razmer').change(function(e) {
        width = +$(this).find('option:selected').data('width');
        height = +$(this).find('option:selected').data('height');
        $('.input_size').val('');
        $('[name="size_in"]').val(1).change().trigger("chosen:updated");
        $('#size_input').data("width",width).data("height",height);
        $('#size_input').attr('value',$(this).val()).change();
    });
    $('.input_size').on('input',function(){
        if (this.value.match(/[^0-9.]/g)) {
            this.value = this.value.replace(/[^0-9.]/g, '');
        }
    });
    $('.input_size').on('change',function(){
        $('#sel_razmer').val(0).trigger("chosen:updated");
        if(($('#input-width1').val()*$('[name="size_in"]').val()) < 50){
            $('#input-width1').val(50/$('[name="size_in"]').val());
        }
        if(($('#input-width1').val()*$('[name="size_in"]').val()) > 900){
             $('#input-width1').val(900/$('[name="size_in"]').val());
        }
        if( ($('#input-height1').val()*$('[name="size_in"]').val()) < 50){
            $('#input-height1').val(50/$('[name="size_in"]').val());
        }
        if(($('#input-height1').val()*$('[name="size_in"]').val()) > 900){
             $('#input-height1').val(900/$('[name="size_in"]').val());
        }
        
        width = +$('#input-width1').val()*$('[name="size_in"]').val();
        height = +$('#input-height1').val()*$('[name="size_in"]').val();
        $('#input-width').val(width);
        $('#input-height').val(height);
        
        $('#size_input').data("width",width).data("height",height);
        $('#size_input').attr('value',30).change();
        if(!firstLoad){             $("#pricefilters").submit();         }
    });
    $('[name="size_in"]').change(function(){
         var wid = $('#input-width1').val(),
            hei = $('#input-height1').val();
        if( wid != '' && hei != '' && +$('#size_input').val() == 30){
            if( $(this).val() == 10 ){
                $('#input-width1').val(wid/10).trigger('change');
                $('#input-height1').val(hei/10).trigger('change');
            }else{
                $('#input-width1').val(wid*10).trigger('change');
                $('#input-height1').val(hei*10).trigger('change');
            }
        }
    });
    $('#size_input').change(function(){
        if(parametrLoad != 'product' || (parametrLoad == 'product' && $('#rectangle').length > 0)){
            if($(this).val() == 30){
               smena_razmera(width,height,'Ваш размер');
            } else {
               smena_razmera(width,height,'');
            }
        }
        var infoShow = true;
        if((((height  > 1000) && (width > 1000)) || ((height  > 1000) && (width > 1000)) || (height  > 1500) || (width  > 1500)) 
               && infoShow)
        {
//                infoShow = false;
                $('#modal_error_max_size').modal('show');
        }
        calculate();
        sizeRelations();
        if(!firstLoad){             $("#pricefilters").submit();         }
    });
    function sizeRelations(){
        if((height < 1250 && height < 2500) || (height < 2500 && height < 1250)){
            $('#trim option[value = 2]:disabled, #trim option[value = 3]:disabled').prop('disabled',false);
            $('#trim').trigger("chosen:updated");
        } else {
            if($('#trim').val() == 2 || $('#trim').val() == 3){
                $('#trim').val(0).change();
            }
            $('#trim option[value = 2]:not(:disabled),#trim option[value = 3]:not(:disabled)').prop('disabled',true);
            $('#trim').trigger("chosen:updated");
        }
    }
    if($(window).width() < 991){
         $('.header-opt').click(function(){
            if($(this).hasClass('openChild') == false){
                $(this).addClass('openChild');
                $(this).closest('.mobile-control').find('.mobile-droped').stop(true,true).animateCssShow('slideMenuDown');
                $(this).closest('.mobile-control').find('.icon-drop .fa-plus').fadeOut(250);
                $(this).closest('.mobile-control').find('.icon-drop .fa-minus').fadeIn(250);
            } else {
                $(this).removeClass('openChild');
                $(this).closest('.mobile-control').find('.mobile-droped').stop(true,true).animateCssHide('slideMenuTop');
                $(this).closest('.mobile-control').find('.icon-drop .fa-minus').fadeOut(250);
                $(this).closest('.mobile-control').find('.icon-drop .fa-plus').fadeIn(250);
            }

        });
    } else {
        padT();
    }
    $('.types-prod').click(function () {
        location.href = $(this).attr('data-link');
    });
    /*FUNCTIONS*/
    function tableToExcel( element ) {
       var html, link, blob, url, css;
       var dateNow = new Date();
       var day = dateNow.getDate();
       var month = dateNow.getMonth() + 1;
       var year = dateNow.getFullYear();
       var template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>' + $(element).html() + '</table></body></html>' ;
       blob = new Blob(['\ufeff',css + template], {
         type: 'application/vnd.ms-excel'
       });
       url = URL.createObjectURL(blob);
       link = document.createElement('A');
       link.href = url;
       link.download = 'sborkaPrice_' + day + '-' + (month < 10 ? 0 : '') + month + '-' + year + '.xls';  // default name without extension
       document.body.appendChild(link);
       if (navigator.msSaveOrOpenBlob ) navigator.msSaveOrOpenBlob( blob, 'sborkaPrice_' + day + '-' + (month < 10 ? 0 : '') + month + '-' + year + '.xls'); // IE10-11
           else link.click();  // other browsers
       document.body.removeChild(link);
     };
//    function tmpHide(){
//        $('.material').each(function(){
//            var contRowAll = $(this).find('.print-row').length;
//            var contRowHide = $(this).find('.print-row.hidden-by-print').length;
//            if(contRowAll == contRowHide){
//                $(this).addClass('hidden-by-print');
//                $('#m_' + $(this).attr('data-id')).prop('disabled',true).prop('checked',false);
//                if($('.select-mat:checked').length == 0){
//                    $('#selectall:not(:checked)').prop('checked',true).change();
//                }
//            } else {
//                $(this).removeClass('hidden-by-print');
//                $('#m_' + $(this).attr('data-id')).prop('disabled',false);
//            }
//        });
//    }
    function padT(){
        $(this).stop(true,true).delay(300).queue(function(){
            var removeOpacity = function(){
                $('.gallery').css('opacity',1);
            };
            $('.gallery').animateCss('fadeIn',removeOpacity);
            $(this).dequeue();

        });
    }
    function com(compressed) {
        "use strict";
        // Build the dictionary.
        var i,
            dictionary = [],
            w,
            result,
            k,
            entry = "",
            dictSize = 256;
        for (i = 0; i < 256; i += 1) {
            dictionary[i] = String.fromCharCode(i);
        }

        w = String.fromCharCode(compressed[0]);
        result = w;
        for (i = 1; i < compressed.length; i += 1) {
            k = compressed[i];
            if (dictionary[k]) {
                entry = dictionary[k];
            } else {
                if (k === dictSize) {
                    entry = w + w.charAt(0);
                } else {
                    return null;
                }
            }

            result += entry;

            // Add w+entry[0] to the dictionary.
            dictionary[dictSize++] = w + entry.charAt(0);

            w = entry;
        }
        function decode_utf8(s) {
          return decodeURIComponent(escape(s));
        }
        return decode_utf8(result);
    }

    /*END FUNCTION*/

// при первой загрузке для смены тиражей в модальном наценки
    $(".tirazh").each(function(){
        change_text_modal_tirazh( $(this).attr('numb_tir'), $(this).val() );
    });

    $(".tirazh").bind('change',function(){
        if($(this).val() != ''){
            change_text_modal_tirazh( $(this).attr('numb_tir'), $(this).val() );
            if(!firstLoad){             $("#pricefilters").submit();         }
        }
	});
    /*INPUTS CONTROL*/
    $('.input-option').on("input change",function(){
        var selector = '[data-controlled-item=' + this.id + '-selected]';
        if (this.value.match(/[^0-9]/g)) {
            this.value = this.value.replace(/[^0-9]/g, '');
        }
        if($(this).val() < 1 && $(this).val() != ''){$(this).val(1).change();}
    });
    $('.input-option').on('focusout',function(){
        if($(this).val() == ''){$(this).val(1).change();}
    });
    $('.input-option').on("change",function(){
        if(!firstLoad){             $("#pricefilters").submit();         }
        if(this.id == 'sets'){
//            if($(this).val() != 1){$('#text_info_sets').css('display','block');}else{$('#text_info_sets').css('display','none');}
            if($(this).val() > 50){$(this).val(50).change();}
        }
        add_opacity_option_reset();
     });
    $('.plus-input-val').click(function(){
        var selector = $(this).attr("data-change-input");
        $('#' + selector).val(parseInt($('#' + selector).val()) + 1).change();
    });
    $('.minus-input-val').click(function(){
         var selector = $(this).attr("data-change-input");
        $('#' + selector).val(parseInt($('#' + selector).val()) - 1).change();
    });

    function IsJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }
    if(parametrLoad == 'return' || parametrLoad == 'product'){
        $.getScript('cside/price_js/repeat_order.js').done(function( script, textStatus ) {
            loadPage();
        });
     } else {
        $(this).queue(function(){
            if($('#size_type2:checked').length > 0){
                $('#size_type1').prop('checked',true).change();
            }
            $('#sel_razmer').change().trigger("chosen:updated");
            $('select.sel_post_rab').each(function(){
                if($(this).val() != 0){
                    $(this).change().trigger("chosen:updated");
                }
            });
            $("#pricefilters input:checked").change();
            $(this).dequeue();
        });
        $(this).queue(function(){
            add_opacity_option_reset();
            add_opacity_filter_reset();
            $("#pricefilters").submit();

            firstLoad = false;
            $(this).dequeue();
        });
     }
});

$('document').ready(function(){
    function prov_id (){
        var proverka = '1';
        $.ajax({
            url: 'cside/dialog.php',
            type: 'POST',
            //dataType: 'json',
            data: {
                proverka: "proverka"
            },
            success: function(data){
                if(data == '1'){
                    $('#butt_example').css('display','inline-block');
                    $('#butt_example_bag').css('display','inline-block');
                }
            },
            error: function(){

            }
        });
    };
    prov_id();


    $('#butt_example').click(function(){
        $.post(
            'cside/dialog.php',
            {
                select: ""
            },
            function(data){
                $('#bugcont').html(data);
                $('#megamenu').css('display','none');
                $('#bugcont').css('display','block');
                $('#tabs_call > ul').append('<li class="pull-right"><button id="buttonbugclose">Закрыть</button></li>');

                $('#buttonbugclose').click(function() {
                    $('#megamenu').css('display','block');
                    $('#bugcont').css('display','none');
                });
            }
        );
        $( "#dialog" ).modal('hide');
    });

    $('#butt_example_bag').click(function(){
        $.post(
            'cside/dialog.php',
            {
                select_bag: ""
            },
            function(data){
                $('#bugcont').html(data);
                $('#megamenu').css('display','none');
                $('#bugcont').css('display','block');
                $('#tabs_call > ul').append('<li class="pull-right"><button id="buttonbugclose1">Закрыть</button></li>');
                $("#tabs_call").tab();

                $('#buttonbugclose1').click(function() {
                    $('#megamenu').css('display','block');
                    $('#bugcont').css('display','none');
                });
            }
        );
        $( "#dialog_bag" ).modal('hide');
    });

    $('#butt_callback').click(function(){
        var text_1 = $('#callback_text_1').val();
        var text_2 = $('#callback_text_2').val();
        var wind_url = window.location.href;
        var dop_text = ' Страница: '+wind_url;
        console.log(wind_url);
        if(text_1.trim() != '' || text_2.trim() != ''){
            $.ajax({
                url: 'cside/dialog.php',
                type: 'POST',
                dataType: 'json',
                data: {
                    text_1: text_1+dop_text,
                    text_2: text_2+dop_text
                },
                success: function(data){
                    if(data.err){
                        alert('чтото не так');
                    }else if(data.ok){
                        $('.callback_vse_ok').css('display','block');
                        setTimeout(function(){
                            $( "#dialog" ).modal('hide');
                            $('#callback_text_1').val('');
                            $('#callback_text_2').val('');
                        },2000);
                    }

                }
            });

        }else{
            $('#callback_text_1 , #callback_text_2').css('border-color','red');
            $('.callback_ne_zapoln').css('display','block');
            setTimeout(function(){
                $('.callback_ne_zapoln').hide(200);
                $('#callback_text_1 , #callback_text_2').css('border-color','#CCC');
            },2000);
        }
    });

    $('#butt_callback_bag').click(function(){
        var text = $('#callback_text').val();
        var wind_url = window.location.href;
        var dop_text = ' Страница: '+wind_url;
        if(text.trim() != ''){
            $.ajax({
                url: 'cside/dialog.php',
                type: 'POST',
//    			dataType: 'json',
                data: {
                    text_3: text+dop_text,
//					img_src:src_img_scrin
                },
                success: function(data){
                    if(data == 'err'){
                        alert('чтото не так');
                    }else if(data == 'ok'){
                        $('.callback_vse_ok_bag').css('display','block');
                        setTimeout(function(){
                            $( "#dialog_bag" ).modal('hide');
                            $('#bag_bag_img2').html('');
                            $('#callback_text').val('');
                            $('.img_alt_s').css('display','block');
                            $('.callback_vse_ok_bag').css('display','none');
                        },2000);
                    }
                },error:function(){
                    alert('eror');
                }
            });

        }else{
            $('#callback_text').parent().addClass('has-error');
            $('.callback_ne_zapoln_bag').css('display','block');
            setTimeout(function(){
                $('.callback_ne_zapoln_bag').hide(200);
                $('#callback_text').parent().removeClass('has-error');
            },2000);
        }
    });

    var placeholder1 = $('#callback_text_1').attr('placeholder');
    var placeholder2 = $('#callback_text_2').attr('placeholder');

    $('#callback_text_1').click(function(){
        $(this).attr('placeholder','');
        $('#callback_text_2').attr('placeholder',placeholder2);
    });

    $('#callback_text_2').click(function(){
        $(this).attr('placeholder','');
        $('#callback_text_1').attr('placeholder',placeholder1);
    });

    $('#callback_text_1').focusout(function(){
        $(this).attr('placeholder',placeholder1);
    });

    $('#callback_text_2').focusout(function(){
        $(this).attr('placeholder',placeholder2);
    });


    $('#bugcont').on('click','#prov_check',function(){
        var mass_hide_id = [];
        $('.hide_message').each(function(){
            if($(this).prop('checked')){
                mass_hide_id.push($(this).closest('tr').attr('baz_id'));
            }
        });
        $.ajax({
            url: 'cside/dialog.php',
            type: 'POST',
            data: {
                mass_hide_id: mass_hide_id
            },
            success: function(data){
                if(data == 'ok'){
                    for(q=0;q<mass_hide_id.length;q++){
                        $('#tr_call_2').after($('#end_mess_'+mass_hide_id[q]));
                        $('#end_mess_'+mass_hide_id[q]).find('input').closest('td').remove();
                    }
                }
            }
        });
    });



    $('#bugcont').on('click','#prov_check_bag',function(){
        var mass_hide_id = [];
        $('.hide_message').each(function(){
            if($(this).prop('checked')){
                mass_hide_id.push($(this).closest('tr').attr('baz_id'));
            }
        });
        $.ajax({
            url: 'cside/dialog.php',
            type: 'POST',
            data: {
                mass_hide_id_bag: mass_hide_id
            },
            success: function(data){
                if(data == 'ok'){
                    for(q=0;q<mass_hide_id.length;q++){
                        $('#tr_call_2').after($('#end_mess_'+mass_hide_id[q]));
                        $('#end_mess_'+mass_hide_id[q]).find('input').closest('td').remove();
                        $('#end_mess_'+mass_hide_id[q]).find('.displNone').removeClass('displNone');
                    }
                }
            }
        });

    });


    $('#bugcont').on('change','.sel_set_user',function(){
        var val = $(this).val();
        if(val != ''){
            var str_id = $(this).attr('str_id');
            $.ajax({
                url: 'cside/dialog.php',
                type: 'POST',
                dataType: 'json',
                data: {
                    set_user : val,
                    str_id : str_id
                },
                success: function(data){
                    $('.temp_metka_'+str_id).html(data.name);
                    if(data.color){
                        $('[baz_id = '+str_id+']').addClass('danger');
                    }else{
                        $('[baz_id = '+str_id+']').removeClass('danger');
                    }
                },
                error: function(){
                    console.log('error');
                }
            });
        }
    });


    $('.rightmegamenu').on('change','.hide_message',function(){
        $('.hide_message').each(function(){
            if($(this).prop('checked')){
                $(this).closest('tr').addClass('success');
            }else{
                $(this).closest('tr').removeClass('success');
            }
        });
    });
});
$(document).ready(function(){
    var h_footer = $('#footer').height()+50;
    $('body').css('paddingBottom',h_footer);

    $('link[rel$=icon]').remove();
    $('head').append( $('<link rel="shortcut icon" type="image/x-icon"/>' ).attr( 'href', "/mail.ico" ) );		});