$(document).ready(function(){
    var parametrLoad = 'default';

    function close_all_tir_drop(){
        $('.wrap_tir').css('background-color','#4a76a8');
        $('.new_i').removeClass('fa-caret-up');
        $('.new_i').addClass('fa-caret-down');
        $('.new_i').css('display','block');
        $('.new_i').css('color','#fff');
        $('.new_ul').css('display','none');
        $('.new_inp').css('width','35px');
    }
    $('.new_inp').focus(function(){
        close_all_tir_drop();
        $(this).val('');
        $(this).css('width','55px');
        $(this).parent().css('background-color','#fff');
//            $(this).parent().find('i').css('color','#333');
        $(this).parent().find('i').removeClass('fa-caret-down');
        $(this).parent().find('i').addClass('fa-caret-up');
        $(this).parent().find('i').css('display','none');
        $(this).parent().find('.new_ul').css('display','block');
    });
    $('.new_i').click(function(){
        if($(this).hasClass('fa-caret-down')){
            var name2 = document.getElementById($(this).attr('input_name'));
            name2.focus();
//                name2.selectionStart = name2.value.length;
        }else{
            close_all_tir_drop();
        }
    });

//        $('li').click(function(){
//            $(this).parents('.wrap_tir').find('input').val($(this).text()).change();
//            close_all_tir_drop()
//        });

    $(document).mouseup(function (e){
        var caret = $('.fa-caret-up'),
            inp = $('.new_inp'),
            ul_li = $('.new_ul li');
        if (!caret.is(e.target) && !ul_li.is(e.target) && !inp.is(e.target)){
            close_all_tir_drop();
        }
    });
    $(".tirazh").bind('blur',function(){
        if($(this).val() == ''){
            $(this).val($(this).attr('placeholder2'));
            change_text_modal_tirazh( $(this).attr('numb_tir'), $(this).val() );
            $("#pricefilters").submit();
        }else{
            change_text_modal_tirazh( $(this).attr('numb_tir'), $(this).val() );
            $(this).attr('placeholder2',$(this).val());
            $("#pricefilters").submit();
        }
    });
    $(".tirazh").keypress(function(e){
        if(e.keyCode==13){
            $(".tirazh").blur();
            close_all_tir_drop();
        }
    });
});

$('document').ready(function(){

    $('.wrap_radio2').on('click',function(){
        if($(this).children('.krug2').attr('position') == 'left'){
            $('[name=cost_var]').eq(1).prop('checked',true).change();
            $('.cost_tir').css('display','none');
            $('.cost_one').css('display','block');
            $('.t_tir').css('opacity','0.5');
            $(this).children('.krug2').attr('position','right');
            $(this).stop(true,true).children('.krug2').animate({
                left:'100%',
                marginLeft: '-20px'
            },250);
            $(this).delay(250).queue(function(){
                $('.t_one').css('opacity','1');
                $(this).dequeue();
            });
        }else{
            $('[name=cost_var]').eq(0).prop('checked',true).change();
            $('.cost_tir').css('display','block');
            $('.cost_one').css('display','none');
            $('.t_one').css('opacity','0.5');
            $('.krug2').removeClass('radio_bg2');
            $(this).children('.krug2').attr('position','left');
            $(this).stop(true,true).children('.krug2').animate({
                left:'0',
                marginLeft: '0'
            },250);
            $(this).delay(250).queue(function(){
                $('.t_tir').css('opacity','1');
                $(this).dequeue();
            });
        }
    });
});

$('document').ready(function () {
    //        Galleria.loadTheme('cside/plugin/gallery/galleria.classic.js');
    Galleria.configure({
        thumbFit: true,
        layerFollow: true,
        showInfo: false,
        showCounter: false,
        imagePosition: 'top',
        imageCrop: false,
        thumbCrop: true,
        wait: true,
        debug: false,
        idleMode: 'hover',
        idleSpeed: 300,
        lightbox: true,
        popupLinks: false
    });
    // -------------------------    МОДАЛЬНЫЕ ОКНА С ИНФО   ---------------------------------
    // Для добавления модального окна на страницу
    // нужно в елемент по которому кликаем
    // добавить атрибут data-sborka-modal-info="id_modal"
    // где "id_modal это id(без #!!!) полностью сверстанного
    // модального окна с id="id_modal" которое находится
    // в условии: if( isset( $_GET['id_modal'] ) )
    // в файле 'modal_info.php'
    $('body').on('click', '[data-sborka-modal-info]', function () {
        var modal_id = this.getAttribute('data-sborka-modal-info');
        // если нужная инфошка уже была показана
        if ($('#' + modal_id).length > 0) {
            $('#' + modal_id).modal("show");
            //            console.log('yes');
            // если нужной инфошки еще нету на странице -> загружаем ее
        } else {
            var url = "cside/modal_info.php?" + modal_id;
            $.ajax({
                type: "POST",
                url: url,
                success: function (data) {
                    // если нету нужной инфошки
                    if (data == 'no_info') {
                        // если инфошка error уже была показана
                        if ($('#info_error').length != 0) {
                            $('#info_error').modal('show');
                            // если инфошки error еще нету на странице
                        } else {
                            $.get('cside/modal_info.php?info_error', function (txt) {
                                $(this).queue(function () {
                                    $('body').append(txt);
                                    $(this).dequeue();
                                });
                                $('#info_error').modal('show');
                            });
                        }
                        // если нашли нужную инфошку ПОКАЗЫВАЕМ ее
                    } else {
                        $(this).queue(function () {
                            $('body').append(data);
                            $(this).dequeue();
                        });
                        $(this).queue(function () {
                            if ($('#' + modal_id + ' .galleria').length > 0) {
                                $('#' + modal_id + ' .galleria').galleria();
                            }
                            $(this).dequeue();
                        });
                        $(this).queue(function () {
                            jQuery(function () {
                                $('a.zoom').easyZoom({
                                    parent: '.zoom-block'
                                });
                                $('a.zoom_uv').easyZoom({
                                    parent: '.zoom-block_uv',
                                    id: 'easy_zoom_uv'
                                });
                                $('a.zoom_holst').easyZoom({
                                    parent: '.zoom-block_holst',
                                    id: 'easy_zoom_holst'
                                });
                            });
                            $(this).dequeue();
                        });
                        $(this).queue(function () {
                            $('#' + modal_id).modal('show');
                            $(this).dequeue();
                        });
                    }
                },
                error: function () {
                    //                    console.log('error ajax modal');
                }
            });
        }
    });
});


$('document').ready(function(){
    function prov_id (){
        var proverka = '1';
        $.ajax({
            url: 'cside/dialog.php',
            type: 'POST',
            //dataType: 'json',
            data: {
                proverka: "proverka"
            },
            success: function(data){
                if(data == '1'){
                    $('#butt_example').css('display','inline-block');
                    $('#butt_example_bag').css('display','inline-block');
                }
            },
            error: function(){

            }
        });
    };
    prov_id();


    $('#butt_example').click(function(){
        $.post(
            'cside/dialog.php',
            {
                select: ""
            },
            function(data){
                $('#bugcont').html(data);
                $('#megamenu').css('display','none');
                $('#bugcont').css('display','block');
                $('#tabs_call > ul').append('<li class="pull-right"><button id="buttonbugclose">Закрыть</button></li>');

                $('#buttonbugclose').click(function() {
                    $('#megamenu').css('display','block');
                    $('#bugcont').css('display','none');
                });
            }
        );
        $( "#dialog" ).modal('hide');
    });

    $('#butt_example_bag').click(function(){
        $.post(
            'cside/dialog.php',
            {
                select_bag: ""
            },
            function(data){
                $('#bugcont').html(data);
                $('#megamenu').css('display','none');
                $('#bugcont').css('display','block');
                $('#tabs_call > ul').append('<li class="pull-right"><button id="buttonbugclose1">Закрыть</button></li>');
                $("#tabs_call").tab();

                $('#buttonbugclose1').click(function() {
                    $('#megamenu').css('display','block');
                    $('#bugcont').css('display','none');
                });
            }
        );
        $( "#dialog_bag" ).modal('hide');
    });

    $('#butt_callback').click(function(){
        var text_1 = $('#callback_text_1').val();
        var text_2 = $('#callback_text_2').val();
        var wind_url = window.location.href;
        var dop_text = ' Страница: '+wind_url;
        console.log(wind_url);
        if(text_1.trim() != '' || text_2.trim() != ''){
            $.ajax({
                url: 'cside/dialog.php',
                type: 'POST',
                dataType: 'json',
                data: {
                    text_1: text_1+dop_text,
                    text_2: text_2+dop_text
                },
                success: function(data){
                    if(data.err){
                        alert('чтото не так');
                    }else if(data.ok){
                        $('.callback_vse_ok').css('display','block');
                        setTimeout(function(){
                            $( "#dialog" ).modal('hide');
                            $('#callback_text_1').val('');
                            $('#callback_text_2').val('');
                        },2000);
                    }

                }
            });

        }else{
            $('#callback_text_1 , #callback_text_2').css('border-color','red');
            $('.callback_ne_zapoln').css('display','block');
            setTimeout(function(){
                $('.callback_ne_zapoln').hide(200);
                $('#callback_text_1 , #callback_text_2').css('border-color','#CCC');
            },2000);
        }
    });

    $('#butt_callback_bag').click(function(){
        var text = $('#callback_text').val();
        var wind_url = window.location.href;
        var dop_text = ' Страница: '+wind_url;
        if(text.trim() != ''){
            $.ajax({
                url: 'cside/dialog.php',
                type: 'POST',
//    			dataType: 'json',
                data: {
                    text_3: text+dop_text,
//					img_src:src_img_scrin
                },
                success: function(data){
                    if(data == 'err'){
                        alert('чтото не так');
                    }else if(data == 'ok'){
                        $('.callback_vse_ok_bag').css('display','block');
                        setTimeout(function(){
                            $( "#dialog_bag" ).modal('hide');
                            $('#bag_bag_img2').html('');
                            $('#callback_text').val('');
                            $('.img_alt_s').css('display','block');
                            $('.callback_vse_ok_bag').css('display','none');
                        },2000);
                    }
                },error:function(){
                    alert('eror');
                }
            });

        }else{
            $('#callback_text').parent().addClass('has-error');
            $('.callback_ne_zapoln_bag').css('display','block');
            setTimeout(function(){
                $('.callback_ne_zapoln_bag').hide(200);
                $('#callback_text').parent().removeClass('has-error');
            },2000);
        }
    });

    var placeholder1 = $('#callback_text_1').attr('placeholder');
    var placeholder2 = $('#callback_text_2').attr('placeholder');

    $('#callback_text_1').click(function(){
        $(this).attr('placeholder','');
        $('#callback_text_2').attr('placeholder',placeholder2);
    });

    $('#callback_text_2').click(function(){
        $(this).attr('placeholder','');
        $('#callback_text_1').attr('placeholder',placeholder1);
    });

    $('#callback_text_1').focusout(function(){
        $(this).attr('placeholder',placeholder1);
    });

    $('#callback_text_2').focusout(function(){
        $(this).attr('placeholder',placeholder2);
    });


    $('#bugcont').on('click','#prov_check',function(){
        var mass_hide_id = [];
        $('.hide_message').each(function(){
            if($(this).prop('checked')){
                mass_hide_id.push($(this).closest('tr').attr('baz_id'));
            }
        });
        $.ajax({
            url: 'cside/dialog.php',
            type: 'POST',
            data: {
                mass_hide_id: mass_hide_id
            },
            success: function(data){
                if(data == 'ok'){
                    for(q=0;q<mass_hide_id.length;q++){
                        $('#tr_call_2').after($('#end_mess_'+mass_hide_id[q]));
                        $('#end_mess_'+mass_hide_id[q]).find('input').closest('td').remove();
                    }
                }
            }
        });
    });



    $('#bugcont').on('click','#prov_check_bag',function(){
        var mass_hide_id = [];
        $('.hide_message').each(function(){
            if($(this).prop('checked')){
                mass_hide_id.push($(this).closest('tr').attr('baz_id'));
            }
        });
        $.ajax({
            url: 'cside/dialog.php',
            type: 'POST',
            data: {
                mass_hide_id_bag: mass_hide_id
            },
            success: function(data){
                if(data == 'ok'){
                    for(q=0;q<mass_hide_id.length;q++){
                        $('#tr_call_2').after($('#end_mess_'+mass_hide_id[q]));
                        $('#end_mess_'+mass_hide_id[q]).find('input').closest('td').remove();
                        $('#end_mess_'+mass_hide_id[q]).find('.displNone').removeClass('displNone');
                    }
                }
            }
        });

    });


    $('#bugcont').on('change','.sel_set_user',function(){
        var val = $(this).val();
        if(val != ''){
            var str_id = $(this).attr('str_id');
            $.ajax({
                url: 'cside/dialog.php',
                type: 'POST',
                dataType: 'json',
                data: {
                    set_user : val,
                    str_id : str_id
                },
                success: function(data){
                    $('.temp_metka_'+str_id).html(data.name);
                    if(data.color){
                        $('[baz_id = '+str_id+']').addClass('danger');
                    }else{
                        $('[baz_id = '+str_id+']').removeClass('danger');
                    }
                },
                error: function(){
                    console.log('error');
                }
            });
        }
    });


    $('.rightmegamenu').on('change','.hide_message',function(){
        $('.hide_message').each(function(){
            if($(this).prop('checked')){
                $(this).closest('tr').addClass('success');
            }else{
                $(this).closest('tr').removeClass('success');
            }
        });
    });
});

$(document).ready(function(){
    var h_footer = $('#footer').height()+50;
    $('body').css('paddingBottom',h_footer);

    $('link[rel$=icon]').remove();
    $('head').append( $('<link rel="shortcut icon" type="image/x-icon"/>' ).attr( 'href', "/mail.ico" ) );		});
