$.fn.extend({
    animateCssShow: function (animationName) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        this.addClass('animated open ' + animationName).one(animationEnd, function() {
            $(this).removeClass('animated ' + animationName);
        });
    },
    animateCssHide: function (animationName) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
         $(this).addClass('animated ' + animationName).one(animationEnd, function() {
            $(this).removeClass('animated open ' + animationName);
        });
    },
    animateCss: function (animationName,afterFunc) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
         $(this).addClass('animated ' + animationName).one(animationEnd, function() {
            if(afterFunc != ''){
                afterFunc();
            }
            $(this).removeClass('animated ' + animationName);
        });
    }
});
function add_opacity_filter_reset(){
    var checkM = $('#selectall:checked').length + $('.select-mat:checked').length;
    var checkT = $('#alltime:checked').length + $('.terms:checked').length;
    var checkP = $('#all-print:checked').length + $('.select-print:checked').length;
    var checkL = $('#all-lam:checked').length + $('.select-lam:checked').length;
    var sum = checkM + checkT + checkP + checkL;
    if( sum == 0)
    {
        $('#filter-reset').addClass('reset_no_active');
    }else{
        $('#filter-reset').removeClass('reset_no_active');
    }
}
var active = 0;
var activeP = 0;
var xhrM = '';
var xhrP = '';
var insert = '<p class="text-center" style="font-size: 50px;padding: 20px 0;"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span></p>';
var width = 0;
var height = 0;
var firstLoad = true;
var insertNotSel = '<tr><td colspan="9" class="text-center" style="vertical-align:middle;padding:20px;font-weight:500;font-size:22px;">Для формирования прайс-листа выберите материал</td></tr>';
var flagReset = true;
// функция смены текста тиража в модальном наценки
function change_text_modal_tirazh(name,val_inp){
    $('.m_d_tir_'+name).html(val_inp);
}


$('document').ready(function(){
    $('select.sel_post_rab,#lam,#sel_razmer,.sel_metr,.sel_post_luv').chosen({width: "100%",disable_search_threshold: 10,disable_search: true});
    $('#price').popover({
        selector: '[data-toggle=tooltip]',
        placement: 'top',
        html: true,
        trigger: 'hover'
    });
    $('.types-prod').click(function () {
        location.href = $(this).attr('data-link');
    });
    function removeAnim(){
        $('.animated.zoomIn').removeClass('animated zoomIn');
    }
    setTimeout(removeAnim,1000);
    $('.t_greyblock,.header-opt').popover({
        selector: '[data-toggle=tooltip]',
        placement: 'top',
        html: true,
        trigger: 'hover'
    });
    $('body').on('submit','[name=form_discount],[name="form_discount_download"]',function(){
        return false;
    });

        $('.print-price').click(function(){
            if($(this).hasClass('icon_print')){
                printClick = 'print';
            } else if($(this).hasClass('icon_exel')) {
                printClick = 'excel';
            } else if($(this).hasClass('icon_word')) {
                printClick = 'word';
            }
        });
        $('.input_discount').on('input', function(){
            if (this.value.match(/[^0-9.]/g)) {
                this.value = this.value.replace(/[^0-9.]/g, '');
            }
        });
        $('body').on('click','#m_d_submit',function(){
            $('.d_m_d_variant').eq( $('.m_d_variant:checked').val() ).prop('checked',true);
            $('#d_m_d_all').val( $('#m_d_all').val() );
            $('#d_m_d_t_1').val( $('#m_d_t_1').val() );
            $('#d_m_d_t_2').val( $('#m_d_t_2').val() );
            $('#d_m_d_t_3').val( $('#m_d_t_3').val() );
            $('#d_m_d_t_4').val( $('#m_d_t_4').val() );
            $('#d_m_d_t_5').val( $('#m_d_t_5').val() );
            $('#d_m_d_t_6').val( $('#m_d_t_6').val() );
            $(this).closest('.modal').modal('hide');
            if(!firstLoad){             $("#pricefilters").submit();         }
        });
         $('body').on('click','#d_m_d_submit',function(){
             $(this).closest('.modal').modal('hide');
             pricePints(printClick);
         });

        $('body').on('change keyup','#m_d_all',function(){
            $('#m_d_t_1,#m_d_t_2,#m_d_t_3,#m_d_t_4,#m_d_t_5,#m_d_t_6').val($(this).val());
        });
        $('body').on('change keyup','#m_d_t_1,#m_d_t_2,#m_d_t_3,#m_d_t_4,#m_d_t_5,#m_d_t_6',function(){
            $('#m_d_all').val('');
        });
        $('body').on('change keyup','#d_m_d_all',function(){
            $('#d_m_d_t_1,#d_m_d_t_2,#d_m_d_t_3,#d_m_d_t_4,#d_m_d_t_5,#d_m_d_t_6').val($(this).val());
        });
        $('body').on('change keyup','#d_m_d_t_1,#d_m_d_t_2,#d_m_d_t_3,#d_m_d_t_4,#d_m_d_t_5,#d_m_d_t_6',function(){
            $('#d_m_d_all').val('');
        });
    $("#pricefilters").submit(function() {
        if(active != 0){
            xhrM.abort();
        }

        var dataSend = $("#pricefilters").serialize();
        xhrM = $.ajax({
            type: "POST",
            url: "cside/ajax.php?action=price_holst",
            data: dataSend,
            dataType: 'json',
            beforeSend: function(){
                active++;
                $('#insert-in').html('');
                $('#loading-table').html(insert);
                console.log(dataSend)
            },
            complete: function(data){
                if(data.responseText == 'error'){
                    location.reload();
                } else if(data.statusText == 'OK' && IsJsonString(data.responseText) == false){
                    $('#loading-table').html('');
                    var text = '<tr><td colspan="9" class="text-center" style="vertical-align:middle;padding:20px;font-weight:500;font-size:22px;">При подсчете произошла ошибка! Пожалуйста сообщите нам, об этом нажав на кнопку "Нашли ошибку".</td></tr>';
                    $('#insert-in').html(text);
                }
            },
            success: function(data){
                 $('#loading-table').html('');
                 $('#insert-in').html(com(data.text));
                active=0;
                var checkM = $('#selectall:checked').length + $('.select-mat:checked').length;
                var checkT = $('#alltime:checked').length + $('.terms:checked').length;
                var checkP = $('#all-print:checked').length + $('.select-print:checked').length;
                if($('.select-lam').length > 0){
                    var checkL = $('#all-lam:checked').length + $('.select-lam:checked').length;
                } else {
                  var checkL = 1;
                }
                if($('#print_color').val() == 1){
                    checkM = 1;
                }
                if(checkM > 0 && checkP > 0 && checkT > 0){
                    $('.time-select.hidden').removeClass('hidden').animateCss('fadeIn','');
                    $('#alltime').closest('.t_switchable').find('.filter-name').html('Cрок печати');
                }
                xhrM = '';
            }
        });
        return false;
    });

    function pricePints(printClass){
        if(activeP != 0){
            xhrP.abort();
        }
        var dateNow = new Date();
        var day = dateNow.getDate();
        var month = dateNow.getMonth() + 1;
        var year = dateNow.getFullYear();
        var dataSend = $("#pricefilters").serialize() + '&' + $('[name="form_discount_download"]').serialize();
        xhrP = $.ajax({
            type: "POST",
            url: "cside/ajax.php?action=price_holst&type=print",
            data: dataSend,
            dataType: 'json',
            beforeSend: function(){
                activeP++;
            },
            success: function(data){
                if(printClass == 'print'){
                    $(com(data.text)).print({
                        globalStyles: true,
                        mediaPrint: false,
                        stylesheet: "cside/print-css.css",
                        noPrintSelector: ".no-print",
                        iframe: true,
                        append: null,
                        prepend: null,
                        manuallyCopyFormValues: true,
                        deferred: $.Deferred(),
                        timeout: 750,
                        title: null,
                        doctype: '<!doctype html>'
                    });
                } else if(printClass == 'excel') {
                    tableToExcel(com(data.text));
                }
                activeP=0;
                xhrP = '';
            }
        });
        return false;
    };
    /*END PRINT DATA AJAX*/

    /*POSTWORK SELECT*/
    $(".sel_post_rab").change(function(){
        var selector = '[data-controlled-item=' + this.id + '-selected]';
         if($(this).val() != 0){
              if(this.id == 'podramnik'){
                  $('.parametr-luvers.hidden').removeClass('hidden');
                  $('.sel_post_luv').prop('disabled',false).trigger("chosen:updated");
                  if($('#trim').val() > 1){
                     $('#alert-post.hidden').removeClass('hidden');
                  }
                  $('#trim option[value=1]:not(:selected)').prop('selected',true).change();
                  $('#trim option[value=2]:not(:disabled)').prop('disabled',true);
                  $('#trim option[value=3]:not(:disabled)').prop('disabled',true);
                  $('.mount:not(.disabledSize)').prop('disabled',false);
                  $('.select-lam').prop('disabled',false);
                $('#trim').trigger("chosen:updated");
              } else if(this.id == 'lacquer'){
                    $('#p_1441:not(:disabled)').prop('checked',false).prop('disabled',true).change();
              }
         } else {
              if(this.id == 'podramnik'){
                  $('.parametr-luvers:not(.hidden)').addClass('hidden');
                  $('.sel_post_luv').prop('disabled',true);
                  $('.mount').prop('disabled',true);
                  $('.select-lam:not(#lam0):checked').prop('checked',false).change();
                  $('.select-lam:not(#lam0)').prop('disabled',true);
                  $('#trim option[value=2]:disabled').prop('disabled',false);
                  $('#trim option[value=3]:disabled').prop('disabled',false);
                  $('#alert-post:not(.hidden)').addClass('hidden');
                  $('#trim').trigger("chosen:updated");
              } else if(this.id == 'lacquer'){
                    $('#p_1441:disabled').prop('disabled',false);
              }
         }
         changepic();
         calculate();
         add_opacity_option_reset();
        sizeRelations();
        if(!firstLoad){
            $("#pricefilters").submit();
        }
    });
    $('.sel_post_luv').change(function(){
        calculate();
        if(!firstLoad){
            $("#pricefilters").submit();
        }
    });
    $("#option-reset:not(.reset_no_active)").click(function(){
        if($('#trim').val() != 1){
            $('#trim').val(1).change().trigger("chosen:updated");
        }
        if($('#sets').val() != 1){
            $('#sets').val(1).change();
        }
        add_opacity_option_reset();
    });
    function calculate(){
        var square = round((width*height/1000000),6);
        if($('#trim').val() == 0){
                $('#trim').closest('.pw-cont').find('.selected-show').html('').hide();
            } else if($('#trim').val() == 1) {

                if((width >= 100 && height >= 100) || (width >= 100 && height >= 100)){
                    var trimCost = 2;
                } else {
                    var trimCost = 4;
                }
                var price = (+(width/1000 + height/1000)*trimCost*2).toFixed(2);
                var text = '+ ' + price + ' грн/шт' + ' (минимум ' + trimCost  + ' грн)';
                $('#trim').closest('.pw-cont').find('.selected-show').html(text).show();
            } else if($('#trim').val() == 2) {
                var price = (+(width/1000 + height/1000)*6*2).toFixed(2);
                var text = '+ ' + price + ' грн/шт' + ' (минимум ' + 30  + ' грн)';
                $('#trim').closest('.pw-cont').find('.selected-show').html(text).show();

        } else if($('#trim').val() == 3) {
               var price = (+(width/1000 + height/1000)*6*2*1.5).toFixed(2);
                var text = '+ ' + price + ' грн/шт' + ' (минимум ' + 30  + ' грн)';
                $('[data-controlled-item=trim-selected]').html(text).show();

        }
        if($('#guard_angle').val() > 0){
            var price = 8;
            var text = '+ ' + price +' грн/изделие';
            $("#guard_angle").closest('.parametr-luvers').find('.selected-show').html(text).show();
        } else {
            $("#guard_angle").closest('.parametr-luvers').find('.selected-show').html('').hide();
        }
//        if($('#mount').val() == 1){
//            var price = 10;
//            var text = '+ ' + price +' грн за шт';
//            $('#mount').closest('.parametr-luvers').find('.selected-show').html(text).show();
//        } else {
//            $('#mount').closest('.parametr-luvers').find('.selected-show').html('').hide();
//        }
        if($("#podramnik").val() > 0){
            var priceP = {
                1: 30,
                2: 39.6,
                3: 72,
                4: 78
            }
           if ($("#podramnik").val() == 1 || $("#podramnik").val() ==2) {
				square = round(((+width   + 40) * (+height + 40) / 1000000),6);
				var pi = round( ((+width   + 40)*2 + (+height + 40)*2 ),6 );
			} else if ($("#podramnik").val() ==3) {
				square = round(((+width + 50) * (+height+50) / 1000000),6);
				var pi = round( ((+width + 50)*2 + (+height+50)*2 ),6);
			} else if ($("#podramnik").val() ==4) {
				square = round(((+width + 80) * (+height+80) / 1000000),6);
				var pi = round( ((+width + 80)*2 + (+height+80)*2 ),6 );
			}
            var podramnik = ((priceP[$("#podramnik").val()] * pi /1000) > +priceP[$("#podramnik").val()] ? (priceP[$("#podramnik").val()] * pi /1000) : +priceP[$("#podramnik").val()]);

            $('#podramnik').closest('.pw-cont').find('.selected-show').html('+ ' + round(podramnik/(priceP[$("#podramnik").val()]),2)+' x '+priceP[$("#podramnik").val()]+' = '+round(podramnik,2)+' грн').show();
        } else {
             $('#podramnik').closest('.pw-cont').find('.selected-show').html('').hide();
        }
//        if($('#lacquer').val() > 0){
//            var price = (500*square).toFixed(2);
//            var text = '+ ' + square + ' x 500 = ' + price +' грн';
//            $('#lacquer').closest('.pw-cont').find('.selected-show').html(text).show();
//        } else {
//            $('#lacquer').closest('.pw-cont').find('.selected-show').html('').hide();
//        }
    }
     /*END POSTWORK SELECT*/



    //*INPUTS CONTROL*/
    $('.input-option').on("input change",function(){
        var selector = '[data-controlled-item=' + this.id + '-selected]';
        if (this.value.match(/[^0-9]/g)) {
            this.value = this.value.replace(/[^0-9]/g, '');
        }
        if($(this).val() < 1 && $(this).val() != ''){$(this).val(1).change();}
    });
    $('.input-option').on('focusout',function(){
        if($(this).val() == ''){$(this).val(1).change();}
    });
    $('.input-option').on("change",function(){
        if(!firstLoad){             $("#pricefilters").submit();         }
        if(this.id == 'sets'){
//            if($(this).val() != 1){$('#text_info_sets').css('display','block');}else{$('#text_info_sets').css('display','none');}
            if($(this).val() > 50){$(this).val(50).change();}
        }
        add_opacity_option_reset();
     });
    $('.plus-input-val').click(function(){
        var selector = $(this).attr("data-change-input");
        $('#' + selector).val(parseInt($('#' + selector).val()) + 1).change();
    });
    $('.minus-input-val').click(function(){
         var selector = $(this).attr("data-change-input");
        $('#' + selector).val(parseInt($('#' + selector).val()) - 1).change();
    });
     /*Обработчики фильтров*/
    $('#selectall').change(function(){
        $(this).queue(function(){
            $('.select-mat:checked').prop('checked',false);
            $(this).closest('.t_switchable').find('.filter-name').html('Материал');
            add_opacity_filter_reset();
            $(this).dequeue();
        });
        $(this).queue(function(){
            setSubmit();
            $(this).dequeue();
        });
    });

    $('#alltime').change(function(){
        $(this).queue(function(){
            $('.terms:checked').prop('checked',false);
            $(this).closest('.t_switchable').find('.filter-name').html('Срок печати');
            add_opacity_filter_reset();
            $(this).dequeue();
        });
        $(this).queue(function(){
            setSubmit();
            $(this).dequeue();
        });
    });
    $('#all-print').change(function(){
        $(this).queue(function(){
            $('.select-print:checked').prop('checked',false);
            $(this).closest('.t_switchable').find('.filter-name').html('Качество печати');
            add_opacity_filter_reset();
            $(this).dequeue();
        });
        $(this).queue(function(){
            setSubmit();
            $(this).dequeue();
        });
    });

    $('.select-mat').change(function(){
        $(this).queue(function(){
            $('#selectall:checked').prop('checked',false);
            $('#selectall:not(:disabled)').closest('.t_switchable').find('.filter-name').html('Выбрать все<br>материалы');
            add_opacity_filter_reset();
            $(this).dequeue();
        });
        $(this).queue(function(){
            setSubmit();
            $(this).dequeue();
        });
    });
    $(".select-print").change(function(){
        $(this).queue(function(){
            $('#all-print:checked').prop('checked',false);
            $('#all-print:not(:disabled)').closest('.t_switchable').find('.filter-name').html('Выбрать все<br>печати');
            add_opacity_filter_reset();
            $(this).dequeue();
        });
        $(this).queue(function(){
            setSubmit();
            $(this).dequeue();
        });
    });
    $(".terms").change(function(){
        $(this).queue(function(){
            $('#alltime:checked').prop('checked',false);
            $('#alltime:not(:disabled)').closest('.t_switchable').find('.filter-name').html('Выбрать все<br>сроки печати');
            add_opacity_filter_reset();
            $(this).dequeue();
        });
        $(this).queue(function(){
            setSubmit();
            $(this).dequeue();
        });
    });
        $('#all-lam').change(function(){
        $(this).queue(function(){
            $('.select-lam:checked').prop('checked',false);
            $(this).closest('.t_switchable').find('.filter-name').html('Покрытие');
            add_opacity_filter_reset();
            $(this).dequeue();
        });
        $(this).queue(function(){
            setSubmit();
            $(this).dequeue();
        });
    });
    $(".select-lam").change(function(){
         $(this).queue(function(){
            $('#all-lam:checked').prop('checked',false);
            $('#all-lam:not(:disabled)').closest('.t_switchable').find('.filter-name').html('Выбрать все<br>покрытия');
            add_opacity_filter_reset();
            $(this).dequeue();
        });
        $(this).queue(function(){
            setSubmit();
            $(this).dequeue();
        });
    });
    function setSubmit(){
        var checkM = $('#selectall:checked').length + $('.select-mat:checked').length;
        var checkT = $('#alltime:checked').length + $('.terms:checked').length;
        var checkP = $('#all-print:checked').length + $('.select-print:checked').length;
        if($('.select-lam').length > 0){
            var checkL = $('#all-lam:checked').length + $('.select-lam:checked').length;
        } else {
          var checkL = 1;
        }
        if($('#print_color').val() == 1){
            checkM = 1;
        }
        if(checkM > 0 && checkP > 0 && checkT > 0){
            if(flagReset){
                flagReset = false;
                $('.select-all-btn').each(function () {
                   if(this.id == 'selectall'){
                      $(this).closest('.t_switchable').find('.filter-name').html('Выбрать все<br>материалы');
                   } else if (this.id == 'all-lam') {
                     $(this).closest('.t_switchable').find('.filter-name').html('Выбрать все<br>покрытия');
                   } else if (this.id == 'alltime') {
                     $(this).closest('.t_switchable').find('.filter-name').html('Выбрать все<br>сроки печати');
                   } else if (this.id == 'all-print') {
                     $(this).closest('.t_switchable').find('.filter-name').html('Выбрать все<br>печати');
                   }
                });
            }
            $('.select-all-btn').prop('disabled',false);
            $("#pricefilters").submit();
        } else {
            var insertNotSel = '<tr><td colspan="9" class="text-center" style="vertical-align:middle;padding:20px;font-weight:500;font-size:22px;">Для формирования прайс-листа выберите';
            var text = '';
            if(checkM == 0){
                text += ' материал,';
            }
            if(checkL == 0){
                text += ' покрытие,';
            }
            if(checkP == 0){
                text += ' качество печати,';
            }
            if(checkT == 0){
                text += ' срок печати,';
            }
            text = text.substring(0, text.length - 1)
            insertNotSel += text + '</td></tr>';
            $('#insert-in').html(insertNotSel);
        }
    }
    $('#filter-reset').click(function(){
        $(this).queue(function(){
            $('#alltime:checked,#all-lam:checked,#all-print:checked,.select-mat:checked,#selectall:checked,.terms:checked,.select-print:checked,.select-lam:checked').prop('checked',false).change();
            $('#selectall').closest('.t_switchable').find('.filter-name').html('Материал');
            $('#all-дфь').closest('.t_switchable').find('.filter-name').html('Покрытие');
            $('#alltime').closest('.t_switchable').find('.filter-name').html('Срок печати');
            $('#all-print').closest('.t_switchable').find('.filter-name').html('КАЧЕСТВО ПЕЧАТИ');
            $(this).dequeue();
        });
        $(this).queue(function(){
            $('.select-all-btn').prop('disabled',true);
            add_opacity_filter_reset();
            flagReset = true;
            $(this).dequeue();
        });
    });

    /*END FILTERS*/
    $('#sel_razmer').change(function(e) {
        width = +$(this).find('option:selected').data('width');
        height = +$(this).find('option:selected').data('height');
        $('.input_size').val('');
        $('[name="size_in"]').val(1).change().trigger("chosen:updated");
        $('#size_input').data("width",width).data("height",height);
        $('#size_input').attr('value',$(this).val()).change();
    });
    $('.input_size').on('input',function(){
        if (this.value.match(/[^0-9.]/g)) {
            this.value = this.value.replace(/[^0-9.]/g, '');
        }
    });
    $('.input_size').on('change',function(){
        $('#sel_razmer').val(0).trigger("chosen:updated");
        if(($('#input-width1').val()*$('[name="size_in"]').val()) < 150){
            $('#input-width1').val(150/$('[name="size_in"]').val());
        }
        if(($('#input-width1').val()*$('[name="size_in"]').val()) > 50000){
             $('#input-width1').val(50000/$('[name="size_in"]').val());
        }
        if( ($('#input-height1').val()*$('[name="size_in"]').val()) < 150){
            $('#input-height1').val(150/$('[name="size_in"]').val());
        }
        if(($('#input-height1').val()*$('[name="size_in"]').val()) > 50000){
             $('#input-height1').val(50000/$('[name="size_in"]').val());
        }
        
        width = +$('#input-width1').val()*$('[name="size_in"]').val();
        height = +$('#input-height1').val()*$('[name="size_in"]').val();
        $('#input-width').val(width);
        $('#input-height').val(height);
        
        $('#size_input').data("width",width).data("height",height);
        $('#size_input').attr('value',30).change();
        if(!firstLoad){             $("#pricefilters").submit();         }
    });
    $('[name="size_in"]').change(function(){
         var wid = $('#input-width1').val(),
            hei = $('#input-height1').val();
        if( wid != '' && hei != '' && +$('#size_input').val() == 30){
            if( $(this).val() == 10 ){
                $('#input-width1').val(wid/10).trigger('change');
                $('#input-height1').val(hei/10).trigger('change');
            }else{
                $('#input-width1').val(wid*10).trigger('change');
                $('#input-height1').val(hei*10).trigger('change');
            }
        }
    });
    $('#size_input').change(function(){
        if(parametrLoad != 'product' || (parametrLoad == 'product' && $('#rectangle').length > 0)){
            if($(this).val() == 30){
               smena_razmera(width,height,'Ваш размер');
            } else {
               smena_razmera(width,height,'');
            }
        }
        calculate();
        sizeRelations();
        if(!firstLoad){             $("#pricefilters").submit();         }
    });
    function sizeRelations(){
        if((width <= 1400 && height <= 2000) || (width <= 2000 && height <= 1400)){
            $('#podramnik').prop('disabled',false).trigger("chosen:updated");
        } else {
            if($('#podramnik').val() > 0){
                $('#podramnik').val(0).change();
            }
            $('#podramnik').prop('disabled',true).trigger("chosen:updated");
        }
        if((width > 1150 && height > 1150) && +$('#podramnik').val() > 0){
            $('#m_1883:checked').prop('checked',false).change();
            $('#m_1883').prop('disabled',true);
        } else {
            $('#m_1883').prop('disabled',false);
        }
        if((height < 1250 && height < 2500) || (height < 2500 && height < 1250)){
            $('#trim option[value = 2]:disabled, #trim option[value = 3]:disabled').prop('disabled',false);
            $('#trim').trigger("chosen:updated");
        } else {
            if($('#trim').val() == 2 || $('#trim').val() == 3){
                $('#trim').val(0).change();
            }
            $('#trim option[value = 2]:not(:disabled),#trim option[value = 3]:not(:disabled)').prop('disabled',true);
            $('#trim').trigger("chosen:updated");
        }
        if((height > 850 || height > 850)){
            if($('#mount_3').prop('checked') && !$('#mount_1').prop('disabled')){
                $('#mount_1').prop('checked',true);
            }
            $('#mount_3').prop('disabled',true).addClass('disabledSize');
        } else {
             $('#mount_3').removeClass('disabledSize');
             if($('#podramnik').val() > 0){
                 $('#mount_3').prop('disabled',false);
             }
        }
    }
    if($(window).width() < 991){
         $('.header-opt').click(function(){
            if($(this).hasClass('openChild') == false){
                $(this).addClass('openChild');
                $(this).closest('.mobile-control').find('.mobile-droped').stop(true,true).animateCssShow('slideMenuDown');
                $(this).closest('.mobile-control').find('.icon-drop .fa-plus').fadeOut(250);
                $(this).closest('.mobile-control').find('.icon-drop .fa-minus').fadeIn(250);
            } else {
                $(this).removeClass('openChild');
                $(this).closest('.mobile-control').find('.mobile-droped').stop(true,true).animateCssHide('slideMenuTop');
                $(this).closest('.mobile-control').find('.icon-drop .fa-minus').fadeOut(250);
                $(this).closest('.mobile-control').find('.icon-drop .fa-plus').fadeIn(250);
            }

        });
    } else {
        padT();
    }
    $('.orient,.mount').change(function(){
        if(!firstLoad){             $("#pricefilters").submit();         }
    });

    /*FUNCTIONS*/

    function round(value, decimals) {
      return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
    }
    function tableToExcel( element ) {
       var html, link, blob, url, css;
       var dateNow = new Date();
       var day = dateNow.getDate();
       var month = dateNow.getMonth() + 1;
       var year = dateNow.getFullYear();
       var template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>' + $(element).html() + '</table></body></html>' ;
       blob = new Blob(['\ufeff',css + template], {
         type: 'application/vnd.ms-excel'
       });
       url = URL.createObjectURL(blob);
       link = document.createElement('A');
       link.href = url;
       link.download = 'sborkaPrice_' + day + '-' + (month < 10 ? 0 : '') + month + '-' + year + '.xls';  // default name without extension
       document.body.appendChild(link);
       if (navigator.msSaveOrOpenBlob ) navigator.msSaveOrOpenBlob( blob, 'sborkaPrice_' + day + '-' + (month < 10 ? 0 : '') + month + '-' + year + '.xls'); // IE10-11
           else link.click();  // other browsers
       document.body.removeChild(link);
     };

    function com(compressed) {
        "use strict";
        var i,
            dictionary = [],
            w,
            result,
            k,
            entry = "",
            dictSize = 256;
        for (i = 0; i < 256; i += 1) {
            dictionary[i] = String.fromCharCode(i);
        }
        w = String.fromCharCode(compressed[0]);
        result = w;
        for (i = 1; i < compressed.length; i += 1) {
            k = compressed[i];
            if (dictionary[k]) {
                entry = dictionary[k];
            } else {
                if (k === dictSize) {
                    entry = w + w.charAt(0);
                } else {
                    return null;
                }
            }
            result += entry;
            dictionary[dictSize++] = w + entry.charAt(0);
            w = entry;
        }
        function decode_utf8(s) {
          return decodeURIComponent(escape(s));
        }
        return decode_utf8(result);
    }


    function filterChange(target,elemName,nameParr,tmpName,changeName){
        var idM = $(target).val();
        var n = $('.' + elemName + ':checked').length;

		if (n == 0) {
            $('#' + nameParr + '').prop("checked",true).change();
		}else{
            var i = 0;
            var s = 0;
            $('#' + nameParr + '').prop("checked",false);
            var tmpCheck = [];
            var tmpCheckHide = [];
            var addClass = (elemName == 'select-lam' ? 'hidden-by-lam' : (elemName == 'select-print' ? 'hidden-by-print' : 'hidden'));
            $('.' + elemName).each(function(){
                if($(this).prop("checked") == true){

                    tmpCheck[i] = tmpName + ((elemName == 'select-mat' ||  elemName == 'select-lam' ||  elemName == 'select-print') ? $(this).val() : $(this).attr('name'));
                    i++;
                } else {
                  tmpCheckHide[s] = tmpName + ((elemName == 'select-mat' ||  elemName == 'select-lam' ||  elemName == 'select-print') ? $(this).val() : $(this).attr('name'));
                    s++;
                }
            });

            for(var m = 0; m<tmpCheckHide.length; m++){
               if($(tmpCheckHide[m]).hasClass(addClass) == false){
                  $(tmpCheckHide[m]).addClass(addClass);
               }
            }

            for(var m = 0; m<tmpCheck.length; m++){
               $(tmpCheck[m]).removeClass(addClass);
            }

            if(elemName == 'select-print'){
                $('.table-for-print').each(function(){
                    var elem = this;
                    var noH = $(elem).find('.print-row:not(.hidden-by-print)').length;
                    $(elem).find('.print-row.last-print').removeClass('last-print');
                    $(elem).find($('.print-row:not(.hidden-by-print)')).eq((noH-1)).addClass('last-print');
                });

            }

            $('#' + nameParr).closest('.t_switchable').find('.filter-name').html(changeName);
        }
    }

    function selectAll(target,name,elemName,childrenName,changeName){
        var addClass = (elemName == 'select-lam' ? 'hidden-by-lam' : (elemName == 'select-print' ? 'hidden-by-print' : 'hidden'));
        if($('#' + name).prop("checked") == true){
            var n = $('.' + elemName + ':checked').length;
            if (n != 0) {
                $('.' + elemName + ':checked').prop("checked",false);
                $('.' + childrenName).removeClass(addClass);
            } else {
               $('.' + childrenName).removeClass(addClass);
            }
            $('#' + name).closest('.t_switchable').find('.filter-name').html(changeName);
        } else {
           var n = $('.' + elemName + ':checked').length;
            if (n == 0) {
                $('#' + name).prop('checked',true);
                return false;
            }
        }

        if(name == 'all-print'){
            $('.print-row.last-print').removeClass('last-print');
        }
    }
    function padT(){
        $(this).stop(true,true).delay(300).queue(function(){
            var removeOpacity = function(){
                $('.gallery').css('opacity',1);
            };
            $('.gallery').animateCss('fadeIn',removeOpacity);
            $(this).dequeue();

        });
    }

    /*END FUNCTION*/
// при первой загрузке для смены тиражей в модальном наценки
    $(".tirazh").each(function(){
        change_text_modal_tirazh( $(this).attr('numb_tir'), $(this).val() );
    });

    $(".tirazh").bind('change keyup',function(){
        if($(this).val() != ''){
            change_text_modal_tirazh( $(this).attr('numb_tir'), $(this).val() );
            if(!firstLoad){             $("#pricefilters").submit();         }
        }
	});
    function IsJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }
    function add_opacity_option_reset(){
        var allNull = true;
        if($('#trim').val() != 1){
            allNull = false;
        }
        if($('#sets').val() != 1){
            allNull = false;
        }
        if(allNull){
            $('#option-reset:not(.reset_no_active)').addClass('reset_no_active');
        } else {
            $('#option-reset.reset_no_active').removeClass('reset_no_active');
        }
    }
    if(parametrLoad == 'return' || parametrLoad == 'product'){
        $.getScript('cside/price_js/repeat_order.js').done(function( script, textStatus ) {
            loadPage();
        });
    } else {
        $(this).queue(function(){
            if($('#size_type2:checked').length > 0){
                $('#size_type1').prop('checked',true).change();
            }
            $('#sel_razmer').change().trigger("chosen:updated");
            $('select.sel_post_rab').each(function(){
                if($(this).val() != 0){
                    $(this).change().trigger("chosen:updated");
                }
            });
            $("#pricefilters input:checked").change();
            $(this).dequeue();
        });
        $(this).queue(function(){
            add_opacity_option_reset();
            add_opacity_filter_reset();
            $("#pricefilters").submit();
            firstLoad = false;
            $(this).dequeue();
        });
     }
});
