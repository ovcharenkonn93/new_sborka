var prod_width,
	prod_height,
	prod_name,
	tmp_metka_r = 0;
$('document').ready(function () {
	$('.sel_post_rab').each(function () {
		$(this).change(function () {
            if(parametrLoad != 'product'){
			     change_razmer(typePrewiev);
            }
		});
	});
	$('#perevorot_maket').click(function () {
		var temp_prod_width = '';
		temp_prod_width = prod_width;
		r_w = prod_height;
		r_h = temp_prod_width;
		smena_razmera({
			'width': r_w,
			'height': r_h,
			'name': prod_name,
			'typePrewiev': typePrewiev
		});
	});
	$('#horizont-maket').click(function () {
		if (!$(this).hasClass('active_maket')) {
			var temp_prod_width = '';
			$('#orinet_hor').prop('checked', true).change();
			temp_prod_width = prod_width;
			r_w = prod_height;
			r_h = temp_prod_width;
			smena_razmera({
				'width': r_w,
				'height': r_h,
				'name': prod_name,
				'typePrewiev': typePrewiev
			});
		}
	});
	$('#vert-maket').click(function () {
		if (!$(this).hasClass('active_maket')) {
			var temp_prod_width = '';
			$('#orinet_vert').prop('checked', true).change();
			temp_prod_width = prod_width;
			r_w = prod_height;
			r_h = temp_prod_width;
			smena_razmera({
				'width': r_w,
				'height': r_h,
				'name': prod_name,
				'typePrewiev': typePrewiev
			});
		}
	});
	$('#rectangle').attr('visib', 'width');
});

function smena_razmera(new_param) {
	var option_default = {
		'width': 90,
		'height': 50,
		'name': '',
		'typePrewiev': 'standart',
		'ind_razmer': false
	}
	var option = $.extend(option_default, new_param);
	// метка для отображения сгиба не по центру для продуктов
	var metka_falc_1_3 = 0;
	if (option.typePrewiev == 'henger' || option.typePrewiev == 'folder' || option.typePrewiev == 'domik'|| option.typePrewiev == 'folder') {
		option.width = 100;
		option.height = 100;
	} else if (option.typePrewiev == 'konvert') {
		//        option.width = 90;
		//        option.height = 100;
	}
	if ($('[name=size_in]').val() == 10 && $('#size_type2').prop('checked')) {
		var r_mm = 'см';
		var sin_val = 10;
	} else {
		var r_mm = 'мм';
		var sin_val = 1;
	}
	prod_width = parseInt(option.width);
	prod_height = parseInt(option.height);
	prod_name = option.name;
	var str_razmer = (option.name != '' && option.typePrewiev == 'folding' ? option.name + '<br>' : '') + option.width / sin_val + ' x ' + option.height / sin_val + ' ' + r_mm;
	if (option.typePrewiev == 'folding') {
		var str_razmer = (option.name != '' ? option.name : option.width / sin_val + ' x ' + option.height / sin_val + ' ' + r_mm);
		if (prod_width == 135 || prod_width == 125) {
			metka_falc_1_3 = 1;
		}
	} else {
		if (option.typePrewiev == 'henger' || option.typePrewiev == 'folder' || option.typePrewiev == 'domik' || option.typePrewiev == 'folder_d' || option.typePrewiev == 'folder_fig') {
			var str_razmer = '';
		} else {
			var str_razmer = (option.ind_razmer ? option.name + '<br>' : '') + option.width / sin_val + ' x ' + option.height / sin_val + ' ' + r_mm;
		}
	}
	$('#rectangle .razmer_text').html(str_razmer);
	change_razmer(option.typePrewiev, metka_falc_1_3);
}

function change_razmer(tmp_typePrewiev, metka_falc_1_3) {
		tmp_typePrewiev = tmp_typePrewiev || 'standart';
		metka_falc_1_3 = metka_falc_1_3 || 0;
		// меняем кнопку переворота макета в зависимости от размера
		if (prod_width > prod_height) {
			$('#orinet_hor').prop('checked', true).change();
			$('#perevorot_maket img').attr('src', 'cside/img/arrow.svg');
			$('#vert-maket').removeClass('active_maket');
			$('#horizont-maket').addClass('active_maket');
			//        $('.orient_text').html('Горизонтальное');
			$('.orient_text_hor').css('color', '#c00');
			$('.orient_text_vert').css('color', '#333');
		} else {
			$('#orinet_vert').prop('checked', true).change();
			$('#perevorot_maket img').attr('src', 'cside/img/arrow_v.svg');
			$('#horizont-maket').removeClass('active_maket');
			$('#vert-maket').addClass('active_maket');
			//        $('.orient_text').html('Вертикальное');
			$('.orient_text_hor').css('color', '#333');
			$('.orient_text_vert').css('color', '#c00');
		}
		var blockpicwidth = $('#blockpic').width(),
			/* ширина контейнера */
			blockpicheight = $('#blockpic').height(),
			/* высота контейнера */
			proporcion_new = prod_width / prod_height;
		//            console.log(proporcion_new);
		var prop_width = blockpicwidth / prod_width;
		var prop_height = blockpicheight / prod_height;
		if (prop_width > prop_height) {
			$('#rectangle').stop(true, true).css('display', 'block');
			var new_prod_width = prod_width / prod_height;
			var new_prod_height = prod_height / prod_width;
			new_prod_width = parseInt(new_prod_width * blockpicheight);
			var proporcion = prod_width / prod_height;
			//                        console.log('1 '+proporcion);
			if (proporcion <= 0.471) {
				if ($(window).width > 991) {
					$('#rectangle div').css({
						'margin-left': new_prod_width + 10 + 'px',
						'width': '150px'
					});
				} else {
					var maxleft = $('#blockpic').width() - new_prod_width - 120 + 25;
					if ((new_prod_width + 10) < maxleft) {
						maxleft = (new_prod_width + 10);
					}
					$('#rectangle div').css({
						'margin-left': maxleft + 'px',
						'width': '120px'
					});
				}
			} else {
				$('#rectangle div').css({
					'margin-left': '',
					'width': new_prod_width
				});
			}
			$('#rectangle div').css('display', 'none');
			$('#rectangle img').css('display', 'none');
			if (tmp_typePrewiev == 'circle') {
				setTimeout(function () {
					$('#rectangle').css('borderRadius', new_prod_width + 'px');
				}, 100);
			} else if (tmp_typePrewiev == 'felling') {
				setTimeout(function () {
					$('#rectangle').css('borderRadius', '10px');
				}, 100);
			} else if (tmp_typePrewiev == 'heart') {
				setTimeout(function () {
					$('#rectangle').css({
						'background': 'url(cside2/img/ico_size/heart-33.svg) no-repeat',
						'border': '0px'
					});
				}, 100);
			} else if (tmp_typePrewiev == 'list') {
				setTimeout(function () {
					$('#rectangle').css({
						'background': 'url(cside2/img/ico_size/list_pr.png)  no-repeat',
						'border': '0px',
						'background-position': 'center',
						'background-size': 'cover'
					});
					if ($('[name="orient"]:checked').val() == 1) {
						$('#rectangle').css({
							'transform': 'rotateZ(90deg)',
							'background-size': 'contain'
						});
					} else {
						$('#rectangle').css('transform', 'rotateZ(0deg)');
					}
					$('.razmer_text').remove();
				}, 100);
			} else if (tmp_typePrewiev == 'oval') {
				setTimeout(function () {
					$('#rectangle').css('borderRadius', '75%');
				}, 100);
			} else if (tmp_typePrewiev == 'star') {
				setTimeout(function () {
					$('#rectangle').css({
						'background': 'url(cside/img/star.png) 100% 100% / 100% 100% no-repeat',
						'border': '0px'
					});
				}, 100);
			} else if (tmp_typePrewiev == 'henger') {
				setTimeout(function () {
					if ($('#sel_razmer').val() == 128) {
						$('#rectangle').css({
							'background': 'url(cside/img/Standart/henger1.png) 100% 100% / 100% 100% no-repeat',
							'border': '0px'
						});
					} else {
						$('#rectangle').css({
							'background': 'url(cside/img/Standart/henger2.png) 100% 100% / 100% 100% no-repeat',
							'border': '0px'
						});
					}
				}, 100);
			} else if (tmp_typePrewiev == 'domik') {
				setTimeout(function () {
					if ($('#sel_razmer').val() == 160) {
						$('#rectangle').css({
							'background': 'url(cside/img/Standart/domik.png) 100% 100% / 100% 100% no-repeat',
							'border': '0px'
						});
					} else {
						$('#rectangle').css({
							'background': 'url(cside/img/Standart/piramid.png) 100% 100% / 100% 100% no-repeat',
							'border': '0px'
						});
					}
				}, 100);
			} else if (tmp_typePrewiev == 'folder') {
				setTimeout(function () {
					$('#rectangle').css({
						'background': 'url(cside/img/Standart/papka2.png)  no-repeat',
						'border': '0px',
						'background-position': 'center',
						'background-size': 'contain'
					});
					$('.razmer_text').remove();
				}, 100);
			} else if (tmp_typePrewiev == 'folder_fig') {
				setTimeout(function () {
					$('#rectangle').css({
						'background': 'url(cside/img/ico_size/standart/spf' + prod_height + '_' + prod_width +'.png)  no-repeat',
						'border': '0px',
						'background-position': 'center',
						'background-size': 'contain'
					});
					$('.razmer_text').remove();
				}, 100);
			} else if (tmp_typePrewiev == 'folder_d') {
				setTimeout(function () {
					$('#rectangle').css({
						'background': 'url(cside/img/ico_size/standart/sp' + prod_height + '_' + prod_width +'.png)  no-repeat',
						'border': '0px',
						'background-position': 'center',
						'background-size': 'contain'
					});
					$('.razmer_text').remove();
				}, 100);
			} else if (tmp_typePrewiev == 'konvert') {
				setTimeout(function () {
					$('#rectangle').css({
						'background': 'url(cside2/img/ico_size/konvert_prev/k' + ($('#sel_razmer').val() == 131 ? 130 : $('#sel_razmer').val()) + '.png)  no-repeat',
						'border': '0px',
						'background-position': 'center'
					});
					$('.razmer_text').remove();
				}, 100);
			} else if (tmp_typePrewiev == 'rulon') {
				setTimeout(function () {
					$('#rectangle').css({
						'background': 'url(cside2/img/ico_size/rulon_prev.png)  no-repeat',
						'border': '0px',
						'background-position': 'center',
						'background-size': 'contain'
					});
					if ($('[name="orient"]:checked').val() == 1) {
						$('#rectangle').css('transform', 'rotateZ(90deg)');
					} else {
						$('#rectangle').css('transform', 'rotateZ(0deg)');
					}
					$('.razmer_text').remove();
				}, 100);
			} else {
				$('#rectangle').css('borderRadius', '0');
			}
			var th = proporcion * new_prod_width;
			$('#rectangle').animate({
				height: '100%'
			}, 200);
			$.when($('#rectangle').animate({
				width: new_prod_width + 'px'
			}, 200)).then(function () {
				if (proporcion >= 0.471) {
					if (proporcion_new >= 0.95) {
						changepic2({
							'typePrewiev': tmp_typePrewiev,
							'metka_falc_1_3': metka_falc_1_3
						});
					} else {
						changepic2({
							'rem': 0,
							'drilling_left': 1,
							'typePrewiev': tmp_typePrewiev,
							'metka_falc_1_3': metka_falc_1_3
						});
					}
					$('#rectangle div').css('display', 'block');
				} else {
					changepic2({
						'rem': 1,
						'typePrewiev': tmp_typePrewiev,
						'metka_falc_1_3': metka_falc_1_3
					});
				}
				//                                }
				//								changepic();
			});
			var innerH = (+$('#rectangle').innerHeight() / 2) - 9.5;
			$('#rectangle div').css('margin-top', innerH + 'px');
			//						console.log(proporcion+' 1');
		} else {
			var new_prod_height = prod_height / prod_width;
			new_prod_height = parseInt(new_prod_height * blockpicwidth);
			var proporcion = prod_height / prod_width;
			//                        console.log('2 '+proporcion);
			$('#rectangle').stop(true, true).css('display', 'block');
			$('#rectangle div').css('margin-top', (new_prod_height / 2 - 9.5) + 'px').css('marginLeft', '').css('width', '100%');
			if (proporcion <= 0.2) {
				$('#rectangle div').css('margin-top', (new_prod_height + 10) + 'px').css('marginLeft', '').css('width', '100%');
			}
			$('#rectangle div').css('display', 'none');
			$('#rectangle img').css('display', 'none');
			if (tmp_typePrewiev != 'circle' && tmp_typePrewiev != 'oval' && tmp_typePrewiev != 'felling') {
				$('#rectangle').css('borderRadius', '0');
			}
			if (tmp_typePrewiev == 'konvert') {
				setTimeout(function () {
					$('#rectangle').css({
						'background': 'url(cside2/img/ico_size/konvert_prev/k' + $('#sel_razmer').val() + '.png)  no-repeat',
						'border': '0px',
						'background-position': 'center'
					});
					$('.razmer_text').remove();
				}, 100);
				console.log('sdf');
			}
			$('#rectangle').animate({
				width: '100%'
			}, 200);
			$.when($('#rectangle').animate({
				height: new_prod_height + 'px'
			}, 200)).then(function () {
				//                            $('#rectangle div').css('display','block');
				//                            console.log(tmp_metka_r);
				//                            if(tmp_metka_r == 1){
				if (proporcion >= 0.2) {
					if (proporcion_new >= 0.95) {
						changepic2({
							'typePrewiev': tmp_typePrewiev,
							'metka_falc_1_3': metka_falc_1_3
						});
					} else {
						changepic2({
							'rem': 0,
							'drilling_left': 1,
							'typePrewiev': tmp_typePrewiev,
							'metka_falc_1_3': metka_falc_1_3
						});
					}
					$('#rectangle div').css('display', 'block');
				} else {
					changepic2({
						'rem': 1,
						'typePrewiev': tmp_typePrewiev,
						'metka_falc_1_3': metka_falc_1_3
					});
					//                                console.log(proporcion+' 2');
				}
				//							changepic();
			});
			//						console.log(proporcion+' 2');
		}
	}
	//========================================================================================================================================
	//========================================================================================================================================
	//========================================================================================================================================
	//========================================================================================================================================
function changepic2(new_param) {
		var option_default = {
			'rem': 0,
			'drilling_left': 0,
			'typePrewiev': 'standart',
			'metka_falc_1_3': 0,
		}
		var option = $.extend(option_default, new_param);
		var standartFlag = (typePrewiev != undefined ? (typePrewiev == 'oval' || typePrewiev == 'heart' || typePrewiev == 'star' || typePrewiev == 'circle' || typePrewiev == 'list' || option.typePrewiev == 'folder_d' || option.typePrewiev == 'folder_fig' ? true : false) : false);
		$('#post_left_text').remove();
		var div_text_block = $('#rectangle .razmer_text').clone();
		$('#rectangle div').remove();
		$('#post_left_text').remove();
		$('#rectangle img').remove();
		$('#rectangle').append(div_text_block);
		delete div_text_block;
		if (option_default.rem == 1) {
			//        var st = 'position:absolute;top:0;left:0;width:156px;';
			var st = 'width:160px;';
			//        var st = '';
			var div = '<div id="post_left_text" style="' + st + '">';
			if ($('[name=dop_drilling]').length) {
				if ($('[name=dop_drilling]').val() != '0') {
					//                div += '<p>Сверление : '+$('[name=dop_drilling]').val()+'</p>';
				}
			}
			if ($('[name=dop_falc]').length) {
				if ($('[name=dop_falc]').val() != '0') {
					div += '<p>Сгиб : ' + $('[name=dop_falc]').val().substring(0, 1) + '</p>';
				}
			}
			if ($('[name=dop_creasing]').length) {
				if ($('[name=dop_creasing]').val() != '0') {
					div += '<p>Биговка : ' + $('[name=dop_creasing]').val() + '</p>';
				}
			}
			if ($('[name=dop_perforation]').length) {
				if ($('[name=dop_perforation]').val() != '0') {
					div += '<p>Перфорация : ' + $('[name=dop_perforation]').val() + '</p>';
				}
			}
			if ($('[name=dop_plot]').length) {
				if ($('[name=dop_plot]').val() != '0') {
					div += '<p>Плоттерная порезка : есть</p>';
				}
			}
			if ($('[name=dop_contour]').length && !standartFlag) {
				if ($('[name=dop_contour]').val() != '0') {
					div += '<p>Планшетная порезка : есть</p>';
				}
			}
			if ($('[name=dop_person]').length) {
				if ($('[name=dop_person]').val() != '0') {
					div += '<p>Персонализация : есть</p>';
				}
			}
			div += '</div>';
			$('#blockpic').prepend(div);
			if ($('#rectangle').width() > $('#rectangle').height()) {
				$('#rectangle .razmer_text').css({
					'marginTop': '0',
					'textAlign': 'center'
				});
			} else {
				$('#rectangle .razmer_text').css({
					'marginTop': '0',
					'textAlign': 'left'
				});
			}
			$('#rectangle .razmer_text').append(div);
			$('#rectangle .razmer_text').css('display', 'block');
			return false;
		}
		$('#rectangle .razmer_text').css({
			'textAlign': 'center'
		});
		if ($('[name=dop_creasing]').val() > 0) {
			var val_b = $('[name=dop_creasing]').val();
		}
		if ($('[name=dop_perforation]').val() > 0) {
			var val_p = $('[name=dop_perforation]').val();
		}
		if ($('[name=dop_plot]').val() > 0) {
			var val_plot = $('[name=dop_plot]').val();
		}
		if ($('[name=dop_contour]').val() > 0 && !standartFlag) {
			var val_contour = $('[name=dop_contour]').val();
		}
		if ($('[name=dop_person]').val() > 0) {
			var val_person = $('[name=dop_person]').val();
		}
        if ($('[name=dop_scratch]').length > 0) {
			var val_scratch = $('[name=dop_scratch]:checked').val();
		}
		if ($('[name=dop_falc]').val() > 0) {
			var val_bf = $('[name=dop_falc]').val();
			if (val_bf == 23) {
				var falc_window = 1;
			} else {
				var falc_window = 0;
			}
			if (val_bf == 32 || val_bf == 22) {
				var falc_bayan = 1;
			} else {
				var falc_bayan = 0;
			}
			val_bf = val_bf.substring(0, 1);
		}
		var w = $('#rectangle').outerWidth(),
			h = $('#rectangle').outerHeight(),
			// Отступ блока слева с количеством пост работ вертикальная линия 
			div_left = 5;
		/* Добавление плотерной порезки */
		if (val_plot && !standartFlag) {
			var plotporez = '<div class="bl_plotporez"></div>';
			$('#rectangle').append(plotporez);
		}
		/* Добавление планшетной порезки */
		if (val_contour && !standartFlag) {
			var contour = '<div id="zvezda" style="position:absolute;left:0;top:0;width:100%;height:100%;margin-top:0;z-index:9999">';
            if(val_contour == 1){
                contour += '<div style="width: 100%;height: 100%;border-radius: 300px;border: 1px solid #c1c1c1;"></div></div>';
            }else{
                contour += '<img style="width:100%;height:100%" src="cside/img/star.png"></div>';
            }
			$('#rectangle').append(contour);
		}
		/* Добавление персонализации */
		if (val_scratch) {
			var widthScr = (val_scratch/prod_width)*$('#rectangle').width();
            var heightScr = ((val_scratch == 36 ? 6 : 8)/prod_height)*$('#rectangle').height();
			var scratch = '<div style="position:absolute;right:10px;top:10px; width:'+widthScr+'px;background-color: rgb(136, 136, 136);height: '+heightScr+'px;"></div>';
			$('#rectangle').append(scratch);
		}
        if (val_person) {
			//                    var person = '<div style="position:absolute;right:5px;bottom:5px;"></div>';
			var person = '<div style="position:absolute;right:5px;bottom:5px;"><i class="fa fa-3x fa-barcode" aria-hidden="true"></i><p style="font-size:8px;margin-top:-5px;">1234567890</p></div>';
			$('#rectangle').append(person);
		}
		/* Добавление сгиба */
		if (val_bf&& !standartFlag) {
			//                    var tmp_displ_bf = ' display:none';
			var tmp_displ_bf = '';
			var val_bigfalc = val_bf;
			var b_w = $('#rectangle').width();
			var bigfalc_h = h + 8;
			var img_in = 'http://sborka.ua/tm/menu/prodimg/falc11.png';
			var img_out = 'http://sborka.ua/cside/img/falc_g2.png';
			if (!val_b && !val_p) {
				if (falc_window) {
					bigfalc_w = b_w / 4 - 15;
					bigfalc_w_2_3 = b_w / 4 * 3 - 15;
					var img_bigfalc = '<img src="' + img_in + '" class="img_bigfalc" style="left:' + bigfalc_w + 'px;height:' + bigfalc_h + 'px">';
					var img_bigfalc2 = '<img src="' + img_in + '" class="img_bigfalc" style="left:' + bigfalc_w_2_3 + 'px;height:' + bigfalc_h + 'px">';
					$('#rectangle').find('.img_bigfalc').remove();
					$('#rectangle').append(img_bigfalc).append(img_bigfalc2);
				} else if (val_bf == 1) {
					if (option_default.metka_falc_1_3 != 0) {
						bigfalc_w_3_c = b_w / 3 - 15;
						var img_bigfalc = '<img src="' + img_in + '" class="img_bigfalc" style="left:' + bigfalc_w_3_c + 'px;height:' + bigfalc_h + 'px">';
						$('#rectangle').find('.img_bigfalc').remove();
						$('#rectangle').append(img_bigfalc);
					} else {
						if (+$('#falc').val() == 121) {
							// if($('#falc_short').val() > 0){
							console.log(prod_width)
							var assimKoef = b_w * (1 - (prod_width - $('#falc_short').val()) / prod_width);
							bigfalc_w_3_c = assimKoef - 15;
							// } else {
							//     var assimKoef = 1;
							//     bigfalc_w_3_c = b_w/4*2-15;
							// }
							var img_bigfalc = '<img src="' + img_in + '" class="img_bigfalc" style="left:' + bigfalc_w_3_c + 'px;height:' + bigfalc_h + 'px">';
						} else {
							bigfalc_w_3_c = b_w / 4 * 2 - 15;
							var img_bigfalc = '<img src="' + img_in + '" class="img_bigfalc" style="left:' + bigfalc_w_3_c + 'px;height:' + bigfalc_h + 'px">';
						}
						$('#rectangle').find('.img_bigfalc').remove();
						$('#rectangle').append(img_bigfalc);
					}
				} else if (val_bf == 2) {
					bigfalc_w_2_l = b_w / 3 - 15;
					bigfalc_w_2_r = b_w / 3 * 2 - 15;
					var img_bigfalc = '<img src="' + img_in + '" class="img_bigfalc" style="left:' + bigfalc_w_2_l + 'px;height:' + bigfalc_h + 'px">';
					if (falc_bayan) {
						var img_bigfalc2 = '<img src="' + img_out + '" class="img_bigfalc" style="left:' + bigfalc_w_2_r + 'px;height:' + bigfalc_h + 'px">';
					} else {
						var img_bigfalc2 = '<img src="' + img_in + '" class="img_bigfalc" style="left:' + bigfalc_w_2_r + 'px;height:' + bigfalc_h + 'px">';
					}
					$('#rectangle').find('.img_bigfalc').remove();
					$('#rectangle').append(img_bigfalc).append(img_bigfalc2);
				} else if (val_bf == 3) {
					bigfalc_w = b_w / 4 - 15;
					bigfalc_w_3_c = b_w / 4 * 2 - 15;
					bigfalc_w_3_r = b_w / 4 * 3 - 15;
					var img_bigfalc = '<img src="' + img_in + '" class="img_bigfalc" style="left:' + bigfalc_w + 'px;height:' + bigfalc_h + 'px">';
					if (falc_bayan) {
						var img_bigfalc2 = '<img src="' + img_out + '" class="img_bigfalc" style="left:' + bigfalc_w_3_c + 'px;height:' + bigfalc_h + 'px">';
					} else {
						var img_bigfalc2 = '<img src="' + img_in + '" class="img_bigfalc" style="left:' + bigfalc_w_3_c + 'px;height:' + bigfalc_h + 'px">';
					}
					var img_bigfalc3 = '<img src="' + img_in + '" class="img_bigfalc" style="left:' + bigfalc_w_3_r + 'px;height:' + bigfalc_h + 'px">';
					$('#rectangle').find('.img_bigfalc').remove();
					$('#rectangle').append(img_bigfalc).append(img_bigfalc2).append(img_bigfalc3);
				}
			} else if ((!val_p && val_b) || (val_p && !val_b)) {
				bigfalc_w = b_w / 3 - 15;
				var img_bigfalc = '<img src="' + img_in + '" class="img_bigfalc" style="left:' + bigfalc_w + 'px;height:' + bigfalc_h + 'px">';
				var img_bigfalc2 = '<div class="div_displ_post" style="left:' + (bigfalc_w + 15 + div_left) + 'px;top: 10px;' + tmp_displ_bf + '">x ' + val_bigfalc + '</div>';
				$('#rectangle').find('.img_bigfalc').remove();
				$('#rectangle').append(img_bigfalc).append(img_bigfalc2);
			} else {
				bigfalc_w = b_w / 4 - 15;
				var img_bigfalc = '<img src="' + img_in + '" class="img_bigfalc" style="left:' + bigfalc_w + 'px;height:' + bigfalc_h + 'px">';
				var img_bigfalc2 = '<div class="div_displ_post" style="left:' + (bigfalc_w + 15 + div_left) + 'px;top: 10px;' + tmp_displ_bf + '">x ' + val_bigfalc + '</div>';
				$('#rectangle').find('.img_bigfalc').remove();
				$('#rectangle').append(img_bigfalc).append(img_bigfalc2);
			}
		}
		/* Добавление биговки */
		if (val_b && !standartFlag) {
			//                    var tmp_displ_creasing = ' display:none';
			var tmp_displ_creasing = '';
			var bl_creasing = $('#rectangle').width();
			if (!val_bf && !val_p) {
				if (val_b == 1) {
					var l_b = bl_creasing / 2 - 1;
					var line = '<div class="bigovka_line" style="left:' + l_b + 'px"></div>';
					$('#rectangle').find('.bigovka_line').remove();
					$('#rectangle').append(line);
				} else if (val_b == 2) {
					var l_b = bl_creasing / 3 - 1;
					var l_b2 = bl_creasing / 3 * 2 - 1;
					var line = '<div class="bigovka_line" style="left:' + l_b + 'px"></div>';
					var line2 = '<div class="bigovka_line" style="left:' + l_b2 + 'px"></div>';
					$('#rectangle').find('.bigovka_line').remove();
					$('#rectangle').append(line).append(line2);
				} else if (val_b == 3) {
					var l_b = bl_creasing / 4 - 1;
					var l_b2 = bl_creasing / 4 * 2 - 1;
					var l_b3 = bl_creasing / 4 * 3 - 1;
					var line = '<div class="bigovka_line" style="left:' + l_b + 'px"></div>';
					var line2 = '<div class="bigovka_line" style="left:' + l_b2 + 'px"></div>';
					var line3 = '<div class="bigovka_line" style="left:' + l_b3 + 'px"></div>';
					$('#rectangle').find('.bigovka_line').remove();
					$('#rectangle').append(line).append(line2).append(line3);
				} else {
					var l_b = bl_creasing / 2 - 1;
					var line = '<div class="bigovka_line" style="left:' + l_b + 'px"></div>';
					var line2 = '<div class="div_displ_post" style="left:' + (l_b + div_left) + 'px;top: 10px;' + tmp_displ_creasing + '">x ' + val_b + '</div>';
					$('#rectangle').find('.bigovka_line').remove();
					$('#rectangle').append(line).append(line2);
				}
			} else if (!val_bf && val_p) {
				var l_b = bl_creasing / 3 - 1;
				var line = '<div class="bigovka_line" style="left:' + l_b + 'px"></div>';
				var line2 = '<div class="div_displ_post" style="left:' + (l_b + div_left) + 'px;top: 10px;' + tmp_displ_creasing + '">x ' + val_b + '</div>';
				$('#rectangle').find('.bigovka_line').remove();
				$('#rectangle').append(line).append(line2);
			} else if (val_bf && !val_p) {
				var l_b = bl_creasing / 3 * 2 - 1;
				var line = '<div class="bigovka_line" style="left:' + l_b + 'px"></div>';
				var line2 = '<div class="div_displ_post" style="left:' + (l_b + div_left) + 'px;top: 10px;' + tmp_displ_creasing + '">x ' + val_b + '</div>';
				$('#rectangle').find('.bigovka_line').remove();
				$('#rectangle').append(line).append(line2);
			} else {
				var l_b = bl_creasing / 4 * 2 - 1;
				var line = '<div class="bigovka_line" style="left:' + l_b + 'px"></div>';
				var line2 = '<div class="div_displ_post" style="left:' + (l_b + div_left) + 'px;top: 10px;' + tmp_displ_creasing + '">x ' + val_b + '</div>';
				$('#rectangle').find('.bigovka_line').remove();
				$('#rectangle').append(line).append(line2);
			}
			//                    bl_creasing = bl_creasing/4;
			//                    bl_creasing = bl_creasing-1;
			//                    var line2 = '<div class="div_displ_post" style="left:'+(bl_creasing*3+div_left)+'px;top: 10px;'+tmp_displ_creasing+'">x '+val_b+'</div>';
			//                    var line = '<div class="bigovka_line" style="left:'+(bl_creasing*3)+'px"></div>';
			//                    $('#rectangle').find('.bigovka_line').remove();
			//                    $('#rectangle').append(line).append(line2);
		}
		//			Добавление перфорации
		if (val_p) {
			//                    var tmp_displ_perf = ' display:none';
			var tmp_displ_perf = '';
			var val_perf = $('[name=dop_perforation] option:selected').val();
			var bl_perf = $('#rectangle').width();
			if (!val_bf && !val_b) {
				if (val_p == 1) {
					var l_p = bl_perf / 2 - 1;
					var line = '<div class="perf_line" style="left:' + l_p + 'px"></div>';
					$('#rectangle').find('.perf_line').remove();
					$('#rectangle').append(line);
				} else if (val_p == 2) {
					var l_p = bl_perf / 3 - 1;
					var l_p2 = bl_perf / 3 * 2 - 1;
					var line = '<div class="perf_line" style="left:' + l_p + 'px"></div>';
					var line2 = '<div class="perf_line" style="left:' + l_p2 + 'px"></div>';
					$('#rectangle').find('.perf_line').remove();
					$('#rectangle').append(line).append(line2);
				} else if (val_p == 3) {
					var l_p = bl_perf / 4 - 1;
					var l_p2 = bl_perf / 4 * 2 - 1;
					var l_p3 = bl_perf / 4 * 3 - 1;
					var line = '<div class="perf_line" style="left:' + l_p + 'px"></div>';
					var line2 = '<div class="perf_line" style="left:' + l_p2 + 'px"></div>';
					var line3 = '<div class="perf_line" style="left:' + l_p3 + 'px"></div>';
					$('#rectangle').find('.perf_line').remove();
					$('#rectangle').append(line).append(line2).append(line3);
				} else {
					var l_p = bl_perf / 2 - 1;
					var line2 = '<div class="div_displ_post" style="left:' + (l_p + div_left) + 'px;top: 10px;' + tmp_displ_perf + '">x ' + val_perf + '</div>';
					var line = '<div class="perf_line" style="left:' + l_p + 'px"></div>';
					$('#rectangle').find('.perf_line').remove();
					$('#rectangle').append(line).append(line2);
				}
			} else if ((!val_bf && val_b) || (val_bf && !val_b)) {
				var l_p = bl_perf / 3 * 2 - 1;
				var line2 = '<div class="div_displ_post" style="left:' + (l_p + div_left) + 'px;top: 10px;' + tmp_displ_perf + '">x ' + val_perf + '</div>';
				var line = '<div class="perf_line" style="left:' + l_p + 'px"></div>';
				$('#rectangle').find('.perf_line').remove();
				$('#rectangle').append(line).append(line2);
			} else {
				var l_p = bl_perf / 4 * 3 - 1;
				var line2 = '<div class="div_displ_post" style="left:' + (l_p + div_left) + 'px;top: 10px;' + tmp_displ_perf + '">x ' + val_perf + '</div>';
				var line = '<div class="perf_line" style="left:' + l_p + 'px"></div>';
				$('#rectangle').find('.perf_line').remove();
				$('#rectangle').append(line).append(line2);
			}
			//                    bl_perf = bl_perf/2;
			//                    bl_perf = bl_perf-1;
			//                    var line2 = '<div class="div_displ_post" style="left:'+(bl_perf+div_left)+'px;top: 10px;'+tmp_displ_perf+'">x '+val_perf+'</div>';
			//                    var line = '<div class="perf_line" style="left:'+(bl_perf)+'px"></div>';
			//                    $('#rectangle').find('.perf_line').remove();
			//                    $('#rectangle').append(line).append(line2);
		}
		//			Добавление сверления
		if ($('[name=dop_drilling]').val() > 0 && option_default.drilling_left == 0) {
			//                if(po4 == 1 && $('select[atr_id^="post_drilling"]').prop('disabled')){
			//                    var tmp_displ_sv = ' display:none';
			//                }else{
			var tmp_displ_sv = '';
			//                }
			var val_sverl = $('[name=dop_drilling] option:selected').val();
			var sverl_w = $('#rectangle').width();
			var sverl_h = $('#rectangle').height();
			if (!val_b && !val_bf && !val_p) {
				var l = sverl_w / 2 - 8;
			} else {
				var l = 10;
			}
			var bl_sverl2 = '<div class="div_displ_post" style="left:' + (l + 15 + div_left) + 'px;top: 10px;' + tmp_displ_sv + '">x ' + val_sverl + '</div>';
			//                var l = sverl_w/2-18;
			//                var l = sverl_w/2-20;
			var t = sverl_h / 10;
			var bl_sverl = '<div class="bl_sverl" style="left:' + l + 'px;top:10px"></div>';
			$('#rectangle').find('.bl_sverl').remove();
			$('#rectangle').append(bl_sverl).append(bl_sverl2);
		} else if (option_default.drilling_left == 1) {
			var st = 'position:absolute;top:0;left:0;width:150px;';
			var div = '<div id="post_left_text" style="' + st + '">';
			if ($('[name=dop_drilling]').length) {
				if ($('[name=dop_drilling]').val() != '0') {
					div += '<p>Сверление : ' + $('[name=dop_drilling]').val() + '</p>';
				}
			}
			div += '</div>';
			$('#blockpic').prepend(div);
		}
		//--------------------------	Добавление закругление углов   ---------------------------
		if ($('[name=dop_curve] option:selected').val() != 0 && !standartFlag) {
			var val_curve = $('[name=dop_curve] option:selected').val();
			if (val_curve == 1) {
				$('#rectangle').css('border-top-left-radius', '10px');
			} else if (val_curve == 2) {
				$('#rectangle').css('border-top-left-radius', '10px');
				$('#rectangle').css('border-bottom-left-radius', '10px');
			} else if (val_curve == 3) {
				$('#rectangle').css('border-top-left-radius', '10px');
				$('#rectangle').css('border-top-right-radius', '10px');
				$('#rectangle').css('border-bottom-left-radius', '10px');
			} else if (val_curve == 4) {
				$('#rectangle').css('border-radius', '10px');
			}
			//                $('#rectangle').css('border-radius','10px');
		} else if ($('[name=dop_curve] option:selected').val() == 0 && !standartFlag) {
			if (option_default.typePrewiev != 'circle') {
				$('#rectangle').css('border-radius', '0px');
			}
		}
		//--------------------------	Добавление закругление углов на высечке  ---------------------------
		if (option_default.typePrewiev == 'felling') {
			$('#rectangle').css('border-radius', '10px');
		}
	} //-----------------------------------      КОНЕЦ     ФУНКЦИИ    changepic2   ------------------------------------------