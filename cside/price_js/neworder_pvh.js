$.fn.extend({
    animateCssShow: function (animationName) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        this.addClass('animated open ' + animationName).one(animationEnd, function() {
            $(this).removeClass('animated ' + animationName);
        });
    },
    animateCssHide: function (animationName) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
         $(this).addClass('animated ' + animationName).one(animationEnd, function() {
            $(this).removeClass('animated open ' + animationName);
        });
    },
    animateCss: function (animationName,afterFunc) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
         $(this).addClass('animated ' + animationName).one(animationEnd, function() {
            if(afterFunc != ''){
                afterFunc();
            }
            $(this).removeClass('animated ' + animationName);
        });
    }
});
function add_opacity_filter_reset(){
    var checkM = $('#selectall:checked').length + $('.select-mat:checked').length;
    var checkT = $('#alltime:checked').length + $('.terms:checked').length; 
    var checkP = $('#all-print:checked').length + $('.select-print:checked').length;
    var sum = checkM + checkT + checkP;
    if( sum == 0)
    {
        $('#filter-reset').addClass('reset_no_active');   
    }else{
        $('#filter-reset').removeClass('reset_no_active');
    }
}
var active = 0;
var activeP = 0;
var xhrM = '';
var xhrP = '';
var insert = '<p class="text-center" style="font-size: 50px;padding: 20px 0;"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span></p>';
var width = 0;
var height = 0;
var firstLoad = true;
var insertNotSel = '<tr><td colspan="9" class="text-center" style="vertical-align:middle;padding:20px;font-weight:500;font-size:22px;">Для формирования прайс-листа выберите материал</td></tr>';
var flagReset = true;
// функция смены текста тиража в модальном наценки
function change_text_modal_tirazh(name,val_inp){
    $('.m_d_tir_'+name).html(val_inp);
}
$('document').ready(function(){
    $('select.sel_post_rab,#lam,#sel_razmer,.sel_metr,.sel_post_luv,#prints').chosen({width: "100%",disable_search_threshold: 10,disable_search: true});
    $('#price').popover({
        selector: '[data-toggle=tooltip]',
        placement: 'top',
        html: true,
        trigger: 'hover'
    });
    function removeAnim(){
        $('.animated.zoomIn').removeClass('animated zoomIn');
    }
    setTimeout(removeAnim,1000);
    $('.t_greyblock,.header-opt').popover({
        selector: '[data-toggle=tooltip]',
        placement: 'top',
        html: true,
        trigger: 'hover'
    });
    $('.t_greyblock,.header-opt').popover({
        selector: '[data-toggle=tooltip]',
        placement: 'top',
        html: true,
        trigger: 'hover'
    });
    $('.print-price').click(function(){
        if($(this).hasClass('icon_print')){
            printClick = 'print';
        } else if($(this).hasClass('icon_exel')) {
            printClick = 'excel';
        } else if($(this).hasClass('icon_word')) {
            printClick = 'word';
        } 
    });
    $('.input_discount').on('input', function(){
        if (this.value.match(/[^0-9.]/g)) {
            this.value = this.value.replace(/[^0-9.]/g, '');
        } 
    });
    $('body').on('click','#m_d_submit',function(){
            $('.d_m_d_variant').eq( $('.m_d_variant:checked').val() ).prop('checked',true);
            $('#d_m_d_all').val( $('#m_d_all').val() );
            $('#d_m_d_t_1').val( $('#m_d_t_1').val() );
            $('#d_m_d_t_2').val( $('#m_d_t_2').val() );
            $('#d_m_d_t_3').val( $('#m_d_t_3').val() );
            $('#d_m_d_t_4').val( $('#m_d_t_4').val() );
            $('#d_m_d_t_5').val( $('#m_d_t_5').val() );
            $('#d_m_d_t_6').val( $('#m_d_t_6').val() );
            $(this).closest('.modal').modal('hide');
            $("#pricefilters").submit();
        });
         $('body').on('click','#d_m_d_submit',function(){
             $(this).closest('.modal').modal('hide');
             pricePints(printClick);  
         });
    
        $('body').on('change keyup','#m_d_all',function(){
            $('#m_d_t_1,#m_d_t_2,#m_d_t_3,#m_d_t_4,#m_d_t_5,#m_d_t_6').val($(this).val());
        });
        $('body').on('change keyup','#m_d_t_1,#m_d_t_2,#m_d_t_3,#m_d_t_4,#m_d_t_5,#m_d_t_6',function(){
            $('#m_d_all').val('');
        });
        $('body').on('change keyup','#d_m_d_all',function(){
            $('#d_m_d_t_1,#d_m_d_t_2,#d_m_d_t_3,#d_m_d_t_4,#d_m_d_t_5,#d_m_d_t_6').val($(this).val());
        });
        $('body').on('change keyup','#d_m_d_t_1,#d_m_d_t_2,#d_m_d_t_3,#d_m_d_t_4,#d_m_d_t_5,#d_m_d_t_6',function(){
            $('#d_m_d_all').val('');
        });
    
    $("#pricefilters").submit(function() {
        if(active != 0){
            xhrM.abort();
        }
        
        var dataSend = $("#pricefilters").serialize();
        xhrM = $.ajax({
            type: "POST",
            url: "cside/ajax.php?action=showpvhnew",
            data: dataSend, 
            dataType: 'json',
            beforeSend: function(){
                active++;
                $('#insert-in').html(''); 
                $('#loading-table').html(insert);
//                console.log(dataSend)
            },
            complete: function(data){ 
                if(data.responseText == 'error'){
                    location.reload();
                } else if(data.statusText == 'OK' && IsJsonString(data.responseText) == false){
                    $('#loading-table').html(''); 
                    var text = '<tr><td colspan="9" class="text-center" style="vertical-align:middle;padding:20px;font-weight:500;font-size:22px;">При подсчете произошла ошибка! Пожалуйста сообщите нам, об этом нажав на кнопку "Нашли ошибку".</td></tr>';
                    $('#insert-in').html(text);
                }
            },
            success: function(data){ 
                 $('#loading-table').html('');
                 $('#insert-in').html(com(data.text));
                active=0; 
                var checkM = $('#selectall:checked').length + $('.select-mat:checked').length;
                var checkT = $('#alltime:checked').length + $('.terms:checked').length; 
                var checkP = $('#all-print:checked').length + $('.select-print:checked').length;
                if(checkM > 0 && checkP > 0 && checkT > 0){
                    $('.time-select.hidden').removeClass('hidden').animateCss('fadeIn','');
                    $('#alltime').closest('.t_switchable').find('.filter-name').html('Cрок печати');
                } 
                xhrM = '';
            }
        }); 
        return false; 
    });
    function pricePints(printClass){
        if(activeP != 0){
            xhrP.abort();
        }
        var dateNow = new Date();
        var day = dateNow.getDate();
        var month = dateNow.getMonth() + 1;
        var year = dateNow.getFullYear();
        var dataSend = $("#pricefilters").serialize() + '&' + $('[name="form_discount_download"]').serialize();
        xhrP = $.ajax({
            type: "POST",
            url: "cside/ajax.php?action=showpvhnew&type=print",
            data: dataSend, 
            dataType: 'json',
            beforeSend: function(){
                activeP++;
            },
            success: function(data){ 
                if(printClass == 'print'){
                    $(com(data.text)).print({
                        globalStyles: true,
                        mediaPrint: false,
                        stylesheet: "cside/print-css.css",
                        noPrintSelector: ".no-print",
                        iframe: true,
                        append: null,
                        prepend: null,
                        manuallyCopyFormValues: true,
                        deferred: $.Deferred(),
                        timeout: 750,
                        title: null,
                        doctype: '<!doctype html>'
                    });
                } else if(printClass == 'excel') {
                    tableToExcel(com(data.text));
                }
                activeP=0; 
                xhrP = '';
            }
        }); 
        return false; 
    };
    $('#count_format_contour').change(function(){
        calculate();
        if(!firstLoad){             
            $("#pricefilters").submit();         
        } 
    });

    $(".sel_post_rab").change(function(){
        if($(this).val() > 0 && this.id == 'print_color'){
            $('#p_0').prop('checked',false).prop('disabled',true).change();
        } else if($(this).val() == 0 && this.id == 'print_color'){
            $('#p_0').prop('disabled',false);
        }
        if(this.id == 'trim' && $(this).val() > 1){
            $('.block-plot.hidden').removeClass('hidden');
            $('#count_format_contour:disabled').prop('disabled',false).change().trigger("chosen:updated");
            if($('#guard_angle').val() > 0){
                $('#guard_angle').val(0).change().trigger("chosen:updated");
            }
        } else if(this.id == 'trim' && $(this).val() <= 1) {
            $('.block-plot:not(.hidden)').addClass('hidden');
            $('#count_format_contour:not(:disabled)').prop('disabled',true).change();
        }
        if(this.id == 'guard_angle' && $('#trim').val() > 1){
            $('#trim').val(1).change().trigger("chosen:updated");
        }
        calculate();
        changepic();
        add_opacity_option_reset();
        if(!firstLoad){             
            $("#pricefilters").submit();         
        }  
    });
    $("#option-reset:not(.reset_no_active)").click(function(){
        if($('#trim').val() != 1){
            $('#trim').val(1).change().trigger("chosen:updated");
        }
        if($('#sets').val() != 1){
            $('#sets').val(1).change();
        }
        $('.sel_post_rab:not(#trim)').each(function(){
            if($(this).val() != 0){
                $(this).val(0).change().trigger("chosen:updated");
            }
        });
        
        add_opacity_option_reset();
    });
    $('#print_color').change(function(){
        calculate();
    });
    function calculate(){
        if($("#skotch").val() != 0) {   
//            var price = (+(width * height/1000000).toFixed(3) <= 0.1 ? (10).toFixed(2) : ((width * height/1000000).toFixed(3)*10).toFixed(2));
//             var price = 10;
            var squereList = (width*height)/1000000;
            var S_kv = squereList/($('#count_format_contour').length ? $('#count_format_contour').val() : 1);
            var l_kv = Math.sqrt(S_kv);
            var P_kv = l_kv*4;
            
            var P_all_kv = P_kv*($('#count_format_contour').length ? $('#count_format_contour').val() : 1);
            var price = ((P_all_kv*5.5).toFixed(2) > 11 ? (P_all_kv*5.5).toFixed(2) : 11);
            
//            var price = ( ((width/1000+height/1000)*2*5).toFixed(2) > 10 ? ((width/1000+height/1000)*2*5).toFixed(2) : 10);
             var text = '+ ' + price + ' грн/шт (минимум 11 грн)';
             $("#skotch").closest('.pw-cont').find('.selected-show').html(text).show();
        } else {
           $("#skotch").closest('.pw-cont').find('.selected-show').html('').hide();
        } 
        if($('#print_color').val() > 0){
            var price = +((width * height/1000000)*150).toFixed(2);
            var text = '+ ' + (+(width * height/1000000)).toFixed(2)+' x 150 = ' + price +' грн';
            $("#print_color").closest('.pw-cont').find('.selected-show').html(text).show();
        } else {
            $("#print_color").closest('.pw-cont').find('.selected-show').html('').hide();
        }
        if($('#guard_angle').val() > 0){
            var price = 8;
            var text = '+ ' + price +' грн/изделие';
            $("#guard_angle").closest('.pw-cont').find('.selected-show').html(text).show();
        } else {
            $("#guard_angle").closest('.pw-cont').find('.selected-show').html('').hide();
        }
        if($('#pvh_drilling').val() > 0){
            var count = $('#pvh_drilling').val(); 
            var price = +(count*2).toFixed(2);
            var text = '+ ' + count +' x 2 = ' + price +' грн';
            $("#pvh_drilling").closest('.pw-cont').find('.selected-show').html(text).show();
        } else {
            $("#pvh_drilling").closest('.pw-cont').find('.selected-show').html('').hide();
        }
        if($('#trim').val() == 1){
            var price1 = ((width/1000 + height/1000)*3.3*2).toFixed(2);
            var price2 = ((width/1000 + height/1000)*13.2*2).toFixed(2);
            
            var text = '+ ' + price1 + ' - ' + price2 + ' грн/шт, зависит от толщины материала (минимум ' + 11  + ' грн)';
           $('#trim').closest('.pw-cont').find('.selected-show').html(text).show();
        } else {
            var squereList = (width*height)/1000000;
            var S_kv = squereList/($('#count_format_contour').length ? $('#count_format_contour').val() : 1);
            var l_kv = Math.sqrt(S_kv);
            var P_kv = l_kv*4;
            var P_all_kv = P_kv*($('#count_format_contour').length ? $('#count_format_contour').val() : 1);
            var price1 = (+P_all_kv*4.4*($('#trim').val() == 3 ? 1.5 : 1)).toFixed(2);
            var price2 = (+P_all_kv*14.1*($('#trim').val() == 3 ? 1.5 : 1)).toFixed(2); 
            
            var text = '+ ' + price1 + ' - ' + price2 + ' грн/шт, зависит от толщины материала (минимум 4.4 - 14.1 грн)';
            
            $('#trim').closest('.pw-cont').find('.selected-show').html(text).show();
        }
    }
    function add_opacity_option_reset(){
        var allNull = true;
        $('.sel_post_rab:not(#trim)').each(function(){
            if($(this).val() != 0){
                allNull = false;
            }
        });
        if($('#sets').val() != 1){
            allNull = false;
        }
        if($('#trim').val() != 1){
            allNull = false;
        }
        if(allNull){
            $('#option-reset:not(.reset_no_active)').addClass('reset_no_active');
        } else {
            $('#option-reset.reset_no_active').removeClass('reset_no_active');
        }
    }
    /*Обработчики фильтров*/
    $('#selectall').change(function(){
        $(this).queue(function(){
            $('.select-mat:checked').prop('checked',false);
            $(this).closest('.t_switchable').find('.filter-name').html('Материал');
            add_opacity_filter_reset();
            $(this).dequeue();
        });
        $(this).queue(function(){
            setSubmit();
            $(this).dequeue();
        });
    });
    
    $('#alltime').change(function(){
        $(this).queue(function(){
            $('.terms:checked').prop('checked',false);
            $(this).closest('.t_switchable').find('.filter-name').html('Срок печати');
            add_opacity_filter_reset();
            $(this).dequeue();
        });
        $(this).queue(function(){
            setSubmit();
            $(this).dequeue();
        });
    });
    $('#all-print').change(function(){
        $(this).queue(function(){
            $('.select-print:checked').prop('checked',false);
            $(this).closest('.t_switchable').find('.filter-name').html('Качество печати');
            add_opacity_filter_reset();
            $(this).dequeue();
        });
        $(this).queue(function(){
            setSubmit();
            $(this).dequeue();
        });
    });
    
    $('.select-mat').change(function(){
        $(this).queue(function(){
            $('#selectall:checked').prop('checked',false);
            $('#selectall:not(:disabled)').closest('.t_switchable').find('.filter-name').html('Выбрать все<br>материалы');
            add_opacity_filter_reset();
            $(this).dequeue();
        });
        $(this).queue(function(){
            setSubmit();
            $(this).dequeue();
        });
    });
    $(".select-print").change(function(){
        $(this).queue(function(){
            $('#all-print:checked').prop('checked',false);
            $('#all-print:not(:disabled)').closest('.t_switchable').find('.filter-name').html('Выбрать все<br>печати');
            add_opacity_filter_reset();
            $(this).dequeue();
        });
        $(this).queue(function(){
            setSubmit();
            $(this).dequeue();
        });
    });
    $(".terms").change(function(){
        $(this).queue(function(){
            $('#alltime:checked').prop('checked',false);
            $('#alltime:not(:disabled)').closest('.t_switchable').find('.filter-name').html('Выбрать все<br>сроки печати');
            add_opacity_filter_reset();
            $(this).dequeue();
        });
        $(this).queue(function(){
            setSubmit();
            $(this).dequeue();
        });
    });
    function setSubmit(){
        var checkM = $('#selectall:checked').length + $('.select-mat:checked').length;
        var checkT = $('#alltime:checked').length + $('.terms:checked').length; 
        var checkP = $('#all-print:checked').length + $('.select-print:checked').length;
        if($('#print_color').val() == 1){
            checkM = 1;
        }
        if(checkM > 0 && checkP > 0 && checkT > 0){
            if(flagReset){
                flagReset = false;
               $('.select-all-btn').each(function () {
                   if(this.id == 'selectall'){
                      $(this).closest('.t_switchable').find('.filter-name').html('Выбрать все<br>материалы');
                   } else if (this.id == 'alltime') {
                     $(this).closest('.t_switchable').find('.filter-name').html('Выбрать все<br>сроки печати');
                   } else if (this.id == 'all-print') {
                     $(this).closest('.t_switchable').find('.filter-name').html('Выбрать все<br>печати');
                   }
                });
            }
            $('.select-all-btn').prop('disabled',false);
            $("#pricefilters").submit();
        } else {
            var insertNotSel = '<tr><td colspan="9" class="text-center" style="vertical-align:middle;padding:20px;font-weight:500;font-size:22px;">Для формирования прайс-листа выберите';
            var text = '';
            if(checkM == 0){
                text += ' материал,';
            }
            if(checkP == 0){
                text += ' печать,';
            }
            if(checkT == 0){
                text += ' срок,';
            }
            text = text.substring(0, text.length - 1)
            insertNotSel += text + '</td></tr>';
            $('#insert-in').html(insertNotSel);
        }
    }
    $('#filter-reset').click(function(){
        $(this).queue(function(){
            $('#alltime:checked,#all-print:checked,.select-mat:checked,#selectall:checked,.terms:checked,.select-print:checked').prop('checked',false).change();
            $('#selectall').closest('.t_switchable').find('.filter-name').html('Материал');
            $('#alltime').closest('.t_switchable').find('.filter-name').html('Срок печати');
            $('#all-print').closest('.t_switchable').find('.filter-name').html('Печать');
            $(this).dequeue();
        });
        $(this).queue(function(){
            $('.select-all-btn').prop('disabled',true);
            add_opacity_filter_reset();
            flagReset = true;
            $(this).dequeue();
        });
    });
    
    /*END FILTERS*/
    
    $('#sel_razmer').change(function(e) {
        width = +$(this).find('option:selected').data('width');
        height = +$(this).find('option:selected').data('height');
        $('.input_size').val('');
        $('[name="size_in"]').val(1).change().trigger("chosen:updated");
        $('#size_input').data("width",width).data("height",height);
        $('#size_input').attr('value',$(this).val()).change();
    });
    $('.input_size').on('input',function(){
        if (this.value.match(/[^0-9.]/g)) {
            this.value = this.value.replace(/[^0-9.]/g, '');
        }
    });
    $('.input_size').on('change',function(){
        $('#sel_razmer').val(0).trigger("chosen:updated");
        if(($('#input-width1').val()*$('[name="size_in"]').val()) < 50){
            $('#input-width1').val(50/$('[name="size_in"]').val());
        }
        if(($('#input-width1').val()*$('[name="size_in"]').val()) > 50000){
             $('#input-width1').val(50000/$('[name="size_in"]').val());
        }
        if( ($('#input-height1').val()*$('[name="size_in"]').val()) < 50){
            $('#input-height1').val(50/$('[name="size_in"]').val());
        }
        if(($('#input-height1').val()*$('[name="size_in"]').val()) > 50000){
             $('#input-height1').val(50000/$('[name="size_in"]').val());
        }
        
        width = +$('#input-width1').val()*$('[name="size_in"]').val();
        height = +$('#input-height1').val()*$('[name="size_in"]').val();
        $('#input-width').val(width);
        $('#input-height').val(height);
        
        $('#size_input').data("width",width).data("height",height);
        $('#size_input').attr('value',30).change();
        if(!firstLoad){             $("#pricefilters").submit();         }
    });
    $('[name="size_in"]').change(function(){
         var wid = $('#input-width1').val(),
            hei = $('#input-height1').val();
        if( wid != '' && hei != '' && +$('#size_input').val() == 30){
            if( $(this).val() == 10 ){
                $('#input-width1').val(wid/10).trigger('change');
                $('#input-height1').val(hei/10).trigger('change');
            }else{
                $('#input-width1').val(wid*10).trigger('change');
                $('#input-height1').val(hei*10).trigger('change');
            }
        }
    });
    $('#size_input').change(function(){
        if(parametrLoad != 'product' || (parametrLoad == 'product' && $('#rectangle').length > 0)){
            if($(this).val() == 30){
               smena_razmera(width,height,'Ваш размер');
            } else {
               smena_razmera(width,height,'');
            }
        }
        var infoShow = true;
        if((((height  > 1210) && (width > 1210)) || ((height  > 1210) && (width > 1210)) || (height  > 2040) || (width  > 2040)) 
               && infoShow)
        {
//                infoShow = false;
                $('#modal_error_max_size').modal('show');
        }
        calculate();
        sizeRelations();
        if(!firstLoad){             $("#pricefilters").submit();         }
    });
    function sizeRelations(){
        if((height < 1250 && height < 2500) || (height < 2500 && height < 1250)){
            $('#trim option[value = 2]:disabled, #trim option[value = 3]:disabled').prop('disabled',false);
            $('#trim').trigger("chosen:updated");
        } else {
            if($('#trim').val() == 2 || $('#trim').val() == 3){
                $('#trim').val(0).change();
            }
            $('#trim option[value = 2]:not(:disabled),#trim option[value = 3]:not(:disabled)').prop('disabled',true);
            $('#trim').trigger("chosen:updated");
        }
    }
    if($(window).width() < 991){
         $('.header-opt').click(function(){
            if($(this).hasClass('openChild') == false){
                $(this).addClass('openChild');
                $(this).closest('.mobile-control').find('.mobile-droped').stop(true,true).animateCssShow('slideMenuDown');
                $(this).closest('.mobile-control').find('.icon-drop .fa-plus').fadeOut(250);
                $(this).closest('.mobile-control').find('.icon-drop .fa-minus').fadeIn(250);
            } else {
                $(this).removeClass('openChild');
                $(this).closest('.mobile-control').find('.mobile-droped').stop(true,true).animateCssHide('slideMenuTop');
                $(this).closest('.mobile-control').find('.icon-drop .fa-minus').fadeOut(250);
                $(this).closest('.mobile-control').find('.icon-drop .fa-plus').fadeIn(250);
            }
                
        });
    } else {
        padT();
    }
    $('.types-prod').click(function () {
        location.href = $(this).attr('data-link');
    });
    /*FUNCTIONS*/
   
    function tmpHide(){
        $('.material').each(function(){
            var contRowAll = $(this).find('.print-row').length;
            var contRowHide = $(this).find('.print-row.hidden-by-print').length; 
            if(contRowAll == contRowHide){
                $(this).addClass('hidden-by-print');
                $('#m_' + $(this).attr('data-id')).prop('disabled',true).prop('checked',false);
                if($('.select-mat:checked').length == 0){
                    $('#selectall:not(:checked)').prop('checked',true).change();
                }
            } else {
                $(this).removeClass('hidden-by-print');
                $('#m_' + $(this).attr('data-id')).prop('disabled',false);
            }
        });
    }
    function padT(){
        $(this).stop(true,true).delay(300).queue(function(){
            var removeOpacity = function(){
                $('.gallery').css('opacity',1);
            };
            $('.gallery').animateCss('fadeIn',removeOpacity);
            $(this).dequeue();
        
        });
    }
    function tableToExcel( element ) {
       var html, link, blob, url, css;
       var dateNow = new Date();
       var day = dateNow.getDate();
       var month = dateNow.getMonth() + 1;
       var year = dateNow.getFullYear();
       var template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>' + $(element).html() + '</table></body></html>' ;
       blob = new Blob(['\ufeff',css + template], {
         type: 'application/vnd.ms-excel'
       });
       url = URL.createObjectURL(blob);
       link = document.createElement('A');
       link.href = url;
       link.download = 'sborkaPrice_' + day + '-' + (month < 10 ? 0 : '') + month + '-' + year + '.xls';  // default name without extension 
       document.body.appendChild(link);
       if (navigator.msSaveOrOpenBlob ) navigator.msSaveOrOpenBlob( blob, 'sborkaPrice_' + day + '-' + (month < 10 ? 0 : '') + month + '-' + year + '.xls'); // IE10-11
           else link.click();  // other browsers
       document.body.removeChild(link);
     };
    function com(compressed) {
        "use strict";
        // Build the dictionary.
        var i,
            dictionary = [],
            w,
            result,
            k,
            entry = "",
            dictSize = 256;
        for (i = 0; i < 256; i += 1) {
            dictionary[i] = String.fromCharCode(i);
        }
 
        w = String.fromCharCode(compressed[0]);
        result = w;
        for (i = 1; i < compressed.length; i += 1) {
            k = compressed[i];
            if (dictionary[k]) {
                entry = dictionary[k];
            } else {
                if (k === dictSize) {
                    entry = w + w.charAt(0);
                } else {
                    return null;
                }
            }
 
            result += entry;
 
            // Add w+entry[0] to the dictionary.
            dictionary[dictSize++] = w + entry.charAt(0);
 
            w = entry;
        }
        function decode_utf8(s) {
          return decodeURIComponent(escape(s));
        }
        return decode_utf8(result);
    }
    
    /*END FUNCTION*/
    
// при первой загрузке для смены тиражей в модальном наценки
    $(".tirazh").each(function(){
        change_text_modal_tirazh( $(this).attr('numb_tir'), $(this).val() );
    });
    
    $(".tirazh").bind('change',function(){
        if($(this).val() != ''){
            change_text_modal_tirazh( $(this).attr('numb_tir'), $(this).val() );
            if(!firstLoad){             $("#pricefilters").submit();         } 
        }
	});
    /*FIRST LOAD*/
    add_opacity_filter_reset();
    /*INPUTS CONTROL*/
    $('.input-option').on("input change",function(){
        var selector = '[data-controlled-item=' + this.id + '-selected]';
        if (this.value.match(/[^0-9]/g)) {
            this.value = this.value.replace(/[^0-9]/g, '');
        }
        if(this.id == 'sets'){
            if($(this).val() < 1 && $(this).val() != ''){$(this).val(1).change();}
        } else if(this.id == 'drilling'){
            if($(this).val() < 0 && $(this).val() != ''){$(this).val(0).change();}
        }
    });
    $('.input-option').on('focusout',function(){
        if($(this).val() == ''){$(this).val(1).change();}
    });
    $('.input-option').on("change",function(){
        if(!firstLoad){             $("#pricefilters").submit();         } 
        if(this.id == 'sets'){
//            if($(this).val() != 1){$('#text_info_sets').css('display','block');}else{$('#text_info_sets').css('display','none');}
            if($(this).val() > 50){$(this).val(50).change();}
        }
        add_opacity_option_reset();
     });
    $('.plus-input-val').click(function(){
        var selector = $(this).attr("data-change-input");
        $('#' + selector).val(parseInt($('#' + selector).val()) + 1).change();
    });
    $('.minus-input-val').click(function(){
        var selector = $(this).attr("data-change-input");
        if($('#' + selector).val() > 0){
            $('#' + selector).val(parseInt($('#' + selector).val()) - 1).change();
        }
    });
    $('.orient').change(function(){
        if(!firstLoad){             $("#pricefilters").submit();         }
    });
    function IsJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }
    function add_opacity_option_reset(){
        var allNull = true;
        $('.sel_post_rab:not(#trim)').each(function(){
            if($(this).val() != 0){
                allNull = false;
            }
        });
        if($('#trim').val() != 1){
            allNull = false;
        }
//        if($('#drilling').val() != 0){
//            allNull = false;
//        }
        if($('#sets').val() != 1){
            allNull = false;
        }
        if(allNull){
            $('#option-reset:not(.reset_no_active)').addClass('reset_no_active');
        } else {
            $('#option-reset.reset_no_active').removeClass('reset_no_active');
        }
    }
    if(parametrLoad == 'return' || parametrLoad == 'product'){
        $.getScript('cside/price_js/repeat_order.js').done(function( script, textStatus ) {
            loadPage();
        });
    } else {
        $(this).queue(function(){
            if($('#size_type2:checked').length > 0){
                $('#size_type1').prop('checked',true).change();
            }
            $('#sel_razmer').change().trigger("chosen:updated");
            $('select.sel_post_rab').each(function(){
                if($(this).val() != 0){
                    $(this).change().trigger("chosen:updated");
                }
            });
            $("#pricefilters input:checked").change();
            $(this).dequeue();
        });
        $(this).queue(function(){
            add_opacity_option_reset();
            add_opacity_filter_reset();
            $("#pricefilters").submit();
            firstLoad = false;
            $(this).dequeue();
        });
     }
    
    add_opacity_filter_reset();
});